package logs.generation;

import controller.tracers.processes.ProcessStepTracer;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import model.processModel.ProcessStep;
import model.processModel.ProcessStepTransition;
import model.traces.BusinessProcessTrace;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class BusinessProcessTracer {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private static ProcessMap processMap = new ProcessMap("Test");

    private static ProcessStep processStep1 = new ProcessStep("Step 1");
    private static ProcessStep processStep2 = new ProcessStep("Step 2");
    private static ProcessStep processStep3 = new ProcessStep("Step 3");
    private static ProcessStep processStep4 = new ProcessStep("Step 4");
    private static ProcessStep processStep5 = new ProcessStep("Step 5");
    private static ProcessStep processStep6 = new ProcessStep("Step 6");
    private static ProcessStep processStep7 = new ProcessStep("Step 7");

    private static ProcessStepTransition transition1 = new ProcessStepTransition(processStep1, processStep2, "");
    private static ProcessStepTransition transition2 = new ProcessStepTransition(processStep2, processStep3, "");
    private static ProcessStepTransition transition3 = new ProcessStepTransition(processStep4, processStep3, "");
    private static ProcessStepTransition transition4 = new ProcessStepTransition(processStep3, processStep5, "");
    private static ProcessStepTransition transition5 = new ProcessStepTransition(processStep5, processStep6, "");
    private static ProcessStepTransition transition6 = new ProcessStepTransition(processStep5, processStep7, "");

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @BeforeClass
    public static void setupClass() {
        BusinessProcess businessProcess = new BusinessProcess("Process 1");

        businessProcess.getProcessSteps().add(processStep1);
        businessProcess.getProcessSteps().add(processStep2);
        businessProcess.getProcessSteps().add(processStep3);
        businessProcess.getProcessSteps().add(processStep4);
        businessProcess.getProcessSteps().add(processStep5);
        businessProcess.getProcessSteps().add(processStep6);
        businessProcess.getProcessSteps().add(processStep7);

        businessProcess.getTransitions().add(transition1);
        businessProcess.getTransitions().add(transition2);
        businessProcess.getTransitions().add(transition3);
        businessProcess.getTransitions().add(transition4);
        businessProcess.getTransitions().add(transition5);
        businessProcess.getTransitions().add(transition6);

        businessProcess.getStartSteps().add(processStep1);
        businessProcess.getStartSteps().add(processStep4);

        businessProcess.getEndSteps().add(processStep6);
        businessProcess.getEndSteps().add(processStep7);

        processMap.getBusinessProcesses().add(businessProcess);
    }

    @Test
    public void test() {
        ProcessStepTracer tracer = new ProcessStepTracer();
        List<BusinessProcessTrace> traces = tracer.determineBusinessProcessTraces(processMap.getBusinessProcesses());

        Assert.assertEquals(4, traces.size());
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
