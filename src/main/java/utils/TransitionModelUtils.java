package utils;

import model.processModel.BusinessProcess;
import model.processModel.ProcessStep;
import model.processModel.ProcessStepTransition;
import model.transitionModel.TransitionEdge;
import model.transitionModel.TransitionModel;
import model.transitionModel.TransitionNode;

import java.util.*;

public class TransitionModelUtils {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    public static TransitionModel generateFromProcesses(List<BusinessProcess> businessProcesses) {
        Map<String, TransitionNode> nodes = new HashMap<>();
        Map<String, Map<String, TransitionEdge>> edges = new HashMap<>();

        for (BusinessProcess process : businessProcesses) {
            for (ProcessStep step : process.getProcessSteps()) {
                if (!nodes.containsKey(step.getId()))
                    nodes.put(step.getId(), new TransitionNode(step.getId(), step.getAction()));
            }
        }

        for (BusinessProcess process : businessProcesses) {
            for (ProcessStepTransition transition : process.getTransitions()) {
                if (!edges.containsKey(transition.getFrom().getId()))
                    edges.put(transition.getFrom().getId(), new HashMap<>());

                if (!edges.get(transition.getFrom().getId()).containsKey(transition.getTo().getId()))
                    edges.get(transition.getFrom().getId()).put(
                            transition.getTo().getId(),
                            new TransitionEdge(
                                    nodes.get(transition.getFrom().getId()),
                                    nodes.get(transition.getTo().getId()),
                                    transition.getLabel()
                            )
                    );
            }
        }

        Set<TransitionNode> nodeSet = new HashSet<>(nodes.values());
        Set<TransitionEdge> edgeSet = new HashSet<>();
        for (Map<String, TransitionEdge> edgeMap : edges.values()) {
            edgeSet.addAll(edgeMap.values());
        }

        return new TransitionModel(nodeSet, edgeSet);
    }

    public static List<BusinessProcess> extractProcesses(TransitionModel transitionModel, int limit) {
        List<BusinessProcess> businessProcesses = new ArrayList<>();
        TransitionModel model = new TransitionModel(
                new HashSet<>(transitionModel.getTransitionNodes()),
                new HashSet<>(transitionModel.getTransitionsEdges())
        );

        Set<TransitionEdge> removeEdges = new HashSet<>();
        for (TransitionEdge edge : model.getTransitionsEdges()) {
            if (edge.getWeight() < limit) removeEdges.add(edge);
        }

        model.getTransitionsEdges().removeAll(removeEdges);


        while (model.getTransitionNodes().size() > 0) {
            BusinessProcess businessProcess = new BusinessProcess("Process-" + businessProcesses.size());

            //Adding first node and removing it from the model
            TransitionNode transitionNode = model.getTransitionNodes().iterator().next();
            model.getTransitionNodes().remove(transitionNode);
            Set<TransitionEdge> edges = new HashSet<>();
            Set<TransitionNode> nodes = new HashSet<>();
            nodes.add(transitionNode);

            boolean changes = true;
            while (changes) {
                changes = false;
                Set<TransitionEdge> edgesToBeRemoved = new HashSet<>();
                for (TransitionEdge edge : model.getTransitionsEdges()) {
                    if (nodes.contains(edge.getFrom()) || nodes.contains(edge.getTo())) {
                        edges.add(edge);
                        nodes.add(edge.getFrom());
                        nodes.add(edge.getTo());

                        model.getTransitionNodes().remove(edge.getFrom());
                        model.getTransitionNodes().remove(edge.getTo());
                        edgesToBeRemoved.add(edge);

                        changes = true;
                    }
                }
                model.getTransitionsEdges().removeAll(edgesToBeRemoved);
            }

            HashMap<String, ProcessStep> processStepHashMap = new HashMap<>();
            for (TransitionNode node : nodes) {
                ProcessStep step = new ProcessStep(
                        node.getId(),
                        node.getAction()
                );
                processStepHashMap.put(node.getId(), step);
                businessProcess.getProcessSteps().add(step);
            }
            for (TransitionEdge edge : edges) {
                businessProcess.getTransitions().add(new ProcessStepTransition(
                        processStepHashMap.get(edge.getFrom().getId()),
                        processStepHashMap.get(edge.getTo().getId()),
                        edge.getLabel()
                ));
            }

            businessProcesses.add(businessProcess);
        }


        return businessProcesses;
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
