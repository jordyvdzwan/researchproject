package utils;

import model.graph.Edge;
import model.graph.Graph;
import model.graph.Node;
import model.processMapComparison.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GraphComparisonUtils {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    public static GraphComparison compareGraphs(Graph original, Graph actual) {
        GraphComparison comparisonResult = new GraphComparison(original, actual);

        Map<String, Node> originalNodes = new HashMap<>();
        Set<Node> foundNodes = new HashSet<>();

        Set<Node> invalidNodes = new HashSet<>();
        Set<NodePair> correctNodes = new HashSet<>();
        Set<Node> missingNodes = new HashSet<>();

        Map<String, Edge> originalEdges = new HashMap<>();
        Set<Edge> foundEdges = new HashSet<>();

        Set<Edge> invalidEdges = new HashSet<>();
        Set<EdgePair> correctEdges = new HashSet<>();
        Set<Edge> missingEdges = new HashSet<>();


        for (Node node : original.getNodes()) {
            originalNodes.put(node.getId(), node);
        }
        for (Node node : actual.getNodes()) {
            if (originalNodes.containsKey(node.getId())) {
                correctNodes.add(new NodePair(originalNodes.get(node.getId()), node));
                foundNodes.add(originalNodes.get(node.getId()));
            } else {
                invalidNodes.add(node);
            }
        }
        for (Node node : originalNodes.values()) {
            if (!foundNodes.contains(node))
                missingNodes.add(node);
        }

        for (Edge edge : original.getEdges()) {
            originalEdges.put(edge.getFrom().getId() + "-" + edge.getTo().getId(), edge);
        }
        for (Edge edge : actual.getEdges()) {
            if (originalEdges.containsKey(edge.getFrom().getId() + "-" + edge.getTo().getId())) {
                correctEdges.add(new EdgePair(originalEdges.get(edge.getFrom().getId() + "-" + edge.getTo().getId()), edge) {
                });
                foundEdges.add(originalEdges.get(edge.getFrom().getId()+"-"+edge.getTo().getId()));
            } else {
                invalidEdges.add(edge);
            }
        }
        for (Edge edge : originalEdges.values()) {
            if (!foundEdges.contains(edge))
                missingEdges.add(edge);
        }

        comparisonResult.setCorrectEdges(correctEdges);
        comparisonResult.setCorrectNodes(correctNodes);
        comparisonResult.setInvalidEdges(invalidEdges);
        comparisonResult.setInvalidNodes(invalidNodes);
        comparisonResult.setMissingEdges(missingEdges);
        comparisonResult.setMissingNodes(missingNodes);

        return comparisonResult;
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
