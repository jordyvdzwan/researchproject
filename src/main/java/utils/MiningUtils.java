package utils;

import model.logs.kendo.KendoLog;
import model.logs.kendo.KendoLogRow;
import model.logs.model.Action;
import model.processModel.BusinessProcess;
import model.processModel.ProcessStep;
import model.processModel.ProcessStepTransition;
import model.traces.Trace;
import model.traces.TraceGraph;
import model.traces.TraceNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("Duplicates")
public class MiningUtils {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public static Set<BusinessProcess> extractProcessesWithErrorRemoval(TraceNode startNode, TraceGraph traceGraph, int trafficThreshold, int errorThreshold, int errorCooldown) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess = new HashMap<>();
        BusinessProcess startProcess = new BusinessProcess("p");
        startProcess.getProcessSteps().add(new ProcessStep(startNode.getAction()));
        tempTraceToBusinessProcess.put(startProcess, new HashMap<>());
        for (Trace trace : startNode.getTraceSetMap().keySet()) {
            tempTraceToBusinessProcess.get(startProcess).put(trace, startNode.getTraceSetMap().get(trace));
        }

        tempTraceToBusinessProcess = MiningUtils.traverseTracesWithErrorRemoval(tempTraceToBusinessProcess, trafficThreshold, errorThreshold, errorCooldown);
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = MiningUtils.retraverseTracesWithErrorRemoval(tempTraceToBusinessProcess, trafficThreshold, errorThreshold, errorCooldown);

        return finalBusinessProcessTraces.keySet();
    }


    public static Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traverseTracesWithErrorRemoval(Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess, int trafficThreshold, int errorThreshold, int errorCooldown) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = new HashMap<>();
        int offset = 0;
        boolean changing = true;
        Map<Trace, Map<KendoLogRow, Integer>> errorOffsets = new HashMap<>();
        Map<Trace, Map<KendoLogRow, Integer>> errorOffsetCooldown = new HashMap<>();

        //Going from the middle to the ends
        while (changing) {
            Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traceToBusinessProcess = tempTraceToBusinessProcess;
            tempTraceToBusinessProcess = new HashMap<>();
            changing = false;
            offset++;

            //Decrease all the error cooldowns
            for (Map<KendoLogRow, Integer> map : errorOffsetCooldown.values()) {
                for (KendoLogRow row : map.keySet()) {
                    if (map.get(row) > 0)
                        map.put(row, map.get(row) - 1);
                }
            }

            for (BusinessProcess process : traceToBusinessProcess.keySet()) {
                Map<Trace, Set<KendoLogRow>> traces = traceToBusinessProcess.get(process);
                Map<Action, Integer> nextSteps = new HashMap<>();
                Map<Action, Map<Trace, Set<KendoLogRow>>> nextStepsTraces = new HashMap<>();
                Map<Trace, Set<KendoLogRow>> potentialErrorTraces = new HashMap<>();
                Map<Trace, Set<KendoLogRow>> newTracesMap = new HashMap<>();

                int endHereCount = 0;
                for (Trace trace : traces.keySet()) {
                    Set<KendoLogRow> startPoints = traces.get(trace);
                    for (KendoLogRow row : startPoints) {

                        //Calculating the index of the next row using start position, offset and error offset
                        int errorOffset = errorOffsets.containsKey(trace) && errorOffsets.get(trace).containsKey(row)
                                ? errorOffsets.get(trace).get(row)
                                : 0;
                        int index = trace.getRows().indexOf(row) + offset + errorOffset;
                        if (index < trace.getRows().size()) {
                            Action currentAction = trace.getRows().get(index).toAction();
                            //Add nextSteps count
                            if (!nextSteps.containsKey(currentAction)) {
                                nextSteps.put(currentAction, 0);
                                nextStepsTraces.put(currentAction, new HashMap<>());
                            }
                            nextSteps.put(currentAction, nextSteps.get(currentAction) + 1);

                            //Adding new Log reference
                            Map<Trace, Set<KendoLogRow>> nextTraces = nextStepsTraces.get(currentAction);
                            if (!nextTraces.containsKey(trace)) {
                                nextTraces.put(trace, new HashSet<>());
                            }
                            nextTraces.get(trace).add(row);
                        } else {
                            if (!newTracesMap.containsKey(trace)) {
                                newTracesMap.put(trace, new HashSet<>());
                            }
                            newTracesMap.get(trace).add(row);
                            endHereCount++;
                        }
                    }
                }

                //Add the actions to the business processes
                Set<Action> removActions = new HashSet<>();
                for (Action action : nextSteps.keySet()) {
                    int count = nextSteps.get(action);
                    if (count >= trafficThreshold) {
                        BusinessProcess businessProcess = new BusinessProcess("p", process);
                        businessProcess.getProcessSteps().add(new ProcessStep(action));
                        businessProcess.getTransitions().add(new ProcessStepTransition(
                                businessProcess.getProcessSteps().get(businessProcess.getProcessSteps().size() - 2),
                                businessProcess.getProcessSteps().get(businessProcess.getProcessSteps().size() - 1),
                                ""
                        ));
                        changing = true;
                        tempTraceToBusinessProcess.put(businessProcess, nextStepsTraces.get(action));
                    } else {
                        for (Trace trace : nextStepsTraces.get(action).keySet()) {
                            for (KendoLogRow row : nextStepsTraces.get(action).get(trace)) {
                                if (errorOffsetCooldown.get(trace) == null ||
                                        errorOffsetCooldown.get(trace).get(row) == null ||
                                        errorOffsetCooldown.get(trace).get(row) == 0) {
                                    if (!potentialErrorTraces.containsKey(trace))
                                        potentialErrorTraces.put(trace, new HashSet<>());
                                    potentialErrorTraces.get(trace).add(row);
                                } else {
                                    // End when the cooldown has not yet expired
                                    endHereCount++;
                                }
                            }
                        }
                        removActions.add(action);
                    }
                }

                for (Action action : removActions) {
                    nextSteps.remove(action);
                }

                //Error skipping
                for (int errorIndex = 0; errorIndex < errorThreshold; errorIndex++) {
                    Set<KendoLogRow> rows = new HashSet<>();
                    Map<KendoLogRow, Trace> rowTraceMap = new HashMap<>();
                    Map<KendoLogRow, Action> rowActionMap = new HashMap<>();

                    //Increase error offsets
                    for (Trace trace : potentialErrorTraces.keySet()) {
                        for (KendoLogRow row : potentialErrorTraces.get(trace)) {
                            if (!errorOffsets.containsKey(trace))
                                errorOffsets.put(trace, new HashMap<>());
                            if (!errorOffsets.get(trace).containsKey(row))
                                errorOffsets.get(trace).put(row, 0);

                            errorOffsets.get(trace).put(row, errorOffsets.get(trace).get(row) + 1);

                            //Determine whether traces can join existing path and set cooldown
                            int errorOffset = errorOffsets.containsKey(trace) && errorOffsets.get(trace).containsKey(row)
                                    ? errorOffsets.get(trace).get(row)
                                    : 0;
                            int index = trace.getRows().indexOf(row) + offset + errorOffset;

                            if (index < trace.getRows().size()) {
                                Action currentAction = trace.getRows().get(index).toAction();
                                //Add nextSteps count
                                if (nextSteps.containsKey(currentAction) && nextStepsTraces.containsKey(currentAction)) {
                                    nextSteps.put(currentAction, nextSteps.get(currentAction) + 1);

                                    //Adding new Log reference
                                    Map<Trace, Set<KendoLogRow>> nextTraces = nextStepsTraces.get(currentAction);
                                    if (!nextTraces.containsKey(trace)) {
                                        nextTraces.put(trace, new HashSet<>());
                                    }
                                    nextTraces.get(trace).add(row);
                                    rows.add(row);
                                    rowTraceMap.put(row, trace);
                                }
                            } else {
                                endHereCount++;
                            }
                        }
                    }

                    //Remove all 'recovered' items and set the cooldown
                    for (KendoLogRow row : rows) {
                        potentialErrorTraces.get(rowTraceMap.get(row)).remove(row);
                        if (!errorOffsetCooldown.containsKey(rowTraceMap.get(row)))
                            errorOffsetCooldown.put(rowTraceMap.get(row), new HashMap<>());
                        errorOffsetCooldown.get(rowTraceMap.get(row)).put(row, errorCooldown);
                    }

                }

                //Check for end here count based on the nr of processes left with nowhere to go
                for (Trace trace : potentialErrorTraces.keySet()) {
                    endHereCount += potentialErrorTraces.get(trace).size();
                }

                //If the row stops than the process will stop here as well
                if (endHereCount >= trafficThreshold) {
                    changing = true;
                    for (Map<Trace, Set<KendoLogRow>> tracesIndex : nextStepsTraces.values()) {
                        for (Trace trace : tracesIndex.keySet()) {
                            if (!newTracesMap.containsKey(trace)) {
                                newTracesMap.put(trace, new HashSet<>());
                            }
                            newTracesMap.get(trace).addAll(tracesIndex.get(trace));
                        }
                    }
                    finalBusinessProcessTraces.put(process, newTracesMap);
                }
            }
        }
        return finalBusinessProcessTraces;
    }
    public static Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> retraverseTracesWithErrorRemoval(Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess, int trafficThreshold, int errorThreshold, int errorCooldown) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = new HashMap<>();
        int offset = 0;
        boolean changing = true;
        Map<Trace, Map<KendoLogRow, Integer>> errorOffsets = new HashMap<>();
        Map<Trace, Map<KendoLogRow, Integer>> errorOffsetCooldown = new HashMap<>();

        //Going from the middle to the ends
        while (changing) {
            Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traceToBusinessProcess = tempTraceToBusinessProcess;
            tempTraceToBusinessProcess = new HashMap<>();
            changing = false;
            offset--;

            //Decrease all the error cooldowns
            for (Map<KendoLogRow, Integer> map : errorOffsetCooldown.values()) {
                for (KendoLogRow row : map.keySet()) {
                    if (map.get(row) > 0)
                        map.put(row, map.get(row) - 1);
                }
            }

            for (BusinessProcess process : traceToBusinessProcess.keySet()) {
                Map<Trace, Set<KendoLogRow>> traces = traceToBusinessProcess.get(process);
                Map<Action, Integer> nextSteps = new HashMap<>();
                Map<Action, Map<Trace, Set<KendoLogRow>>> nextStepsTraces = new HashMap<>();
                Map<Trace, Set<KendoLogRow>> potentialErrorTraces = new HashMap<>();

                int endHereCount = 0;
                for (Trace trace : traces.keySet()) {
                    Set<KendoLogRow> startPoints = traces.get(trace);
                    for (KendoLogRow row : startPoints) {
                        //Calculating the index of the next row using start position, offset and error offset
                        int errorOffset = errorOffsets.containsKey(trace) && errorOffsets.get(trace).containsKey(row)
                                ? errorOffsets.get(trace).get(row)
                                : 0;
                        int index = trace.getRows().indexOf(row) + offset + errorOffset;

                        if (index >= 0) {
                            Action currentAction = trace.getRows().get(index).toAction();
                            //Add nextSteps count
                            if (!nextSteps.containsKey(currentAction)) {
                                nextSteps.put(currentAction, 0);
                                nextStepsTraces.put(currentAction, new HashMap<>());
                            }
                            nextSteps.put(currentAction, nextSteps.get(currentAction) + 1);

                            //Adding new Log reference
                            Map<Trace, Set<KendoLogRow>> nextTraces = nextStepsTraces.get(currentAction);
                            if (!nextTraces.containsKey(trace)) {
                                nextTraces.put(trace, new HashSet<>());
                            }
                            nextTraces.get(trace).add(row);
                        } else endHereCount++;
                    }
                }

                //Add the actions to the business processes
                Set<Action> removActions = new HashSet<>();
                for (Action action : nextSteps.keySet()) {
                    int count = nextSteps.get(action);
                    if (count >= trafficThreshold) {
                        BusinessProcess businessProcess = new BusinessProcess("p", process);
                        businessProcess.getProcessSteps().add(0, new ProcessStep(action));
                        businessProcess.getTransitions().add(new ProcessStepTransition(
                                businessProcess.getProcessSteps().get(0),
                                businessProcess.getProcessSteps().get(1),
                                ""
                        ));
                        changing = true;
                        tempTraceToBusinessProcess.put(businessProcess, nextStepsTraces.get(action));
                    } else {
                        for (Trace trace : nextStepsTraces.get(action).keySet()) {
                            for (KendoLogRow row : nextStepsTraces.get(action).get(trace)) {
                                if (errorOffsetCooldown.get(trace) == null ||
                                        errorOffsetCooldown.get(trace).get(row) == null ||
                                        errorOffsetCooldown.get(trace).get(row) == 0) {
                                    if (!potentialErrorTraces.containsKey(trace))
                                        potentialErrorTraces.put(trace, new HashSet<>());
                                    potentialErrorTraces.get(trace).add(row);
                                } else {
                                    // End when the cooldown has not yet expired
                                    endHereCount++;
                                }
                            }
                        }
                        removActions.add(action);
                    }
                }

                for (Action action : removActions) {
                    nextSteps.remove(action);
                }

                //Error skipping
                for (int errorIndex = 0; errorIndex < errorThreshold; errorIndex++) {
                    Set<KendoLogRow> rows = new HashSet<>();
                    Map<KendoLogRow, Trace> rowTraceMap = new HashMap<>();

                    //Increase error offsets
                    for (Trace trace : potentialErrorTraces.keySet()) {
                        for (KendoLogRow row : potentialErrorTraces.get(trace)) {
                            if (!errorOffsets.containsKey(trace))
                                errorOffsets.put(trace, new HashMap<>());
                            if (!errorOffsets.get(trace).containsKey(row))
                                errorOffsets.get(trace).put(row, 0);

                            errorOffsets.get(trace).put(row, errorOffsets.get(trace).get(row) - 1);

                            //Determine whether traces can join existing path and set cooldown
                            int errorOffset = errorOffsets.containsKey(trace) && errorOffsets.get(trace).containsKey(row)
                                    ? errorOffsets.get(trace).get(row)
                                    : 0;
                            int index = trace.getRows().indexOf(row) + offset + errorOffset;

                            if (index >= 0) {
                                Action currentAction = trace.getRows().get(index).toAction();
                                //Add nextSteps count
                                if (nextSteps.containsKey(currentAction) && nextStepsTraces.containsKey(currentAction)) {
                                    nextSteps.put(currentAction, nextSteps.get(currentAction) + 1);

                                    //Adding new Log reference
                                    if (!nextStepsTraces.get(currentAction).containsKey(trace)) {
                                        nextStepsTraces.get(currentAction).put(trace, new HashSet<>());
                                    }
                                    nextStepsTraces.get(currentAction).get(trace).add(row);
                                    rows.add(row);
                                    rowTraceMap.put(row, trace);
                                }
                            } else {
                                endHereCount++;
                            }
                        }
                    }

                    //Remove all 'recovered' items and set the cooldown
                    for (KendoLogRow row : rows) {
                        potentialErrorTraces.get(rowTraceMap.get(row)).remove(row);
                        if (!errorOffsetCooldown.containsKey(rowTraceMap.get(row)))
                            errorOffsetCooldown.put(rowTraceMap.get(row), new HashMap<>());
                        errorOffsetCooldown.get(rowTraceMap.get(row)).put(row, errorCooldown);
                    }

                }

                //Check for end here count based on the nr of processes left with nowhere to go
                for (Trace trace : potentialErrorTraces.keySet()) {
                    endHereCount += potentialErrorTraces.get(trace).size();
                }

                //If the row stops than the process will stop here as well
                if (endHereCount >= trafficThreshold) {
                    changing = true;
                    Map<Trace, Set<KendoLogRow>> newTracesMap = new HashMap<>();
                    for (Map<Trace, Set<KendoLogRow>> tracesIndex : nextStepsTraces.values()) {
                        for (Trace trace : tracesIndex.keySet()) {
                            if (!newTracesMap.containsKey(trace)) {
                                newTracesMap.put(trace, new HashSet<>());
                            }
                            newTracesMap.get(trace).addAll(tracesIndex.get(trace));
                        }
                    }
                    finalBusinessProcessTraces.put(process, newTracesMap);
                }
            }
        }
        return finalBusinessProcessTraces;
    }


    public static Set<BusinessProcess> extractProcesses(TraceNode startNode, TraceGraph traceGraph, int trafficThreshold) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess = new HashMap<>();
        BusinessProcess startProcess = new BusinessProcess("p");
        startProcess.getProcessSteps().add(new ProcessStep(startNode.getAction()));
        tempTraceToBusinessProcess.put(startProcess, new HashMap<>());
        for (Trace trace : startNode.getTraceSetMap().keySet()) {
            tempTraceToBusinessProcess.get(startProcess).put(trace, startNode.getTraceSetMap().get(trace));
        }

        tempTraceToBusinessProcess = MiningUtils.traverseTraces(tempTraceToBusinessProcess, trafficThreshold);
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = MiningUtils.retraverseTraces(tempTraceToBusinessProcess, trafficThreshold);

        return finalBusinessProcessTraces.keySet();
    }
    public static Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traverseTraces(Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess, int trafficThreshold) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = new HashMap<>();
        int offset = 0;
        boolean changing = true;
        //Going from the middle to the ends
        while (changing) {
            Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traceToBusinessProcess = tempTraceToBusinessProcess;
            tempTraceToBusinessProcess = new HashMap<>();
            changing = false;
            offset++;
            for (BusinessProcess process : traceToBusinessProcess.keySet()) {
                Map<Trace, Set<KendoLogRow>> traces = traceToBusinessProcess.get(process);
                Map<Action, Integer> nextSteps = new HashMap<>();
                Map<Action, Map<Trace, Set<KendoLogRow>>> nextStepsTraces = new HashMap<>();
                Map<Trace, Set<KendoLogRow>> newTracesMap = new HashMap<>();
                int endHereCount = 0;
                for (Trace trace : traces.keySet()) {
                    Set<KendoLogRow> startPoints = traces.get(trace);
                    for (KendoLogRow row : startPoints) {
                        int index = trace.getRows().indexOf(row) + offset;
                        if (index < trace.getRows().size()) {
                            Action currentAction = trace.getRows().get(index).toAction();
                            //Add nextSteps count
                            if (!nextSteps.containsKey(currentAction)) {
                                nextSteps.put(currentAction, 0);
                                nextStepsTraces.put(currentAction, new HashMap<>());
                            }
                            nextSteps.put(currentAction, nextSteps.get(currentAction) + 1);

                            //Adding new Log reference
                            Map<Trace, Set<KendoLogRow>> nextTraces = nextStepsTraces.get(currentAction);
                            if (!nextTraces.containsKey(trace)) {
                                nextTraces.put(trace, new HashSet<>());
                            }
                            nextTraces.get(trace).add(row);
                        } else {
                            if (!newTracesMap.containsKey(trace)) {
                                newTracesMap.put(trace, new HashSet<>());
                            }
                            newTracesMap.get(trace).add(row);

                            endHereCount++;
                        }
                    }
                }

                for (Action action : nextSteps.keySet()) {
                    int count = nextSteps.get(action);
                    if (count >= trafficThreshold) {
                        BusinessProcess businessProcess = new BusinessProcess("p", process);
                        businessProcess.getProcessSteps().add(new ProcessStep(action));
                        businessProcess.getTransitions().add(new ProcessStepTransition(
                                businessProcess.getProcessSteps().get(businessProcess.getProcessSteps().size() - 2),
                                businessProcess.getProcessSteps().get(businessProcess.getProcessSteps().size() - 1),
                                ""
                        ));
                        changing = true;
                        tempTraceToBusinessProcess.put(businessProcess, nextStepsTraces.get(action));
                        nextStepsTraces.remove(action);
                    } else
                        endHereCount += count;
                }

                //If the row stops than the process will stop here as well
                if (endHereCount >= trafficThreshold) {
                    changing = true;
                    finalBusinessProcessTraces.put(process, newTracesMap);
                }
            }
        }
        return finalBusinessProcessTraces;
    }
    public static Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> retraverseTraces(Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess, int trafficThreshold) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = new HashMap<>();
        int offset = 0;
        boolean changing = true;
        //Going from the middle to the ends
        while (changing) {
            Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traceToBusinessProcess = tempTraceToBusinessProcess;
            tempTraceToBusinessProcess = new HashMap<>();
            changing = false;
            offset--;
            for (BusinessProcess process : traceToBusinessProcess.keySet()) {
                Map<Trace, Set<KendoLogRow>> traces = traceToBusinessProcess.get(process);
                Map<Action, Integer> nextSteps = new HashMap<>();
                Map<Action, Map<Trace, Set<KendoLogRow>>> nextStepsTraces = new HashMap<>();
                int endHereCount = 0;
                for (Trace trace : traces.keySet()) {
                    Set<KendoLogRow> startPoints = traces.get(trace);
                    for (KendoLogRow row : startPoints) {
                        int index = trace.getRows().indexOf(row) + offset;
                        if (index >= 0) {
                            Action currentAction = trace.getRows().get(index).toAction();
                            //Add nextSteps count
                            if (!nextSteps.containsKey(currentAction)) {
                                nextSteps.put(currentAction, 0);
                                nextStepsTraces.put(currentAction, new HashMap<>());
                            }
                            nextSteps.put(currentAction, nextSteps.get(currentAction) + 1);

                            //Adding new Log reference
                            Map<Trace, Set<KendoLogRow>> nextTraces = nextStepsTraces.get(currentAction);
                            if (!nextTraces.containsKey(trace)) {
                                nextTraces.put(trace, new HashSet<>());
                            }
                            nextTraces.get(trace).add(row);
                        } else endHereCount++;
                    }
                }

                for (Action action : nextSteps.keySet()) {
                    int count = nextSteps.get(action);
                    if (count >= trafficThreshold) {
                        BusinessProcess businessProcess = new BusinessProcess("p", process);
                        businessProcess.getProcessSteps().add(0, new ProcessStep(action));
                        businessProcess.getTransitions().add(new ProcessStepTransition(
                                businessProcess.getProcessSteps().get(0),
                                businessProcess.getProcessSteps().get(1),
                                ""
                        ));
                        changing = true;
                        tempTraceToBusinessProcess.put(businessProcess, nextStepsTraces.get(action));
                        nextStepsTraces.remove(action);
                    } else endHereCount += count;
                }

                //If the row stops than the process will stop here as well
                if (endHereCount >= trafficThreshold) {
                    changing = true;
                    Map<Trace, Set<KendoLogRow>> newTracesMap = new HashMap<>();
                    for (Map<Trace, Set<KendoLogRow>> tracesIndex : nextStepsTraces.values()) {
                        for (Trace trace : tracesIndex.keySet()) {
                            if (!newTracesMap.containsKey(trace)) {
                                newTracesMap.put(trace, new HashSet<>());
                            }
                            newTracesMap.get(trace).addAll(tracesIndex.get(trace));
                        }
                    }
                    finalBusinessProcessTraces.put(process, newTracesMap);
                }
            }
        }
        return finalBusinessProcessTraces;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
