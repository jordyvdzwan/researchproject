package controller.tracers;

import model.logs.kendo.KendoLog;
import model.logs.kendo.KendoLogRow;
import model.logs.model.Action;
import model.logs.model.NovSession;
import model.traces.Trace;
import model.traces.TraceSet;

import java.util.HashMap;
import java.util.Map;

public class MainRecordIdTraceBuilder extends TraceBuilder {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private Map<Integer, String> idMap = null;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public TraceSet build(KendoLog kendoLog, String[] blacklist) {
        Map<Integer, Trace> traceMap = new HashMap<>();
        Map<String, Action> actionMap = new HashMap<>();

        for (KendoLogRow row : kendoLog.getRows()) {
            if (row.getMainrecordid() != null && !inBlacklist(row.getAction().getName(), blacklist)) {
                if (!traceMap.containsKey(row.getMainrecordid()))
                    traceMap.put(row.getMainrecordid(), new Trace());

                String name = row.getDisplayType() + " " + (
                        idMap != null && idMap.containsKey(row.getPageid()) ?
                                idMap.get(row.getPageid()) :
                                row.getPageid()
                );
                if (!actionMap.containsKey(name))
                    actionMap.put(name, new Action(name, row.getPageid(), row.getDisplayType()));

                KendoLogRow kendoLogRow = new KendoLogRow(row);
                kendoLogRow.setAction(actionMap.get(name));
                row.setTrace(traceMap.get(row.getMainrecordid()));
                traceMap.get(row.getMainrecordid()).getRows().add(kendoLogRow);
            }
        }

        TraceSet result = new TraceSet();
        result.getTraces().addAll(traceMap.values());
        return result;
    }

    @Override
    public String toString() {
        return "Main Record Id Trace Builder";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public Map<Integer, String> getIdMap() {
        return idMap;
    }

    public void setIdMap(Map<Integer, String> idMap) {
        this.idMap = idMap;
    }

}
