package controller.tracers.processes;

import model.processModel.BusinessProcess;
import model.processModel.ProcessStep;
import model.processModel.ProcessStepTransition;
import model.traces.BusinessProcessTrace;

import java.util.*;

public class ProcessStepTracer {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public List<BusinessProcessTrace> determineBusinessProcessTraces(List<BusinessProcess> processes) {
        List<BusinessProcessTrace> traces = new ArrayList<>();

        for (BusinessProcess process : processes) {
            //Initializing the nextSteps
            //     trace                  from             to            list of visited steps
            Map<List<ProcessStep>, Map<ProcessStep, Map<ProcessStep, Set<ProcessStepTransition>>>> visitedSteps = new HashMap<>();
            Map<ProcessStep, Set<ProcessStep>> transitionPosibilities = new HashMap<>();
            Map<ProcessStep, Map<ProcessStep, ProcessStepTransition>> transitions = new HashMap<>();
            Set<List<ProcessStep>> traceLists = new HashSet<>();

            for (ProcessStepTransition transition : process.getTransitions()) {
                transitionPosibilities.putIfAbsent(transition.getFromStep(), new HashSet<>());
                transitionPosibilities.get(transition.getFromStep()).add(transition.getToStep());
                transitions.putIfAbsent(transition.getFromStep(), new HashMap<>());
                transitions.get(transition.getFromStep()).put(transition.getToStep(), new ProcessStepTransition(transition.getFromStep(), transition.getToStep(), ""));
            }
            for (ProcessStep startNode : process.getStartSteps()) {
                List<ProcessStep> startTrace = new ArrayList<ProcessStep>(){{add(startNode);}};
                traceLists.add(startTrace);
                visitedSteps.put(startTrace, new HashMap<>());
            }

            boolean changing = true;
            while (changing) {
                changing = false;
                Set<List<ProcessStep>> tempTraceLists = traceLists;
                traceLists = new HashSet<>();

                for (List<ProcessStep> trace : tempTraceLists) {
                    if (transitionPosibilities.get(trace.get(trace.size() - 1)) != null) {
                        for (ProcessStep nextPosibility : transitionPosibilities.get(trace.get(trace.size() - 1))) {
                            List<ProcessStep> newTrace = new ArrayList<>(trace);
                            newTrace.add(nextPosibility);

                            ProcessStep from = trace.get(trace.size() - 1);
                            ProcessStep to = newTrace.get(newTrace.size() - 1);

                            if (visitedSteps.get(trace).get(from) == null
                                    || visitedSteps.get(trace).get(from).get(to) == null
                                    || !visitedSteps.get(trace).get(from).get(to).containsAll(toTransitions(trace, transitions))) {
                                changing = true;

                                visitedSteps.put(newTrace, new HashMap<>());
                                for (ProcessStep fromStep : visitedSteps.get(trace).keySet()) {
                                    for (ProcessStep toStep : visitedSteps.get(trace).get(fromStep).keySet()) {
                                        visitedSteps.get(newTrace).putIfAbsent(fromStep, new HashMap<>());
                                        visitedSteps.get(newTrace).get(fromStep).putIfAbsent(toStep, new HashSet<>());
                                        visitedSteps.get(newTrace).get(fromStep).get(toStep).addAll(visitedSteps.get(trace).get(fromStep).get(toStep));
                                    }
                                }
                                visitedSteps.get(newTrace).putIfAbsent(from, new HashMap<>());
                                visitedSteps.get(newTrace).get(from).putIfAbsent(to, new HashSet<>());
                                visitedSteps.get(newTrace).get(from).put(to, toTransitions(newTrace, transitions));

                                if (process.getEndSteps().contains(to)) {
                                    BusinessProcessTrace businessProcessTrace = new BusinessProcessTrace();
                                    businessProcessTrace.getSteps().addAll(newTrace);
                                    traces.add(businessProcessTrace);
                                } else {
                                    traceLists.add(newTrace);
                                }
                            }
                        }
                    }
                }
            }
        }
        return traces;
    }

    private Set<ProcessStepTransition> toTransitions(List<ProcessStep> steps, Map<ProcessStep, Map<ProcessStep, ProcessStepTransition>> transitionsMap) {
        Set<ProcessStepTransition> transitions = new HashSet<>();

        for (int i = 1; i < steps.size(); i++) {
            transitions.add(transitionsMap.get(steps.get(i - 1)).get(steps.get(i)));
        }

        return transitions;
    }











//    public List<BusinessProcessTrace> determineBusinessProcessTraces(List<BusinessProcess> processes) {
//        List<BusinessProcessTrace> traces = new ArrayList<>();
//
//        for (BusinessProcess process : processes) {
//            //Initializing the nextSteps
//            Map<ProcessStep, List<List<ProcessStep>>> nextSteps = new HashMap<>();
//            for (ProcessStep step : process.getStartSteps()) {
//                nextSteps.put(step, new ArrayList<List<ProcessStep>>(){{
//                    add(new ArrayList<ProcessStep>(){{
//                        add(step);}
//                    });
//                }});
//            }
//
//            //If new edges have been added try to set another step.
//            boolean newEdge = true;
//            Map<List<ProcessStep>, List<ProcessStepTransition>> visitedTransitions = new HashMap<>();
//            while (newEdge) {
//                Map<ProcessStep, List<List<ProcessStep>>> tempNextSteps = new HashMap<>();
//                newEdge = false;
//                for (ProcessStepTransition transition : process.getTransitions()) {
//                    if (nextSteps.containsKey(transition.getFromStep())) {
//
//                        List<List<ProcessStep>> processTraces = nextSteps.get(transition.getFromStep());
//                        ProcessStep nextNode = transition.getToStep();
//
//                        if (process.getEndSteps().contains(nextNode)) {
//                            for (List<ProcessStep> processTrace : processTraces) {
//                                BusinessProcessTrace trace = new BusinessProcessTrace();
//                                trace.getSteps().addAll(processTrace);
//                                trace.getSteps().add(nextNode);
//                                traces.add(trace);
//                                newEdge = true;
//                            }
//                        } else {
//                            if (!tempNextSteps.containsKey(nextNode))
//                                tempNextSteps.put(nextNode, new ArrayList<>());
//
//                            for (List<ProcessStep> processTrace : processTraces) {
//                                if (processTrace.size() <= 1 || !alreadyVisited(processTrace, transition, visitedTransitions)) {
//                                    List<ProcessStep> newProcessTrace = new ArrayList<>(processTrace);
//                                    newProcessTrace.add(nextNode);
//                                    tempNextSteps.get(nextNode).add(newProcessTrace);
//                                    newEdge = true;
//
//                                    if (visitedTransitions.get(processTrace) == null) {
//                                        List<ProcessStepTransition> newTransitions = new ArrayList<>();
//                                        newTransitions.add(transition);
//                                        visitedTransitions.put(newProcessTrace, newTransitions);
//                                    } else {
//                                        List<ProcessStepTransition> newTransitions = new ArrayList<>(visitedTransitions.get(processTrace));
//                                        newTransitions.add(transition);
//                                        visitedTransitions.put(newProcessTrace, newTransitions);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//
//                nextSteps = tempNextSteps;
//            }
//        }
//
//        return traces;
//    }
//
//    private boolean alreadyVisited(List<ProcessStep> processTrace, ProcessStepTransition transition, Map<List<ProcessStep>, List<ProcessStepTransition>> visitedTransitions) {
//        for (List<ProcessStep> processSteps : visitedTransitions.keySet()) {
//            if (visitedTransitions.get(processTrace).contains(transition)
//                    && Collections.indexOfSubList(processTrace, processSteps) != Collections.lastIndexOfSubList(processTrace, processSteps)) return true;
//        }
//        return false;
//    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
