package controller.tracers;

import model.logs.kendo.KendoLog;
import model.traces.TraceSet;

import java.util.Map;

public abstract class TraceBuilder {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/


    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public abstract TraceSet build(KendoLog kendoLog, String[] blacklist);

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public abstract Map<Integer, String> getIdMap();
    public abstract void setIdMap(Map<Integer, String> idMap);

    public boolean inBlacklist(String name, String[] blacklist) {
        for (String blacklistItem : blacklist) {
            if (name.contains(blacklistItem)) return true;
        }
        return false;
    }

}
