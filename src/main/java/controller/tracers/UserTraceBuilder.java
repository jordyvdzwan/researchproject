package controller.tracers;

import model.logs.kendo.KendoLog;
import model.logs.kendo.KendoLogRow;
import model.logs.model.NovSession;
import model.traces.Trace;
import model.traces.TraceSet;

import java.util.HashMap;
import java.util.Map;

public class UserTraceBuilder extends TraceBuilder {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private Map<Integer, String> idMap = null;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public TraceSet build(KendoLog kendoLog, String[] blacklist) {
        Map<Integer, Trace> traceMap = new HashMap<>();

        for (KendoLogRow row : kendoLog.getRows()) {
            if (!inBlacklist(row.getAction().getName(), blacklist)) {
                if (!traceMap.containsKey(row.getUser()))
                    traceMap.put(row.getUser(), new Trace());
                row.setTrace(traceMap.get(row.getUser()));
                traceMap.get(row.getUser()).getRows().add(row);
            }
        }

        TraceSet result = new TraceSet();
        result.getTraces().addAll(traceMap.values());
        return result;
    }

    @Override
    public String toString() {
        return "User Trace Builder";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public Map<Integer, String> getIdMap() {
        return idMap;
    }

    public void setIdMap(Map<Integer, String> idMap) {
        this.idMap = idMap;
    }

}
