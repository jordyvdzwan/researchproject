package controller.processMapMiningTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.tracers.TraceBuilder;
import model.logs.kendo.KendoLogRow;
import model.logs.model.Action;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import model.traces.Trace;
import model.traces.TraceGraph;
import model.traces.TraceNode;
import model.traces.TraceSet;
import utils.MiningUtils;
import utils.TextUtils;
import utils.TransitionModelUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ERMTFProcessMapMiningTask extends ProcessMapMiningTask {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    

    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected ProcessMap call() throws Exception {
        updateStatus("Checking parameters");
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        if (!getParameters().filledIn())
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});

        ProcessMap processMap = new ProcessMap("ERMFT Mined Process Map " + TextUtils.getCurrentDataTime());
        TraceBuilder traceBuilder = getParameters().getTraceBuilder();
        if (getParameters().getDictionaryFile() != null)
            traceBuilder.setIdMap(TextUtils.readIDRegistry(getParameters().getDictionaryFile()));
        TraceSet traceSet = traceBuilder.build(getParameters().getKendoLog(), getParameters().getTracerBlacklist());
        TraceGraph traceGraph = new TraceGraph();

        updateStatus("Building Trace Graph (" + traceSet.getTraces().size() + " traces)");
        int index = 0;
        for (Trace trace : traceSet.getTraces()) {
            if (isCancelled()) return null;
            updateProgress(index++, traceSet.getTraces().size());
            List<KendoLogRow> rows = trace.getRows();
            if (rows.size() > 0) {
                KendoLogRow kendoLogRow = rows.get(0);
                traceGraph.addTraceNode(trace, kendoLogRow);
            }
            for (int i = 1; i < rows.size(); i++) {
                KendoLogRow prevKendoLogRow = rows.get(i - 1);
                KendoLogRow kendoLogRow = rows.get(i);
                traceGraph.addTraceNode(trace, kendoLogRow);
                //traceGraph.addTraceEdge(trace, prevKendoLogRow, kendoLogRow);
            }
        }

        Set<BusinessProcess> tempBusinessProcesses;
        Set<BusinessProcess> businessProcesses = new HashSet<>();
        updateStatus("Building Processes (" + traceGraph.getTraceNodes().keySet().size() + " actions)");
        index = 0;
        for (Action action : traceGraph.getTraceNodes().keySet()) {
            if (isCancelled()) return null;
            TraceNode startNode = traceGraph.getTraceNodes().get(action);
            updateProgress(index++, traceGraph.getTraceNodes().size());
            businessProcesses.addAll(MiningUtils.extractProcessesWithErrorRemoval(
                    startNode,
                    traceGraph,
                    getParameters().getTrafficThreshold(),
                    getParameters().getErrorThreshold(),
                    getParameters().getErrorCooldown())
            );
        }

        updateStatus("Filtering processes (" + businessProcesses.size() + " processes)");
        tempBusinessProcesses = businessProcesses;
        businessProcesses = new HashSet<>();
        index = 0;
        for (BusinessProcess process : tempBusinessProcesses) {
            if (isCancelled()) return null;
            updateProgress(index++, tempBusinessProcesses.size());
            if (process.getProcessSteps().size() > getParameters().getNodesThresHold()) businessProcesses.add(process);
        }

        updateStatus("Deleting duplicates (" + businessProcesses.size() + " processes)");
        tempBusinessProcesses = businessProcesses;
        businessProcesses = new HashSet<>();
        index = 0;
        for (BusinessProcess process : tempBusinessProcesses) {
            if (isCancelled()) return null;
            updateProgress(index++, tempBusinessProcesses.size());
            if (!process.containsEqualValue(businessProcesses)) businessProcesses.add(process);
        }

        for (int i = 0; i < 5; i++) {
            updateStatus("Merging processes (" + businessProcesses.size() + " processes)");
            tempBusinessProcesses = businessProcesses;
            businessProcesses = new HashSet<>();
            index = 0;
            for (BusinessProcess process : tempBusinessProcesses) {
                if (isCancelled()) return null;
                updateProgress(index++, tempBusinessProcesses.size());

                boolean merged = false;
                for (BusinessProcess finalProcess : businessProcesses) {
                    if (process.getMergingMissingPercentage(finalProcess) < getParameters().getMergeThreshold()) {
                        finalProcess.mergeProcess(process);
                        merged = true;
                    }
                }
                if (!merged) {
                    businessProcesses.add(process);
                }
            }

            updateStatus("Deleting duplicates (" + businessProcesses.size() + " processes)");
            tempBusinessProcesses = businessProcesses;
            businessProcesses = new HashSet<>();
            index = 0;
            for (BusinessProcess process : tempBusinessProcesses) {
                if (isCancelled()) return null;
                updateProgress(index++, tempBusinessProcesses.size());
                if (!process.containsEqualValue(businessProcesses)) businessProcesses.add(process);
            }
        }

        updateStatus("Detecting start and end nodes (" + businessProcesses.size() + " processes)");
        index = 0;
        for (BusinessProcess process : businessProcesses) {
            if (isCancelled()) return null;
            updateProgress(index++, businessProcesses.size());
            process.detectStartEndNodes();
        }

        updateStatus("Setting new names (" + businessProcesses.size() + " processes)");
        index = 0;
        for (BusinessProcess process : businessProcesses) {
            if (isCancelled()) return null;
            updateProgress(index++, businessProcesses.size());
            process.setName("Process-" +  index);
        }
        processMap.getBusinessProcesses().addAll(businessProcesses);

        updateStatus("Creating transition model");
        if (!isCancelled()) processMap.setTransitionModel(TransitionModelUtils.generateFromProcesses(processMap.getBusinessProcesses()));

        taskFinished(true, "Done");
        return processMap;
    }




    @Override
    public String toString() {
        return "ERMTF Process Map Miner";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

}
