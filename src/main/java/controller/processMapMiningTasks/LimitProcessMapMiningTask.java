package controller.processMapMiningTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.transitionMapBuilderTasks.SimpleTransitionModelBuilder;
import model.processModel.ProcessMap;
import model.transitionModel.TransitionModel;
import utils.TextUtils;
import utils.TransitionModelUtils;

public class LimitProcessMapMiningTask extends ProcessMapMiningTask {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    

    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected ProcessMap call() throws Exception {
        updateStatus("Checking parameters");
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        if (!getParameters().filledIn())
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});

        ProcessMap processMap = new ProcessMap("Limit Mined Process Map " + TextUtils.getCurrentDataTime());
        SimpleTransitionModelBuilder task = new SimpleTransitionModelBuilder();
        TransitionModel transitionModel = task.getTransitionModel(getParameters().getKendoLog());
        processMap.getBusinessProcesses().addAll(TransitionModelUtils.extractProcesses(transitionModel, getParameters().getTrafficThreshold()));
        processMap.setTransitionModel(TransitionModelUtils.generateFromProcesses(processMap.getBusinessProcesses()));

        return processMap;
    }

    @Override
    public String toString() {
        return "Limit Process Map Miner";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
