package controller.processMapMiningTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.tracers.TraceBuilder;
import model.logs.kendo.KendoLogRow;
import model.logs.model.Action;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import model.processModel.ProcessStep;
import model.processModel.ProcessStepTransition;
import model.traces.Trace;
import model.traces.TraceGraph;
import model.traces.TraceNode;
import model.traces.TraceSet;
import utils.TextUtils;
import utils.TransitionModelUtils;

import java.util.*;

public class TraceFrequencyProcessMapMiningTask extends ProcessMapMiningTask {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    

    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected ProcessMap call() throws Exception {
        updateStatus("Checking parameters");
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        if (!getParameters().filledIn())
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});

        ProcessMap processMap = new ProcessMap("Trace Frequency Mined Process Map " + TextUtils.getCurrentDataTime());
        TraceBuilder traceBuilder = getParameters().getTraceBuilder();
        if (getParameters().getDictionaryFile() != null) traceBuilder.setIdMap(TextUtils.readIDRegistry(getParameters().getDictionaryFile()));
        TraceSet traceSet = traceBuilder.build(getParameters().getKendoLog(), getParameters().getTracerBlacklist());
        TraceGraph traceGraph = new TraceGraph();

        updateStatus("Building Trace Graph (" + traceSet.getTraces().size() + " traces)");
        int index = 0;
        for (Trace trace : traceSet.getTraces()) {
            if (isCancelled()) return null;
            updateProgress(index++, traceSet.getTraces().size());
            List<KendoLogRow> rows = trace.getRows();
            if (rows.size() > 0) {
                KendoLogRow kendoLogRow = rows.get(0);
                traceGraph.addTraceNode(trace, kendoLogRow);
            }
            for (int i = 1; i < rows.size(); i++) {
                KendoLogRow prevKendoLogRow = rows.get(i - 1);
                KendoLogRow kendoLogRow = rows.get(i);
                traceGraph.addTraceNode(trace, kendoLogRow);
                //traceGraph.addTraceEdge(trace, prevKendoLogRow, kendoLogRow);
            }
        }

        Set<BusinessProcess> tempBusinessProcesses;
        Set<BusinessProcess> businessProcesses = new HashSet<>();
        updateStatus("Building Processes (" + traceGraph.getTraceNodes().keySet().size() + " actions)");
        index = 0;
        for (Action action : traceGraph.getTraceNodes().keySet()) {
            if (isCancelled()) return null;
            TraceNode startNode = traceGraph.getTraceNodes().get(action);
            updateProgress(index++, traceGraph.getTraceNodes().size());
            businessProcesses.addAll(extractProcesses(startNode, traceGraph));
        }

        updateStatus("Filtering processes (" + businessProcesses.size() + " processes)");
        tempBusinessProcesses = businessProcesses;
        businessProcesses = new HashSet<>();
        index = 0;
        for (BusinessProcess process : tempBusinessProcesses) {
            if (isCancelled()) return null;
            updateProgress(index++, tempBusinessProcesses.size());
            if (process.getProcessSteps().size() > getParameters().getNodesThresHold()) businessProcesses.add(process);
        }

        updateStatus("Deleting duplicates (" + businessProcesses.size() + " processes)");
        tempBusinessProcesses = businessProcesses;
        businessProcesses = new HashSet<>();
        index = 0;
        for (BusinessProcess process : tempBusinessProcesses) {
            if (isCancelled()) return null;
            updateProgress(index++, tempBusinessProcesses.size());
            if (!process.containsEqualValue(businessProcesses)) businessProcesses.add(process);
        }

        updateStatus("Detecting start and end nodes (" + businessProcesses.size() + " processes)");
        index = 0;
        for (BusinessProcess process : businessProcesses) {
            if (isCancelled()) return null;
            updateProgress(index++, businessProcesses.size());
            process.detectStartEndNodes();
        }
        processMap.getBusinessProcesses().addAll(businessProcesses);

        updateStatus("Creating transition model");
        if (!isCancelled()) processMap.setTransitionModel(TransitionModelUtils.generateFromProcesses(processMap.getBusinessProcesses()));

        taskFinished(true, "Done");
        return processMap;
    }

    private Set<BusinessProcess> extractProcesses(TraceNode startNode, TraceGraph traceGraph) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess = new HashMap<>();
        BusinessProcess startProcess = new BusinessProcess("p");
        startProcess.getProcessSteps().add(new ProcessStep(startNode.getAction()));
        tempTraceToBusinessProcess.put(startProcess, new HashMap<>());
        for (Trace trace : startNode.getTraceSetMap().keySet()) {
            tempTraceToBusinessProcess.get(startProcess).put(trace, startNode.getTraceSetMap().get(trace));
        }

        tempTraceToBusinessProcess = traverseTraces(tempTraceToBusinessProcess);
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = retraverseTraces(tempTraceToBusinessProcess);

        return finalBusinessProcessTraces.keySet();
    }
    private Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traverseTraces(Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = new HashMap<>();
        int offset = 0;
        boolean changing = true;
        //Going from the middle to the ends
        while (changing) {
            Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traceToBusinessProcess = tempTraceToBusinessProcess;
            tempTraceToBusinessProcess = new HashMap<>();
            changing = false;
            offset++;
            for (BusinessProcess process : traceToBusinessProcess.keySet()) {
                Map<Trace, Set<KendoLogRow>> traces = traceToBusinessProcess.get(process);
                Map<Action, Integer> nextSteps = new HashMap<>();
                Map<Action, Map<Trace, Set<KendoLogRow>>> nextStepsTraces = new HashMap<>();
                Map<Trace, Set<KendoLogRow>> newTracesMap = new HashMap<>();
                int endHereCount = 0;
                for (Trace trace : traces.keySet()) {
                    Set<KendoLogRow> startPoints = traces.get(trace);
                    for (KendoLogRow row : startPoints) {
                        int index = trace.getRows().indexOf(row) + offset;
                        if (index < trace.getRows().size()) {
                            Action currentAction = trace.getRows().get(index).toAction();
                            //Add nextSteps count
                            if (!nextSteps.containsKey(currentAction)) {
                                nextSteps.put(currentAction, 0);
                                nextStepsTraces.put(currentAction, new HashMap<>());
                            }
                            nextSteps.put(currentAction, nextSteps.get(currentAction) + 1);

                            //Adding new Log reference
                            Map<Trace, Set<KendoLogRow>> nextTraces = nextStepsTraces.get(currentAction);
                            if (!nextTraces.containsKey(trace)) {
                                nextTraces.put(trace, new HashSet<>());
                            }
                            nextTraces.get(trace).add(row);
                        } else {
                            if (!newTracesMap.containsKey(trace)) {
                                newTracesMap.put(trace, new HashSet<>());
                            }
                            newTracesMap.get(trace).add(row);

                            endHereCount++;
                        }
                    }
                }

                for (Action action : nextSteps.keySet()) {
                    int count = nextSteps.get(action);
                    if (count >= getParameters().getTrafficThreshold()) {
                        BusinessProcess businessProcess = new BusinessProcess("p", process);
                        businessProcess.getProcessSteps().add(new ProcessStep(action));
                        businessProcess.getTransitions().add(new ProcessStepTransition(
                                businessProcess.getProcessSteps().get(businessProcess.getProcessSteps().size() - 2),
                                businessProcess.getProcessSteps().get(businessProcess.getProcessSteps().size() - 1),
                                ""
                        ));
                        changing = true;
                        tempTraceToBusinessProcess.put(businessProcess, nextStepsTraces.get(action));
                        nextStepsTraces.remove(action);
                    } else endHereCount += count;
                }

                //If the row stops than the process will stop here as well
                if (endHereCount >= getParameters().getTrafficThreshold()) {
                    changing = true;
                    finalBusinessProcessTraces.put(process, newTracesMap);
                }
            }
        }
        return finalBusinessProcessTraces;
    }
    private Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> retraverseTraces(Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> tempTraceToBusinessProcess) {
        Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> finalBusinessProcessTraces = new HashMap<>();
        int offset = 0;
        boolean changing = true;
        //Going from the middle to the ends
        while (changing) {
            Map<BusinessProcess, Map<Trace, Set<KendoLogRow>>> traceToBusinessProcess = tempTraceToBusinessProcess;
            tempTraceToBusinessProcess = new HashMap<>();
            changing = false;
            offset--;
            for (BusinessProcess process : traceToBusinessProcess.keySet()) {
                Map<Trace, Set<KendoLogRow>> traces = traceToBusinessProcess.get(process);
                Map<Action, Integer> nextSteps = new HashMap<>();
                Map<Action, Map<Trace, Set<KendoLogRow>>> nextStepsTraces = new HashMap<>();
                int endHereCount = 0;
                for (Trace trace : traces.keySet()) {
                    Set<KendoLogRow> startPoints = traces.get(trace);
                    for (KendoLogRow row : startPoints) {
                        int index = trace.getRows().indexOf(row) + offset;
                        if (index >= 0) {
                            Action currentAction = trace.getRows().get(index).toAction();
                            //Add nextSteps count
                            if (!nextSteps.containsKey(currentAction)) {
                                nextSteps.put(currentAction, 0);
                                nextStepsTraces.put(currentAction, new HashMap<>());
                            }
                            nextSteps.put(currentAction, nextSteps.get(currentAction) + 1);

                            //Adding new Log reference
                            Map<Trace, Set<KendoLogRow>> nextTraces = nextStepsTraces.get(currentAction);
                            if (!nextTraces.containsKey(trace)) {
                                nextTraces.put(trace, new HashSet<>());
                            }
                            nextTraces.get(trace).add(row);
                        } else endHereCount++;
                    }
                }

                for (Action action : nextSteps.keySet()) {
                    int count = nextSteps.get(action);
                    if (count >= getParameters().getTrafficThreshold()) {
                        BusinessProcess businessProcess = new BusinessProcess("p", process);
                        businessProcess.getProcessSteps().add(0, new ProcessStep(action));
                        businessProcess.getTransitions().add(new ProcessStepTransition(
                                businessProcess.getProcessSteps().get(0),
                                businessProcess.getProcessSteps().get(1),
                                ""
                        ));
                        changing = true;
                        tempTraceToBusinessProcess.put(businessProcess, nextStepsTraces.get(action));
                        nextStepsTraces.remove(action);
                    } else endHereCount += count;
                }

                //If the row stops than the process will stop here as well
                if (endHereCount >= getParameters().getTrafficThreshold()) {
                    changing = true;
                    Map<Trace, Set<KendoLogRow>> newTracesMap = new HashMap<>();
                    for (Map<Trace, Set<KendoLogRow>> tracesIndex : nextStepsTraces.values()) {
                        for (Trace trace : tracesIndex.keySet()) {
                            if (!newTracesMap.containsKey(trace)) {
                                newTracesMap.put(trace, new HashSet<>());
                            }
                            newTracesMap.get(trace).addAll(tracesIndex.get(trace));
                        }
                    }
                    finalBusinessProcessTraces.put(process, newTracesMap);
                }
            }
        }
        return finalBusinessProcessTraces;
    }

    @Override
    public String toString() {
        return "Trace Frequency Process Map Miner";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

}
