package controller.processMapMiningTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import model.processModel.ProcessMap;

public abstract class ProcessMapMiningTask  extends LoggableTask<ProcessMap> {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private ProcessMapMiningParameters parameters;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public ProcessMap outsideCall() throws Exception {
        return call();
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public ProcessMapMiningParameters getParameters() {
        return parameters;
    }

    public void setParameters(ProcessMapMiningParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException {
        if (!(taskParameters instanceof ProcessMapMiningParameters))
            throw new InvalidArgumentException(new String[] {"Wrong parameters given."});
        parameters = (ProcessMapMiningParameters) taskParameters;
    }

}
