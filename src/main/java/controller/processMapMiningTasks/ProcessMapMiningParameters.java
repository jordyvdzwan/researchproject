package controller.processMapMiningTasks;

import controller.TaskParameters;
import controller.tracers.TraceBuilder;
import model.logs.kendo.KendoLog;

import java.io.File;
import java.util.Map;

public class ProcessMapMiningParameters extends TaskParameters {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private KendoLog kendoLog;
    private File dictionaryFile;
    private TraceBuilder traceBuilder;

    private Integer trafficThreshold;
    private Integer errorThreshold;
    private Integer errorCooldown;
    private Integer nodesThresHold;
    private Double mergeThreshold;
    private String[] tracerBlacklist = new String[0];

    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public boolean filledIn() {
        return kendoLog != null &&
                trafficThreshold != null &&
                traceBuilder != null &&
                mergeThreshold != null &&
                errorThreshold != null &&
                errorCooldown != null &&
                nodesThresHold != null;
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public KendoLog getKendoLog() {
        return kendoLog;
    }
    public void setKendoLog(KendoLog kendoLog) {
        this.kendoLog = kendoLog;
    }

    public Integer getTrafficThreshold() {
        return trafficThreshold;
    }
    public void setTrafficThreshold(Integer trafficThreshold) {
        this.trafficThreshold = trafficThreshold;
    }

    public Integer getNodesThresHold() {
        return nodesThresHold;
    }
    public void setNodesThresHold(Integer nodesThresHold) {
        this.nodesThresHold = nodesThresHold;
    }

    public File getDictionaryFile() {
        return dictionaryFile;
    }
    public void setDictionaryFile(File dictionaryFile) {
        this.dictionaryFile = dictionaryFile;
    }

    public TraceBuilder getTraceBuilder() {
        return traceBuilder;
    }
    public void setTraceBuilder(TraceBuilder traceBuilder) {
        this.traceBuilder = traceBuilder;
    }

    public Double getMergeThreshold() {
        return mergeThreshold;
    }
    public void setMergeThreshold(Double mergeThreshold) {
        this.mergeThreshold = mergeThreshold;
    }

    public Integer getErrorThreshold() {
        return errorThreshold;
    }
    public void setErrorThreshold(Integer errorThreshold) {
        this.errorThreshold = errorThreshold;
    }

    public Integer getErrorCooldown() {
        return errorCooldown;
    }
    public void setErrorCooldown(Integer errorCooldown) {
        this.errorCooldown = errorCooldown;
    }

    public void setTracerBlacklist(String[] tracerBlacklist) {
        this.tracerBlacklist = tracerBlacklist;
    }

    public String[] getTracerBlacklist() {
        return tracerBlacklist;
    }
}

