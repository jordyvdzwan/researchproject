package controller.processComparators;

import controller.TaskParameters;
import javafx.concurrent.Task;
import model.processModel.BusinessProcess;

public class ProcessComparatorTaskParameters extends TaskParameters {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private BusinessProcess original;
    private BusinessProcess actual;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public boolean filledIn() {
        return original != null && actual != null;
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public BusinessProcess getOriginal() {
        return original;
    }

    public void setOriginal(BusinessProcess original) {
        this.original = original;
    }

    public BusinessProcess getActual() {
        return actual;
    }

    public void setActual(BusinessProcess actual) {
        this.actual = actual;
    }
}
