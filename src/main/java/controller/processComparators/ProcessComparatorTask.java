package controller.processComparators;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import controller.processMapComparators.ProcessMapComparatorTaskParameters;
import model.processMapComparison.ProcessComparison;

public abstract class ProcessComparatorTask extends LoggableTask<ProcessComparison> {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private ProcessComparatorTaskParameters parameters;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public ProcessComparatorTaskParameters getParameters() {
        return parameters;
    }

    public void setParameters(ProcessComparatorTaskParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException {
        if (!(taskParameters instanceof ProcessComparatorTaskParameters))
            throw new InvalidArgumentException(new String[] {"Wrong parameters given."});
        parameters = (ProcessComparatorTaskParameters) taskParameters;
    }
}
