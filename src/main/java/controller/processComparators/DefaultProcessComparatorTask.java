package controller.processComparators;

import com.sun.javaws.exceptions.InvalidArgumentException;
import model.processMapComparison.GraphComparison;
import model.processMapComparison.ProcessComparison;
import model.processMapComparison.ProcessMapComparison;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import utils.GraphComparisonUtils;

public class DefaultProcessComparatorTask extends ProcessComparatorTask {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected ProcessComparison call() throws Exception {
        updateStatus("Checking parameters");
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        updateProgress(1, 4);
        if (!getParameters().filledIn())
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});

        BusinessProcess original = getParameters().getOriginal();
        BusinessProcess actual = getParameters().getActual();

        updateStatus("Comparing Transition models");
        GraphComparison comparison = GraphComparisonUtils.compareGraphs(
                original,
                actual
        );

        return new ProcessComparison(original, actual, comparison);
    }

    @Override
    public String toString() {
        return "Default Process Comparator";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
