package controller.logImportTasks;

import controller.TaskParameters;

import java.io.File;
import java.util.List;

public class KendoLogImportParameters extends TaskParameters {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private List<File> files;
    private int maxNrOfRows = Integer.MAX_VALUE;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public int getMaxNrOfRows() {
        return maxNrOfRows;
    }

    public void setMaxNrOfRows(int maxNrOfRows) {
        this.maxNrOfRows = maxNrOfRows;
    }
}
