package controller.logImportTasks;

import com.monitorjbl.xlsx.StreamingReader;
import com.sun.javaws.exceptions.InvalidArgumentException;
import model.logs.kendo.KendoLog;
import model.logs.kendo.KendoLogRow;
import model.logs.model.*;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KendoLogXLSXReadTask extends LogImportTask {

    private Map<String, NovSession> novSessionMap = new HashMap<>();
    private Map<String, ASPSession> aspSessionMap = new HashMap<>();
    private Map<String, WindowSession> windowSessionMap = new HashMap<>();
    private Map<String, Action> actionsMap = new HashMap<>();

    @Override
    protected KendoLog call() throws Exception {
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters given"});
        if (getParameters().getFiles() == null)
            throw new InvalidArgumentException(new String[]{"Must set files field"});
        if (getParameters().getName() == null)
            throw new InvalidArgumentException(new String[]{"Must set name field"});

        List<File> files = getParameters().getFiles();
        int maxNrOfRows = getParameters().getMaxNrOfRows();

        int logRow = 0;

        updateStatus("Opening workbook(s)");
        KendoLog kendoLog = new KendoLog();
        for (int i = 0; i < files.size(); i++) {
            File file = files.get(i);
            updateProgress(i, files.size());
            updateStatus("Reading file "+ file.getName() +" (" + (i + 1) + "/" + files.size() + ")" );

            InputStream is = new FileInputStream(file);
            Workbook workbook = StreamingReader.builder()
                    .rowCacheSize(10000)    // number of rows to keep in memory (defaults to 10)
                    .bufferSize(65536)     // buffer size to use when reading InputStream to file (defaults to 1024)
                    .open(is);            // InputStream or File for XLSX file (required)

            Sheet sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                if (logRow < maxNrOfRows) {
                    logRow++;
                    KendoLogRow kendoLogRow = new KendoLogRow();

                    if (row.getCell(0).getCellTypeEnum() != CellType.NUMERIC)
                        continue;

                    if (row.getCell(0) != null && row.getCell(0).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setId((int) row.getCell(0).getNumericCellValue());
                    if (row.getCell(1) != null && row.getCell(1).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setUser((int) row.getCell(1).getNumericCellValue());
                    if (row.getCell(4) != null && row.getCell(4).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setRequestid((int) row.getCell(4).getNumericCellValue());
                    if (row.getCell(6) != null && row.getCell(6).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setDatetime(row.getCell(6).getDateCellValue());
                    if (row.getCell(7) != null && row.getCell(7).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setTrigger((int) row.getCell(7).getNumericCellValue());
                    if (row.getCell(9) != null && row.getCell(9).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setPageid((int) row.getCell(9).getNumericCellValue());
                    if (row.getCell(10) != null && row.getCell(10).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setMainrecordid((int) row.getCell(10).getNumericCellValue());
                    if (row.getCell(12) != null && row.getCell(12).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setServerProcessTime((int) row.getCell(12).getNumericCellValue());
                    if (row.getCell(13) != null && row.getCell(13).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setServerRequestTime((int) row.getCell(13).getNumericCellValue());
                    if (row.getCell(14) != null && row.getCell(14).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setClientIP(row.getCell(14).getStringCellValue());
                    if (row.getCell(15) != null && row.getCell(15).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setClientRequestTime((int) row.getCell(15).getNumericCellValue());
                    if (row.getCell(16) != null && row.getCell(16).getCellTypeEnum() != CellType.BLANK)
                        kendoLogRow.setClientTotalTime((int) row.getCell(16).getNumericCellValue());

                    if (row.getCell(2) != null && row.getCell(2).getCellTypeEnum() != CellType.BLANK) {
                        String novSession = row.getCell(2).getStringCellValue();
                        if (!novSessionMap.containsKey(novSession))
                            novSessionMap.put(novSession, new NovSession(novSession));
                        kendoLogRow.setNovsessionid(novSessionMap.get(novSession));
                    }
                    if (row.getCell(3) != null && row.getCell(3).getCellTypeEnum() != CellType.BLANK) {
                        String aspSession = row.getCell(3).getStringCellValue();
                        if (!aspSessionMap.containsKey(aspSession))
                            aspSessionMap.put(aspSession, new ASPSession(aspSession));
                        kendoLogRow.setAspsessionid(aspSessionMap.get(aspSession));
                    }
                    if (row.getCell(5) != null && row.getCell(5).getCellTypeEnum() != CellType.BLANK) {
                        String windowSession = row.getCell(5).getStringCellValue();
                        if (!windowSessionMap.containsKey(windowSession))
                            windowSessionMap.put(windowSession, new WindowSession(windowSession));
                        kendoLogRow.setWindowid(windowSessionMap.get(windowSession));
                    }

                    if (row.getCell(11) != null && row.getCell(11).getCellTypeEnum() != CellType.BLANK) {
                        String displayType = row.getCell(11).getStringCellValue();
                        switch (displayType) {
                            case "View":
                                kendoLogRow.setDisplayType(DisplayType.VIEW);
                                break;
                            case "Edit":
                                kendoLogRow.setDisplayType(DisplayType.EDIT);
                                break;
                            case "Add":
                                kendoLogRow.setDisplayType(DisplayType.ADD);
                                break;
                            default:
                                kendoLogRow.setDisplayType(DisplayType.NONE);
                        }
                    }

                    if (row.getCell(8) != null && row.getCell(8).getCellTypeEnum() != CellType.BLANK) {
                        String action = row.getCell(8).getStringCellValue();
                        if (!actionsMap.containsKey(action))
                            actionsMap.put(action, new Action(action));
                        kendoLogRow.setAction(actionsMap.get(action));
                    }

                    kendoLog.getRows().add(kendoLogRow);
                }
            }

            workbook.close();
        }
        kendoLog.setName(getParameters().getName());
        updateProgress(files.size(), files.size());

        return kendoLog;
    }
}
