package controller.logImportTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import model.logs.kendo.KendoLog;

public abstract class LogImportTask extends LoggableTask<KendoLog> {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private KendoLogImportParameters parameters;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public KendoLogImportParameters getParameters() {
        return parameters;
    }

    public void setParameters(KendoLogImportParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException {
        if (!(taskParameters instanceof KendoLogImportParameters))
            throw new InvalidArgumentException(new String[] {"Wrong parameters given."});
        parameters = (KendoLogImportParameters) taskParameters;
    }
}
