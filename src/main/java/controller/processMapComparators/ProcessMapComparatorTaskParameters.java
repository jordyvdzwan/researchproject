package controller.processMapComparators;

import controller.TaskParameters;
import model.processModel.ProcessMap;

public class ProcessMapComparatorTaskParameters extends TaskParameters {

    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private ProcessMap original;
    private ProcessMap actual;
    private boolean calculateProcesses;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public boolean filledIn() {
        return original != null && actual != null;
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public ProcessMap getOriginal() {
        return original;
    }

    public void setOriginal(ProcessMap original) {
        this.original = original;
    }

    public ProcessMap getActual() {
        return actual;
    }

    public void setActual(ProcessMap actual) {
        this.actual = actual;
    }

    public boolean isCalculateProcesses() {
        return calculateProcesses;
    }

    public void setCalculateProcesses(boolean calculateProcesses) {
        this.calculateProcesses = calculateProcesses;
    }
}
