package controller.processMapComparators;

import com.sun.javaws.exceptions.InvalidArgumentException;
import model.processMapComparison.GraphComparison;
import model.processMapComparison.ProcessComparison;
import model.processMapComparison.ProcessMapComparison;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import utils.GraphComparisonUtils;

public class DefaultProcessMapComparatorTask extends ProcessMapComparatorTask {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected ProcessMapComparison call() throws Exception {
        updateStatus("Checking parameters");
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        if (!getParameters().filledIn())
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});
        ProcessMap original = getParameters().getOriginal();
        ProcessMap actual = getParameters().getActual();

        updateStatus("Comparing Transition models");
        GraphComparison transitionModelComparison = GraphComparisonUtils.compareGraphs(
                original.getTransitionModel(),
                actual.getTransitionModel()
        );
        ProcessMapComparison processMapComparison = new ProcessMapComparison(original, actual, transitionModelComparison);

        if (getParameters().isCalculateProcesses()) {
            updateStatus("Comparing processes");
            int index = 0;
            for (BusinessProcess originalProcess : original.getBusinessProcesses()) {
                for (BusinessProcess actualProcess : actual.getBusinessProcesses()) {
                    updateProgress(index++, original.getBusinessProcesses().size() * actual.getBusinessProcesses().size());
                    processMapComparison.getProcessComparisons().add(
                            new ProcessComparison(
                                    originalProcess,
                                    actualProcess,
                                    GraphComparisonUtils.compareGraphs(originalProcess, actualProcess)
                            )
                    );
                }
            }
        }

        taskFinished(true, "Done");
        return processMapComparison;
    }

    @Override
    public String toString() {
        return "Default Process Map Comparator";
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
