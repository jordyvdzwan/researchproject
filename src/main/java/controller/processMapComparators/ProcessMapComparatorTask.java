package controller.processMapComparators;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import model.processMapComparison.ProcessMapComparison;

public abstract class ProcessMapComparatorTask extends LoggableTask<ProcessMapComparison> {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private ProcessMapComparatorTaskParameters parameters;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public ProcessMapComparatorTaskParameters getParameters() {
        return parameters;
    }

    public void setParameters(ProcessMapComparatorTaskParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException {
        if (!(taskParameters instanceof ProcessMapComparatorTaskParameters))
            throw new InvalidArgumentException(new String[] {"Wrong parameters given."});
        parameters = (ProcessMapComparatorTaskParameters) taskParameters;
    }
}
