package controller.parametereizedProcessMapComparators.processMapComparators;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.processMapMiningTasks.ProcessMapMiningParameters;
import controller.processMapMiningTasks.ProcessMapMiningTask;
import model.processMapComparison.GraphComparison;
import model.processMapComparison.ProcessComparison;
import model.processMapComparison.ProcessMapComparison;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import utils.GraphComparisonUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class DefaultParameterizedParameterizedProcessMapComparatorTask extends ParameterizedProcessMapComparatorTask {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    

    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected Map<Double, Score> call() throws Exception {
        updateStatus("Checking parameters");
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        if (!getParameters().filledIn())
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});

        ProcessMap original = getParameters().getOriginal();
        ProcessMapMiningParameters miningParameters = getParameters().getProcessMapMiningParameters();
        Map<Double, Score> result = new HashMap<>();

        BiFunction<ProcessMapMiningParameters, Double, ProcessMapMiningParameters> addFunction = getParameters().getAddFunction();

        updateStatus("Executing mining executions");
        for (Double i = getParameters().getStartIndex(); i < getParameters().getEndIndex(); i += getParameters().getIncrementer()) {
            updateProgress(i - getParameters().getStartIndex(), getParameters().getEndIndex() - getParameters().getStartIndex());

            updateStatus("Mining process map (" +
                    (int)((i - getParameters().getStartIndex())/getParameters().getIncrementer()) +
                    "/" +
                    (int)((getParameters().getEndIndex() - getParameters().getStartIndex())/getParameters().getIncrementer()) +
                    ")"
            );
            ProcessMapMiningTask miningTask = getParameters().getMiningTask().getClass().newInstance();
            miningTask.setParameters(addFunction.apply(miningParameters, i));
            ProcessMap actual = miningTask.outsideCall();

            updateStatus("Comparing Transition models (" +
                    (int)((i - getParameters().getStartIndex())/getParameters().getIncrementer()) +
                    "/" +
                    (int)((getParameters().getEndIndex() - getParameters().getStartIndex())/getParameters().getIncrementer()) +
                    ")"
            );
            GraphComparison transitionModelComparison = GraphComparisonUtils.compareGraphs(
                    original.getTransitionModel(),
                    actual.getTransitionModel()
            );
            ProcessMapComparison processMapComparison = new ProcessMapComparison(original, actual, transitionModelComparison);

            updateStatus("Comparing processes (" +
                    (int)((i - getParameters().getStartIndex())/getParameters().getIncrementer()) +
                    "/" +
                    (int)((getParameters().getEndIndex() - getParameters().getStartIndex())/getParameters().getIncrementer()) +
                    ")"
            );
            int index = 0;
            for (BusinessProcess originalProcess : original.getBusinessProcesses()) {
                for (BusinessProcess actualProcess : actual.getBusinessProcesses()) {
                    updateProgress(index++, original.getBusinessProcesses().size() * actual.getBusinessProcesses().size());
                    processMapComparison.getProcessComparisons().add(
                            new ProcessComparison(
                                    originalProcess,
                                    actualProcess,
                                    GraphComparisonUtils.compareGraphs(originalProcess, actualProcess)
                            )
                    );
                }
            }

            Score score = new Score();
            score.setMaxActualScore(processMapComparison.calculateAverageActualMaxScores());
            score.setMaxOriginalScore(processMapComparison.calculateAverageOriginalMaxScores());
            score.setTotalScore(processMapComparison.calculateAverageScore());
            score.setMaxActualInvalidPercentage(processMapComparison.calculateAverageActualMaxInvalidScores());
            score.setMaxActualMissingPercentage(processMapComparison.calculateAverageActualMaxMissingScores());
            score.setMaxOriginalInvalidPercentage(processMapComparison.calculateAverageOriginalMaxInvalidScores());
            score.setMaxOriginalMissingPercentage(processMapComparison.calculateAverageOriginalMaxMissingScores());

            score.setTransTotal(processMapComparison.getTransitionModelComparison().calculateScorePercentage());
            score.setTransInvalidN(processMapComparison.getTransitionModelComparison().calculateInvalidNodePercentage());
            score.setTransInvalidE(processMapComparison.getTransitionModelComparison().calculateInvalidEdgePercentage());
            score.setTransMissingN(processMapComparison.getTransitionModelComparison().calculateMissingNodePercentage());
            score.setTransMissingE(processMapComparison.getTransitionModelComparison().calculateMissingEdgePercentage());
            score.setTransCorrectN(processMapComparison.getTransitionModelComparison().calculateCorrectNodePercentage());
            score.setTransCorrectE(processMapComparison.getTransitionModelComparison().calculateCorrectEdgePercentage());

            score.setTotalProcessCompletion(processMapComparison.calculateTotalProcessCompletionScore());

            result.put(i, score);
        }

        taskFinished(true, "Done");
        return result;
    }

    @Override
    public String toString() {
        return "Default Process Map Comparator";
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
