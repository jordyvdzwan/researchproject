package controller.parametereizedProcessMapComparators.processMapComparators;

import controller.TaskParameters;
import controller.processMapMiningTasks.ProcessMapMiningParameters;
import controller.processMapMiningTasks.ProcessMapMiningTask;
import model.processMapComparison.ProcessMapComparison;
import model.processModel.ProcessMap;

import java.util.function.BiFunction;
import java.util.function.Function;

public class ParameterizedProcessMapComparatorTaskParameters extends TaskParameters {

    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private ProcessMap original;
    private ProcessMapMiningTask miningTask;
    private Double startIndex;
    private Double endIndex;
    private Double incrementer;
    private ProcessMapMiningParameters processMapMiningParameters;
    private BiFunction<ProcessMapMiningParameters, Double, ProcessMapMiningParameters> addFunction;
    private Function<ProcessMapComparison, Double> scoreRetriever;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public boolean filledIn() {
        return original != null
                && miningTask != null
                && startIndex != null
                && endIndex != null
                && incrementer != null
                && processMapMiningParameters != null
                && addFunction != null;
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public ProcessMapMiningParameters getProcessMapMiningParameters() {
        return processMapMiningParameters;
    }

    public void setProcessMapMiningParameters(ProcessMapMiningParameters processMapMiningParameters) {
        this.processMapMiningParameters = processMapMiningParameters;
    }

//    public Function<ProcessMapComparison, Double> getScoreRetriever() {
//        return scoreRetriever;
//    }
//
//    public void setScoreRetriever(Function<ProcessMapComparison, Double> scoreRetriever) {
//        this.scoreRetriever = scoreRetriever;
//    }

    public ProcessMap getOriginal() {
        return original;
    }

    public void setOriginal(ProcessMap original) {
        this.original = original;
    }

    public ProcessMapMiningTask getMiningTask() {
        return miningTask;
    }

    public void setMiningTask(ProcessMapMiningTask miningTask) {
        this.miningTask = miningTask;
    }

    public Double getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Double startIndex) {
        this.startIndex = startIndex;
    }

    public Double getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(Double endIndex) {
        this.endIndex = endIndex;
    }

    public Double getIncrementer() {
        return incrementer;
    }

    public void setIncrementer(Double incrementer) {
        this.incrementer = incrementer;
    }

    public BiFunction<ProcessMapMiningParameters, Double, ProcessMapMiningParameters> getAddFunction() {
        return addFunction;
    }

    public void setAddFunction(BiFunction<ProcessMapMiningParameters, Double, ProcessMapMiningParameters> addFunction) {
        this.addFunction = addFunction;
    }
}
