package controller.parametereizedProcessMapComparators.processMapComparators;

public class Score {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private double totalScore;
    private double maxOriginalScore;
    private double maxActualScore;
    private double maxOriginalMissingPercentage;
    private double maxActualMissingPercentage;
    private double maxOriginalInvalidPercentage;
    private double maxActualInvalidPercentage;

    private double transTotal;
    private double transInvalidN;
    private double transInvalidE;
    private double transMissingN;
    private double transMissingE;
    private double transCorrectN;
    private double transCorrectE;

    private double totalProcessCompletion;


    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public double getMaxOriginalScore() {
        return maxOriginalScore;
    }

    public void setMaxOriginalScore(double maxOriginalScore) {
        this.maxOriginalScore = maxOriginalScore;
    }

    public double getMaxActualScore() {
        return maxActualScore;
    }

    public void setMaxActualScore(double maxActualScore) {
        this.maxActualScore = maxActualScore;
    }

    public double getMaxOriginalMissingPercentage() {
        return maxOriginalMissingPercentage;
    }

    public void setMaxOriginalMissingPercentage(double maxOriginalMissingPercentage) {
        this.maxOriginalMissingPercentage = maxOriginalMissingPercentage;
    }

    public double getMaxActualMissingPercentage() {
        return maxActualMissingPercentage;
    }

    public void setMaxActualMissingPercentage(double maxActualMissingPercentage) {
        this.maxActualMissingPercentage = maxActualMissingPercentage;
    }

    public double getMaxOriginalInvalidPercentage() {
        return maxOriginalInvalidPercentage;
    }

    public void setMaxOriginalInvalidPercentage(double maxOriginalInvalidPercentage) {
        this.maxOriginalInvalidPercentage = maxOriginalInvalidPercentage;
    }

    public double getMaxActualInvalidPercentage() {
        return maxActualInvalidPercentage;
    }

    public void setMaxActualInvalidPercentage(double maxActualInvalidPercentage) {
        this.maxActualInvalidPercentage = maxActualInvalidPercentage;
    }

    public double getTransTotal() {
        return transTotal;
    }

    public void setTransTotal(double transTotal) {
        this.transTotal = transTotal;
    }

    public double getTransInvalidN() {
        return transInvalidN;
    }

    public void setTransInvalidN(double transInvalidN) {
        this.transInvalidN = transInvalidN;
    }

    public double getTransInvalidE() {
        return transInvalidE;
    }

    public void setTransInvalidE(double transInvalidE) {
        this.transInvalidE = transInvalidE;
    }

    public double getTransMissingN() {
        return transMissingN;
    }

    public void setTransMissingN(double transMissingN) {
        this.transMissingN = transMissingN;
    }

    public double getTransMissingE() {
        return transMissingE;
    }

    public void setTransMissingE(double transMissingE) {
        this.transMissingE = transMissingE;
    }

    public double getTransCorrectN() {
        return transCorrectN;
    }

    public void setTransCorrectN(double transCorrectN) {
        this.transCorrectN = transCorrectN;
    }

    public double getTransCorrectE() {
        return transCorrectE;
    }

    public void setTransCorrectE(double transCorrectE) {
        this.transCorrectE = transCorrectE;
    }

    public double getTotalProcessCompletion() {
        return totalProcessCompletion;
    }

    public void setTotalProcessCompletion(double totalProcessCompletion) {
        this.totalProcessCompletion = totalProcessCompletion;
    }
}
