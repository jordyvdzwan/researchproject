package controller.parametereizedProcessMapComparators.processMapComparators;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import model.processMapComparison.ProcessMapComparison;

import java.util.Map;

public abstract class ParameterizedProcessMapComparatorTask extends LoggableTask<Map<Double, Score>> {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private ParameterizedProcessMapComparatorTaskParameters parameters;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public ParameterizedProcessMapComparatorTaskParameters getParameters() {
        return parameters;
    }

    public void setParameters(ParameterizedProcessMapComparatorTaskParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException {
        if (!(taskParameters instanceof ParameterizedProcessMapComparatorTaskParameters))
            throw new InvalidArgumentException(new String[] {"Wrong parameters given."});
        parameters = (ParameterizedProcessMapComparatorTaskParameters) taskParameters;
    }
}
