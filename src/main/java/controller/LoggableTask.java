package controller;

import com.sun.javaws.exceptions.InvalidArgumentException;
import javafx.concurrent.Task;
import model.ExecutionMessage;
import utils.LogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public abstract class LoggableTask<T> extends Task<T> {

    private String taskStatus = null;
    private long creationTime = System.currentTimeMillis();
    private long statusStartTime;

    private List<ExecutionMessage> executionMessages = new ArrayList<>();

    protected void updateStatus(String status) {
        if (taskStatus != null)
            LogUtils.logger.info("Finished " + taskStatus + " (" + (System.currentTimeMillis() - statusStartTime) + "ms)");
        statusStartTime = System.currentTimeMillis();
        taskStatus = status;
        updateMessage(status);
        LogUtils.logger.info("Starting " + status + "...");
    }

    protected void taskFinished(boolean successful, String reason) {
        if (successful) {
            updateProgress(1, 1);
            updateMessage("Done");
            if (taskStatus != null)
                LogUtils.logger.info("Finished " + taskStatus + " (" + (System.currentTimeMillis() - statusStartTime) + "ms)");
            LogUtils.logger.info("Finished task " + this.toString() + " (" + (System.currentTimeMillis() - creationTime) + "ms)");
        } else {
            updateProgress(0, 1);
            updateMessage("Failed: " + reason);
            if (taskStatus != null)
                LogUtils.logger.warning("Task '" + toString() + "' ended unsuccessfully during status: " + taskStatus + ". Reason: " + reason);
            else
                LogUtils.logger.warning("Task '" + toString() + "' ended unsuccessfully. Reason: " + reason);
        }
    }

    protected void addErrorMessage(ExecutionMessage errorMessage) {
        executionMessages.add(errorMessage);
    }

    public List<ExecutionMessage> retrieveErrorMessages() {
        return executionMessages;
    }

    public abstract void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException;
}
