package controller.logGeneratorTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.tracers.processes.ProcessStepTracer;
import model.logs.kendo.KendoLog;
import model.logs.kendo.KendoLogRow;
import model.logs.model.*;
import model.processModel.ProcessMap;
import model.processModel.ProcessStep;
import model.traces.BusinessProcessTrace;
import utils.TextUtils;

import java.util.*;

public class ErrorInsertingLogGeneratorTask extends LogGeneratorTask {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private Date date = new Date();

    ASPSession aspSession = new ASPSession("ASPDummyId");
    WindowSession windowSession = new WindowSession("WindowDummyId");

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected KendoLog call() throws Exception {
        updateStatus("Checking parameters");
        updateProgress(0, 4);
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        updateProgress(1, 4);
        if (getParameters().getProcessMap() == null)
            throw new InvalidArgumentException(new String[]{"Parameter Process Map must be set"});
        updateProgress(2, 4);
        if (getParameters().getNrOfRepeats() == null)
            throw new InvalidArgumentException(new String[]{"Parameter Nr. of repeats must be set"});
        updateProgress(3, 4);
        if (getParameters().getSeed() == null)
            throw new InvalidArgumentException(new String[]{"Parameter Seed must be set"});
        updateProgress(4, 4);
        if (getParameters().getErrorInsertionChance() == null)
            throw new InvalidArgumentException(new String[]{"Parameter Insertion chance must be set"});
        updateProgress(4, 4);


        updateStatus("Setting up variables");
        updateProgress(0, 1);
        KendoLog kendoLog = new KendoLog();
        kendoLog.setName("ErrorInsertedLog " + TextUtils.getCurrentDataTime());
        ProcessMap processMap = getParameters().getProcessMap();
        Random random = new Random(getParameters().getSeed());

        ProcessStepTracer processStepTracer = new ProcessStepTracer();

        Map<String, Action> stringActionMap = new HashMap<>();
        Integer id = 0;
        Integer repeatIndex = getParameters().getNrOfRepeats();
        updateProgress(1, 1);

        updateStatus("Determining business process traces");
        updateProgress(0, 1);
        List<BusinessProcessTrace> traces = processStepTracer.determineBusinessProcessTraces(processMap.getBusinessProcesses());
        updateProgress(1, 1);

        updateStatus("Building log");
        int index = 0;
        while (repeatIndex > 0) {
            for (BusinessProcessTrace trace : traces) {
                updateProgress(index++, getParameters().getNrOfRepeats() * traces.size());
                NovSession novSession = new NovSession("NovDummyId" + id);
                for (ProcessStep step : trace.getSteps()) {
                    KendoLogRow row = new KendoLogRow();
                    row.setId(id++);

                    row.setNovsessionid(novSession);
                    row.setAspsessionid(aspSession);
                    row.setWindowid(windowSession);
                    row.setRequestid(id);

                    row.setClientIP("127.0.0.1");
                    row.setDisplayType(DisplayType.NONE);
                    row.setClientRequestTime(0);
                    row.setClientTotalTime(0);
                    row.setServerProcessTime(0);
                    row.setServerRequestTime(0);

                    row.setDatetime(addToDate(0, 0, random.nextInt(5), random.nextInt(59), random.nextInt(999)));

                    row.setUser(1);
                    row.setPageid(1);
                    row.setMainrecordid(1);
                    row.setTrigger(1);

                    if (!stringActionMap.containsKey(step.getId()))
                        stringActionMap.put(step.getId(), new Action(step.getId()));

                    row.setAction(stringActionMap.get(step.getId()));
                    kendoLog.getRows().add(row);
                    if (random.nextInt(100) < getParameters().getErrorInsertionChance())
                        kendoLog.getRows().addAll(insertError(row, random, stringActionMap, id, novSession, getParameters().getNrOfRepeats()));
                }
                addToDate(0, 0, random.nextInt(40), random.nextInt(59), random.nextInt(999));
            }
            repeatIndex--;
        }

        taskFinished(true, "");
        return kendoLog;
    }

    private List<KendoLogRow> insertError(KendoLogRow previousRow, Random random, Map<String, Action> stringActionMap, Integer id, NovSession novSession, int nrOfRepeats) {
        List<KendoLogRow> result = new ArrayList<>();
        addToDate(0, 0, random.nextInt(40), random.nextInt(59), random.nextInt(999));

        int count = 0;
        int limit = random.nextInt(1 + random.nextInt(2)) + 1;
        while (count++ < limit) {
            KendoLogRow row = new KendoLogRow();
            row.setId(id++);
            row.setNovsessionid(novSession);
            row.setAspsessionid(aspSession);
            row.setWindowid(windowSession);
            row.setRequestid(id);
            row.setClientIP("127.0.0.1");
            row.setDisplayType(DisplayType.NONE);
            row.setClientRequestTime(0);
            row.setClientTotalTime(0);
            row.setServerProcessTime(0);
            row.setServerRequestTime(0);
            row.setDatetime(addToDate(0, 0, random.nextInt(5), random.nextInt(59), random.nextInt(999)));
            row.setUser(1);
            row.setPageid(1);
            row.setMainrecordid(1);
            row.setTrigger(1);
            String actionName = "Miss click" + random.nextInt(nrOfRepeats);
            if (!stringActionMap.containsKey(actionName))
                stringActionMap.put(actionName, new Action(actionName));
            row.setAction(stringActionMap.get(actionName));
            result.add(row);
        }
        if (random.nextInt(4) > 2) {
            result.add(previousRow);
        }

        return result;
    }

    private Date addToDate(int days, int hours, int minutes, int seconds, int milliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, days);
        calendar.add(Calendar.HOUR, hours);
        calendar.add(Calendar.MINUTE, minutes);
        calendar.add(Calendar.SECOND, seconds);
        calendar.add(Calendar.MILLISECOND, milliseconds);
        date = calendar.getTime();
        return date;
    }

    public String getDescription() {
        return "This generator will generate a single user, sequential, single session kendoLog";
    }

    @Override
    public String toString() {
        return "Error inserting Log Generator";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
