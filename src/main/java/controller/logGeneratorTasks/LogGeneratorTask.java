package controller.logGeneratorTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import model.logs.kendo.KendoLog;

public abstract class LogGeneratorTask extends LoggableTask<KendoLog> {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private KendoLogGenerationParameters parameters;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public KendoLogGenerationParameters getParameters() {
        return parameters;
    }

    public void setParameters(KendoLogGenerationParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException {
        if (!(taskParameters instanceof KendoLogGenerationParameters))
            throw new InvalidArgumentException(new String[] {"Wrong parameters given."});
        parameters = (KendoLogGenerationParameters) taskParameters;
    }
}
