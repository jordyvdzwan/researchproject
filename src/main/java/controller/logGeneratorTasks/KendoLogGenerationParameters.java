package controller.logGeneratorTasks;

import controller.TaskParameters;
import model.processModel.ProcessMap;

public class KendoLogGenerationParameters extends TaskParameters {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private ProcessMap processMap;
    private Integer nrOfRepeats;
    private Integer seed;
    private Double errorInsertionChance;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public ProcessMap getProcessMap() {
        return processMap;
    }

    public void setProcessMap(ProcessMap processMap) {
        this.processMap = processMap;
    }

    public Integer getNrOfRepeats() {
        return nrOfRepeats;
    }

    public void setNrOfRepeats(Integer nrOfRepeats) {
        this.nrOfRepeats = nrOfRepeats;
    }

    public Integer getSeed() {
        return seed;
    }

    public void setSeed(Integer seed) {
        this.seed = seed;
    }

    public Double getErrorInsertionChance() {
        return errorInsertionChance;
    }

    public void setErrorInsertionChance(Double errorInsertionChance) {
        this.errorInsertionChance = errorInsertionChance;
    }
}
