package controller.processMapGenerationTasks;

import controller.TaskParameters;

public class ProcessMapGenerationParameters extends TaskParameters {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    //Determining the amount of processes to generate.
    private Integer nrOfProcesses;

    private Integer nrOfStepsPerProcess;
    private Integer stepCountDeviation;

    private Integer nrOfTransitionsPerProcess;
    private Integer transitionCountDeviation;

    private Integer nrOfStartStepsPerProcess;
    private Integer startStepsCountDeviation;

    private Integer nrOfEndStepsPerProcess;
    private Integer endStepCountDeviation;

    private Integer seed;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public Integer getNrOfProcesses() {
        return nrOfProcesses;
    }

    public void setNrOfProcesses(Integer nrOfProcesses) {
        this.nrOfProcesses = nrOfProcesses;
    }

    public Integer getNrOfStepsPerProcess() {
        return nrOfStepsPerProcess;
    }

    public void setNrOfStepsPerProcess(Integer nrOfStepsPerProcess) {
        this.nrOfStepsPerProcess = nrOfStepsPerProcess;
    }

    public Integer getStepCountDeviation() {
        return stepCountDeviation;
    }

    public void setStepCountDeviation(Integer stepCountDeviation) {
        this.stepCountDeviation = stepCountDeviation;
    }

    public Integer getSeed() {
        return seed;
    }

    public void setSeed(Integer seed) {
        this.seed = seed;
    }

    public Integer getNrOfTransitionsPerProcess() {
        return nrOfTransitionsPerProcess;
    }

    public void setNrOfTransitionsPerProcess(Integer nrOfTransitionsPerProcess) {
        this.nrOfTransitionsPerProcess = nrOfTransitionsPerProcess;
    }

    public Integer getTransitionCountDeviation() {
        return transitionCountDeviation;
    }

    public void setTransitionCountDeviation(Integer transitionCountDeviation) {
        this.transitionCountDeviation = transitionCountDeviation;
    }

    public Integer getNrOfStartStepsPerProcess() {
        return nrOfStartStepsPerProcess;
    }

    public void setNrOfStartStepsPerProcess(Integer nrOfStartStepsPerProcess) {
        this.nrOfStartStepsPerProcess = nrOfStartStepsPerProcess;
    }

    public Integer getStartStepsCountDeviation() {
        return startStepsCountDeviation;
    }

    public void setStartStepsCountDeviation(Integer startStepsCountDeviation) {
        this.startStepsCountDeviation = startStepsCountDeviation;
    }

    public Integer getNrOfEndStepsPerProcess() {
        return nrOfEndStepsPerProcess;
    }

    public void setNrOfEndStepsPerProcess(Integer nrOfEndStepsPerProcess) {
        this.nrOfEndStepsPerProcess = nrOfEndStepsPerProcess;
    }

    public Integer getEndStepCountDeviation() {
        return endStepCountDeviation;
    }

    public void setEndStepCountDeviation(Integer endStepCountDeviation) {
        this.endStepCountDeviation = endStepCountDeviation;
    }
}
