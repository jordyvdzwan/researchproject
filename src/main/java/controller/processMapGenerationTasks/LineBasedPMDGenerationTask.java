package controller.processMapGenerationTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import model.processModel.ProcessStep;
import model.processModel.ProcessStepTransition;
import utils.TextUtils;
import utils.TransitionModelUtils;

import java.util.*;

public class LineBasedPMDGenerationTask extends ProcessMapDataGenerationTask{
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/



    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected ProcessMap call() throws Exception {
        updateStatus("Checking parameters");
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        updateProgress(1, 3);
        if (getParameters().getNrOfProcesses() == null)
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});
        updateProgress(2, 3);
        if (getParameters().getNrOfStepsPerProcess() == null)
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});
        updateProgress(3, 3);
        if (getParameters().getStepCountDeviation() == null)
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});
        updateProgress(3, 3);
        if (getParameters().getSeed() == null)
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});

        updateStatus("Setting up variables");
        int nrOfProcesses = getParameters().getNrOfProcesses();
        Random random = new Random(getParameters().getSeed());
        ProcessStepGenerator stepGenerator = new ProcessStepGenerator();

        ProcessMap processMap = new ProcessMap("Line Based BusinessProcess Map " + TextUtils.getCurrentDataTime());

        updateStatus("Building processes");
        for (int i = 0; i < nrOfProcesses; i++) {
            updateProgress(i, nrOfProcesses);
            BusinessProcess process = generateProcess(stepGenerator, getParameters(), random, "Process-" + i);

            //Coloring in te start and end nodes
            process.detectStartEndNodes();
            processMap.getBusinessProcesses().add(process);
        }

        updateStatus("Building Transition model");
        processMap.setTransitionModel(TransitionModelUtils.generateFromProcesses(processMap.getBusinessProcesses()));

        taskFinished(true, "Done");
        return processMap;
    }

    private BusinessProcess generateProcess(ProcessStepGenerator processStepGenerator, ProcessMapGenerationParameters parameters, Random random, String name) {
        int nrOfStepsPerProcess = parameters.getNrOfStepsPerProcess();
        int stepCountDeviation = parameters.getStepCountDeviation();
        int nrOfSteps = nrOfStepsPerProcess + (random.nextInt(stepCountDeviation * 2) - stepCountDeviation);

        int nrOfTransitionsPerProcess = parameters.getNrOfTransitionsPerProcess();
        int transitionCountDeviation = parameters.getTransitionCountDeviation();
        int nrOfTransitions = nrOfTransitionsPerProcess + (random.nextInt(transitionCountDeviation * 2) - transitionCountDeviation);

        int nrOfStartStepsPerProcess = parameters.getNrOfStartStepsPerProcess();
        int startStepCountDeviation = parameters.getStartStepsCountDeviation();
        int nrOfStartSteps = nrOfStartStepsPerProcess + random.nextInt(startStepCountDeviation * 2) - startStepCountDeviation;
        int nrOfEndStepsPerProcess = parameters.getNrOfEndStepsPerProcess();
        int endStepCountDeviation = parameters.getEndStepCountDeviation();
        int nrOfEndSteps = nrOfEndStepsPerProcess + random.nextInt(endStepCountDeviation * 2) - endStepCountDeviation;

        BusinessProcess businessProcess = new BusinessProcess(name);
        List<ProcessStep> steps = new ArrayList<>();
        List<String> blacklist = new ArrayList<>();

        Integer transitionCount = 0;

        //generating nodes
        for (int i = 1; i <= nrOfSteps; i++) {
            ProcessStep processStep = processStepGenerator.getProcessStep(random, blacklist);
            blacklist.add(processStep.getId());
            businessProcess.getProcessSteps().add(processStep);
            steps.add(processStep);
        }

        Map<ProcessStep, ProcessStep> endPointsMap = new HashMap<>();
        for (int i = 0; i < nrOfStartSteps; i++) {
            ProcessStep step = steps.get(random.nextInt(steps.size()));
            businessProcess.getStartSteps().add(step);
            steps.remove(step);
        }

        for (int i = 0; i < nrOfEndSteps; i++) {
            ProcessStep step = steps.get(random.nextInt(steps.size()));
            businessProcess.getEndSteps().add(step);
            steps.remove(step);
        }

        Map<ProcessStep, Set<ProcessStep>> transitions = new HashMap<>();
        int nrOfStepStrings = businessProcess.getStartSteps().size() + businessProcess.getEndSteps().size();

        // build step strings
        List<ProcessStep> stepSet = new ArrayList<>();
        for (ProcessStep startStep : businessProcess.getStartSteps()) {
            ProcessStep prevStep = startStep;
            int limit = (int) ((double) steps.size() / (double) nrOfStepStrings) - 2;
            int nrOfNodes = random.nextInt(limit > 0 ? limit : 1) + 2;

            for (int i = 0; i < nrOfNodes; i++) {
                ProcessStep step = steps.get(random.nextInt(steps.size()));

                ProcessStepTransition transition = new ProcessStepTransition(prevStep, step, "");
                businessProcess.getTransitions().add(transition);
                if (!transitions.containsKey(prevStep))
                    transitions.put(prevStep, new HashSet<>());
                transitions.get(prevStep).add(step);

                steps.remove(step);
                stepSet.add(step);
                prevStep = step;
                transitionCount++;
                endPointsMap.put(startStep, step);
            }
        }

        int count = 0;
        for (ProcessStep endStep : businessProcess.getEndSteps()) {
            ProcessStep prevStep = endStep;
            int limit = (int) ((double) steps.size() / (double) nrOfStepStrings) - 2;
            int nrOfNodes = random.nextInt(limit > 0 ? limit : 1) + 2;
            if (++count == businessProcess.getEndSteps().size())
                nrOfNodes = steps.size();

            for (int i = 0; i < nrOfNodes; i++) {
                ProcessStep step = steps.get(random.nextInt(steps.size()));

                ProcessStepTransition transition = new ProcessStepTransition(step, prevStep, "");
                businessProcess.getTransitions().add(transition);
                if (!transitions.containsKey(prevStep))
                    transitions.put(prevStep, new HashSet<>());
                transitions.get(prevStep).add(step);

                steps.remove(step);
                stepSet.add(step);
                prevStep = step;
                transitionCount++;
                endPointsMap.put(endStep, step);
            }
        }

        //Connecting start and end step strings
        for (ProcessStep startStep : businessProcess.getStartSteps()) {
            for (ProcessStep endStep : businessProcess.getEndSteps()) {
                ProcessStep fromStep = endPointsMap.get(startStep);
                ProcessStep toStep = endPointsMap.get(endStep);

                ProcessStepTransition transition = new ProcessStepTransition(fromStep, toStep, "");
                businessProcess.getTransitions().add(transition);
                if (!transitions.containsKey(fromStep))
                    transitions.put(fromStep, new HashSet<>());
                transitions.get(fromStep).add(toStep);
                transitionCount++;
            }
        }

        // insert extra edges
        for (int i = 0; transitionCount < nrOfTransitions; i++) {
            ProcessStep fromStep;
            ProcessStep toStep;

            do {
                fromStep = stepSet.get(random.nextInt(stepSet.size()));
                toStep = stepSet.get(random.nextInt(stepSet.size()));

                //Accept nodes if the transition doesnt already exist
            } while ((transitions.containsKey(fromStep) && transitions.get(fromStep).contains(toStep))
                    || businessProcess.getStartSteps().contains(toStep)
                    || businessProcess.getEndSteps().contains(fromStep));

            ProcessStepTransition transition = new ProcessStepTransition(fromStep, toStep, "");
            businessProcess.getTransitions().add(transition);
            if (!transitions.containsKey(fromStep))
                transitions.put(fromStep, new HashSet<>());
            transitions.get(fromStep).add(toStep);
            transitionCount++;
        }


        return businessProcess;
    }


    @Override
    public String toString() {
        return "Line based Map Generator";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
