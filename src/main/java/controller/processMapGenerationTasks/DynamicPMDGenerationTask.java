package controller.processMapGenerationTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import javafx.scene.paint.Color;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import model.processModel.ProcessStep;
import model.processModel.ProcessStepTransition;
import utils.TextUtils;
import utils.TransitionModelUtils;

import java.util.*;

public class DynamicPMDGenerationTask extends ProcessMapDataGenerationTask{
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected ProcessMap call() throws Exception {
        updateStatus("Checking parameters");
        if (getParameters() == null)
            throw new InvalidArgumentException(new String[]{"No parameters were given"});
        updateProgress(1, 3);
        if (getParameters().getNrOfProcesses() == null)
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});
        updateProgress(2, 3);
        if (getParameters().getNrOfStepsPerProcess() == null)
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});
        updateProgress(3, 3);
        if (getParameters().getStepCountDeviation() == null)
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});
        updateProgress(3, 3);
        if (getParameters().getSeed() == null)
            throw new InvalidArgumentException(new String[]{"Not all parameters were set"});

        updateStatus("Setting up variables");
        int nrOfProcesses = getParameters().getNrOfProcesses();
        Random random = new Random(getParameters().getSeed());

        ProcessMap processMap = new ProcessMap("Dynamic BusinessProcess Map " + TextUtils.getCurrentDataTime());

        updateStatus("Building processes");
        for (int i = 0; i < nrOfProcesses; i++) {
            updateProgress(i, nrOfProcesses);
            processMap.getBusinessProcesses().add(generateProcess(getParameters(), random, "Process-" + i));
        }

        updateStatus("Building Transition model");
        processMap.setTransitionModel(TransitionModelUtils.generateFromProcesses(processMap.getBusinessProcesses()));

        taskFinished(true, "Done");
        return processMap;
    }

    private BusinessProcess generateProcess(ProcessMapGenerationParameters parameters, Random random, String name) {
        int nrOfStepsPerProcess = parameters.getNrOfStepsPerProcess();
        int stepCountDeviation = parameters.getStepCountDeviation();
        int nrOfSteps = nrOfStepsPerProcess + (random.nextInt(stepCountDeviation * 2) - stepCountDeviation);

        int nrOfTransitionsPerProcess = parameters.getNrOfTransitionsPerProcess();
        int transitionCountDeviation = parameters.getTransitionCountDeviation();
        int nrOfTransitions = nrOfTransitionsPerProcess + (random.nextInt(transitionCountDeviation * 2) - transitionCountDeviation);

        BusinessProcess businessProcess = new BusinessProcess(name);
        List<ProcessStep> steps = new ArrayList<>();

        for (int i = 1; i <= nrOfSteps; i++) {
            ProcessStep processStep = new ProcessStep("Step " + i);
            businessProcess.getProcessSteps().add(processStep);
            steps.add(processStep);
        }

        int nrOfStartStepsPerProcess = parameters.getNrOfStartStepsPerProcess();
        int startStepCountDeviation = parameters.getStartStepsCountDeviation();
        int nrOfStartSteps = nrOfStartStepsPerProcess + random.nextInt(startStepCountDeviation * 2) - startStepCountDeviation;
        for (int i = 0; i < nrOfStartSteps; i++) {
            businessProcess.getStartSteps().add(steps.get(random.nextInt(steps.size())));
        }
        for (ProcessStep processStep : businessProcess.getStartSteps()) {
            processStep.setColor(Color.AQUA);
        }

        //TODO force end steps and start steps to comply wsith the limits. as a double start and end step will now just overlap.
        int nrOfEndStepsPerProcess = parameters.getNrOfEndStepsPerProcess();
        int endStepCountDeviation = parameters.getEndStepCountDeviation();
        int nrOfEndSteps = nrOfEndStepsPerProcess + random.nextInt(endStepCountDeviation * 2) - endStepCountDeviation;
        for (int i = 0; i < nrOfEndSteps; i++) {
            ProcessStep step = steps.get(random.nextInt(steps.size()));
            while (businessProcess.getStartSteps().contains(step))
                step = steps.get(random.nextInt(steps.size()));
            businessProcess.getEndSteps().add(step);
        }
        for (ProcessStep processStep : businessProcess.getEndSteps()) {
            processStep.setColor(Color.ORANGE);
        }

        Map<ProcessStep, Set<ProcessStep>> transitions = new HashMap<>();
        for (int i = 1; i <= nrOfTransitions; i++) {
            ProcessStep fromStep;
            ProcessStep toStep;
            int overflowCounter = 0;
            while (true) {
                //Start over if stuck. Not likely but still...
                if (overflowCounter > 200) return generateProcess(parameters, random, name);

                fromStep = steps.get(random.nextInt(steps.size()));
                toStep = steps.get(random.nextInt(steps.size()));

                if (!businessProcess.getEndSteps().contains(fromStep) &&
                        !businessProcess.getStartSteps().contains(toStep) &&
                        fromStep != toStep &&
                        (
                            !transitions.containsKey(fromStep) ||
                            !transitions.get(fromStep).contains(toStep)
                        )
                    ) {
                    if (!transitions.containsKey(fromStep))
                        transitions.put(fromStep, new HashSet<>());
                    transitions.get(fromStep).add(toStep);

                    break;
                }
                overflowCounter++;
            }

            ProcessStepTransition transition = new ProcessStepTransition(fromStep, toStep, "");
            businessProcess.getTransitions().add(transition);
        }

        while (true) {
            List<ProcessStep> noIncomingEdges = new ArrayList<>();
            List<ProcessStep> noOutgoingEdges = new ArrayList<>();
            for (ProcessStep step : steps) {
                if (!hasIncoming(step, transitions) && !businessProcess.getStartSteps().contains(step))
                    noIncomingEdges.add(step);
                if (!hasOutoing(step, transitions) && !businessProcess.getEndSteps().contains(step))
                    noOutgoingEdges.add(step);
            }
            if (noIncomingEdges.size() == 0 && noOutgoingEdges.size() == 0)
                break;

            for (ProcessStep from : noOutgoingEdges) {
                ProcessStep to;
                if (noIncomingEdges.size() != 0 && !(noIncomingEdges.size() == 1 && noIncomingEdges.get(0) == from)) {
                    to = noIncomingEdges.get(random.nextInt(noIncomingEdges.size()));
                    while (to == from || businessProcess.getStartSteps().contains(to))
                        to = noIncomingEdges.get(random.nextInt(noIncomingEdges.size()));
                } else  {
                    to = businessProcess.getProcessSteps().get(random.nextInt(businessProcess.getProcessSteps().size()));
                    while (to == from || businessProcess.getStartSteps().contains(to))
                        to = businessProcess.getProcessSteps().get(random.nextInt(businessProcess.getProcessSteps().size()));
                }

                businessProcess.getTransitions().add(new ProcessStepTransition(from, to, ""));
                if (!transitions.containsKey(from))
                    transitions.put(from, new HashSet<>());
                transitions.get(from).add(to);
                noIncomingEdges.remove(to);
            }

            for (ProcessStep to : noIncomingEdges) {
                ProcessStep from = businessProcess.getProcessSteps().get(random.nextInt(businessProcess.getProcessSteps().size()));
                while (to == from || businessProcess.getEndSteps().contains(from))
                    from = businessProcess.getProcessSteps().get(random.nextInt(businessProcess.getProcessSteps().size()));

                businessProcess.getTransitions().add(new ProcessStepTransition(from, to, ""));
                if (!transitions.containsKey(from))
                    transitions.put(from, new HashSet<>());
                transitions.get(from).add(to);
            }
        }

        return businessProcess;
    }

    private boolean hasIncoming(ProcessStep step, Map<ProcessStep, Set<ProcessStep>> transitions) {
        for (Set<ProcessStep> steps : transitions.values()) {
            if (steps.contains(step)) return true;
        }
        return false;
    }

    private boolean hasOutoing(ProcessStep step, Map<ProcessStep, Set<ProcessStep>> transitions) {
        return transitions.containsKey(step);
    }

    @Override
    public String toString() {
        return "Dynamic Map Generator";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
