package controller.processMapGenerationTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import model.processModel.ProcessMap;

public abstract class ProcessMapDataGenerationTask extends LoggableTask<ProcessMap> {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private ProcessMapGenerationParameters parameters;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public ProcessMapGenerationParameters getParameters() {
        return parameters;
    }

    public void setParameters(ProcessMapGenerationParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException {
        if (!(taskParameters instanceof ProcessMapGenerationParameters))
            throw new InvalidArgumentException(new String[] {"Wrong parameters given."});
        parameters = (ProcessMapGenerationParameters) taskParameters;
    }

}
