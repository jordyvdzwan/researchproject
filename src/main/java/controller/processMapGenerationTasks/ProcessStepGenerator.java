package controller.processMapGenerationTasks;

import model.logs.model.Action;
import model.logs.model.DisplayType;
import model.processModel.ProcessStep;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProcessStepGenerator {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private List<ProcessStep> steps = new ArrayList<>();

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public ProcessStep getProcessStep(Random random, List<String> blackList) {
        ProcessStep step;
        if (random.nextBoolean() && steps.size() > 0) {
            step = steps.get(random.nextInt(steps.size()));
            int index = 0;
            while (index < 25 && blackList.contains(step.getId())) {
                step = steps.get(random.nextInt(steps.size()));
                index++;
            }
            if (index < 25) return step;
        }
        if (random.nextBoolean()) {
            step = new ProcessStep("Step-" + steps.size());
            Action action = new Action("action" + steps.size());
            action.setTrigger(random.nextInt(9999) + 20000);
            blackList.add(step.getId());
            steps.add(step);
            return step;
        } else {
            step = new ProcessStep("Step-" + steps.size());
            Action action = new Action("loadpage" + steps.size());
            action.setTrigger(random.nextInt(9999) + 20000);
            action.setMainrecordid(random.nextInt(9999) + 10000);
            action.setPageid(random.nextInt(9999) + 30000);
            action.setDisplayType(random.nextBoolean() ? DisplayType.VIEW : random.nextBoolean() ? DisplayType.ADD : DisplayType.EDIT);
            blackList.add(step.getId());
            steps.add(step);
            return step;
        }
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
