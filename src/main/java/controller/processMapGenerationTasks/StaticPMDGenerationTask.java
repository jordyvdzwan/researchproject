package controller.processMapGenerationTasks;

import model.processModel.*;
import utils.TextUtils;
import utils.TransitionModelUtils;

import java.util.HashMap;

public class StaticPMDGenerationTask extends ProcessMapDataGenerationTask{
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected ProcessMap call() throws Exception {
        ProcessMapGenerationParameters parameters = getParameters();
        ProcessMap processMap = new ProcessMap("Simple BusinessProcess Map " + TextUtils.getCurrentDataTime());

//        processMap.getBusinessProcesses().add(generateLineProcess(3));
        processMap.getBusinessProcesses().add(generateLineProcess(6));
//        processMap.getBusinessProcesses().add(generateLineProcess(10));
//        processMap.getBusinessProcesses().add(generateLineProcess(12));
//        processMap.getBusinessProcesses().add(generateLineProcess(16));
//        processMap.getBusinessProcesses().add(generateLineProcess(21));
//        processMap.getBusinessProcesses().add(generateLineProcess(25));
//        processMap.getBusinessProcesses().add(generateLineProcess(30));
//        processMap.getBusinessProcesses().add(generateLineProcess(56));
//        processMap.getBusinessProcesses().add(generateLineProcess(71));
//        processMap.getBusinessProcesses().add(generateLineProcess(89));
//        processMap.getBusinessProcesses().add(generateLineProcess(123));
//        processMap.getBusinessProcesses().add(generateLineProcess(234));

        processMap.setTransitionModel(TransitionModelUtils.generateFromProcesses(processMap.getBusinessProcesses()));

        return processMap;
    }

    private BusinessProcess generateLineProcess(int length) {
        HashMap<Integer, ProcessStep> stepMap = new HashMap<>();
        BusinessProcess businessProcess = new BusinessProcess("LineProcess-" + length);

        ProcessStep processStep1 = new ProcessStep("Step 1");
        businessProcess.getProcessSteps().add(processStep1);
        businessProcess.getStartSteps().add(processStep1);
        stepMap.put(1, processStep1);

        for (int i = 2; i <= length; i++) {
            ProcessStep step = new ProcessStep("Step " + i);
            ProcessStepTransition transition = new ProcessStepTransition(stepMap.get(i - 1), step, "");

            businessProcess.getTransitions().add(transition);
            businessProcess.getProcessSteps().add(step);
            stepMap.put(i, step);

            businessProcess.getEndSteps().clear();
            businessProcess.getEndSteps().add(step);
        }


        return businessProcess;
    }

    @Override
    public String toString() {
        return "Static Map Generator";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
