package controller.transitionMapBuilderTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import model.transitionModel.TransitionModel;

public abstract class TransitionModelBuilderTask extends LoggableTask<TransitionModel> {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private TransitionModelBuilderParameters parameters;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public TransitionModelBuilderParameters getParameters() {
        return parameters;
    }

    public void setParameters(TransitionModelBuilderParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void setTaskParameters(TaskParameters taskParameters) throws InvalidArgumentException {
        if (!(taskParameters instanceof TransitionModelBuilderParameters))
            throw new InvalidArgumentException(new String[] {"Wrong parameters given."});
        parameters = (TransitionModelBuilderParameters) taskParameters;
    }
}
