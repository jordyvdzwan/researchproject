package controller.transitionMapBuilderTasks;

import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.tracers.NovSessionExtendedLoadPageTraceBuilder;
import controller.tracers.NovSessionLoadPageTraceBuilder;
import model.logs.kendo.KendoLog;
import model.logs.kendo.KendoLogRow;
import model.logs.model.Action;
import model.traces.Trace;
import model.transitionModel.TransitionEdge;
import model.transitionModel.TransitionModel;
import model.transitionModel.TransitionNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ExtendedLoadPageQuantityTransitionModelBuilder extends TransitionModelBuilderTask {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/



    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    protected TransitionModel call() throws Exception {

        if (getParameters() == null) throw new InvalidArgumentException(new String[]{"No parameters were given"});
        KendoLog kendoLog = getParameters().getKendoLog();
        if (kendoLog == null) throw new InvalidArgumentException(new String[]{"Kendolog parameter was not set."});

        updateMessage("Building traces");
        NovSessionExtendedLoadPageTraceBuilder traceBuilder = new NovSessionExtendedLoadPageTraceBuilder();
        Set<Trace> traces = traceBuilder.build(kendoLog, new String[0]).getTraces();

        updateMessage("Determining nodes");

        Map<Action, TransitionNode> nodeMap = new HashMap<>();
        int rows = 0;
        for (KendoLogRow row : kendoLog.getRows()) {
            updateProgress(rows++, kendoLog.getRows().size());
            Action action = new Action(row.getAction().getName());
            action.setDisplayType(row.getDisplayType());
            action.setPageid(row.getPageid());
            action.setMainrecordid(row.getMainrecordid());
            action.setTrigger(row.getTrigger());

            TransitionNode transitionNode = new TransitionNode(row.getAction().getName(), action);
            nodeMap.put(row.getAction(), transitionNode);
        }
        Set<TransitionNode> nodes = new HashSet<>(nodeMap.values());

        updateMessage("Determining edges");
        Map<Action, Map<Action, TransitionEdge>> edges = new HashMap<>();
        int tracesCount = 0;
        for (Trace trace : traces) {
            updateProgress(tracesCount++, traces.size());
            for (int i = 1; i < trace.getRows().size(); i++) {
                KendoLogRow prevKendoLogRow = trace.getRows().get(i - 1);
                KendoLogRow kendoLogRow = trace.getRows().get(i);

                if (!edges.containsKey(prevKendoLogRow.getAction()))
                    edges.put(prevKendoLogRow.getAction(), new HashMap<>());

                if (!edges.get(prevKendoLogRow.getAction()).containsKey(kendoLogRow.getAction()))
                    edges.get(
                            prevKendoLogRow.getAction()).put(kendoLogRow.getAction(),
                            new TransitionEdge(
                                    nodeMap.get(prevKendoLogRow.getAction()),
                                    nodeMap.get(kendoLogRow.getAction()),
                                    "1"
                            )
                    );
                else
                    edges.get(prevKendoLogRow.getAction()).get(kendoLogRow.getAction()).setLabel((Integer.parseInt(edges.get(prevKendoLogRow.getAction()).get(kendoLogRow.getAction()).getLabel()) + 1) + "");
            }
        }

        updateMessage("Reformatting edges");
        int transitionsCount = 0;
        Set<TransitionEdge> transitionEdges = new HashSet<>();
        for (Map<Action, TransitionEdge> map : edges.values()) {
            updateProgress(transitionsCount++, edges.values().size());
            for (TransitionEdge edge : map.values()) {
                if (Integer.parseInt(edge.getLabel()) > 14)
                    transitionEdges.add(edge);
            }
        }

        return new TransitionModel(nodes, transitionEdges);
    }

    @Override
    public String toString() {
        return "Extended Page Transition Model Builder";
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
