package controller.transitionMapBuilderTasks;

import controller.TaskParameters;
import model.logs.kendo.KendoLog;

public class TransitionModelBuilderParameters  extends TaskParameters {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private KendoLog kendoLog;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public KendoLog getKendoLog() {
        return kendoLog;
    }

    public void setKendoLog(KendoLog kendoLog) {
        this.kendoLog = kendoLog;
    }
}
