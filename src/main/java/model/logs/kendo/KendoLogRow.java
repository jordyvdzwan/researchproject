package model.logs.kendo;

import model.logs.model.*;
import model.traces.Trace;

import java.util.Date;

public class KendoLogRow {

    private Integer id;
    private Integer user;

    private NovSession novsessionid;
    private ASPSession aspsessionid;
    private WindowSession windowid;

    private Integer requestid;

    private Date datetime;

    private Integer trigger;
    private Action action;

    private Integer pageid;
    private Integer mainrecordid;

    private DisplayType displayType;
    private String clientIP;

    private long serverProcessTime;
    private long serverRequestTime;
    private long clientRequestTime;
    private long clientTotalTime;
    private Trace trace;

    public KendoLogRow() {
    }

    public KendoLogRow(KendoLogRow row) {
        id = row.id;
        user = row.user;
        novsessionid = row.novsessionid;
        aspsessionid = row.aspsessionid;
        windowid = row.windowid;
        requestid = row.requestid;
        datetime = row.datetime;
        trigger = row.trigger;
        action = row.action;
        pageid = row.pageid;
        mainrecordid = row.mainrecordid;
        displayType = row.displayType;
        clientIP = row.clientIP;
        serverProcessTime = row.serverProcessTime;
        serverRequestTime = row.serverRequestTime;
        clientRequestTime = row.clientRequestTime;
        clientTotalTime = row.clientTotalTime;
        trace = row.trace;
    }

    public Action toAction() {
        return new Action(action.getName(), pageid, displayType);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public NovSession getNovsessionid() {
        return novsessionid;
    }

    public void setNovsessionid(NovSession novsessionid) {
        this.novsessionid = novsessionid;
    }

    public ASPSession getAspsessionid() {
        return aspsessionid;
    }

    public void setAspsessionid(ASPSession aspsessionid) {
        this.aspsessionid = aspsessionid;
    }

    public WindowSession getWindowid() {
        return windowid;
    }

    public void setWindowid(WindowSession windowid) {
        this.windowid = windowid;
    }

    public Integer getRequestid() {
        return requestid;
    }

    public void setRequestid(Integer requestid) {
        this.requestid = requestid;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Integer getTrigger() {
        return trigger;
    }

    public void setTrigger(Integer trigger) {
        this.trigger = trigger;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Integer getPageid() {
        return pageid;
    }

    public void setPageid(Integer pageid) {
        this.pageid = pageid;
    }

    public Integer getMainrecordid() {
        return mainrecordid;
    }

    public void setMainrecordid(Integer mainrecordid) {
        this.mainrecordid = mainrecordid;
    }

    public DisplayType getDisplayType() {
        return displayType;
    }

    public void setDisplayType(DisplayType displayType) {
        this.displayType = displayType;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public long getServerProcessTime() {
        return serverProcessTime;
    }

    public void setServerProcessTime(long serverProcessTime) {
        this.serverProcessTime = serverProcessTime;
    }

    public long getServerRequestTime() {
        return serverRequestTime;
    }

    public void setServerRequestTime(long serverRequestTime) {
        this.serverRequestTime = serverRequestTime;
    }

    public long getClientRequestTime() {
        return clientRequestTime;
    }

    public void setClientRequestTime(long clientRequestTime) {
        this.clientRequestTime = clientRequestTime;
    }

    public long getClientTotalTime() {
        return clientTotalTime;
    }

    public void setClientTotalTime(long clientTotalTime) {
        this.clientTotalTime = clientTotalTime;
    }

    public Trace getTrace() {
        return trace;
    }

    public void setTrace(Trace trace) {
        this.trace = trace;
    }
}
