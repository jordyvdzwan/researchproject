package model.logs.kendo;

import java.util.ArrayList;
import java.util.List;

public class KendoLog {

    private List<KendoLogRow> rows = new ArrayList<>();
    private String name;

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<KendoLogRow> getRows() {
        return rows;
    }
}
