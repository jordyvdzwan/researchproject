package model.logs.events;

public abstract class ProcessEvent {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    public abstract boolean equalEvent(ProcessEvent event);
    public abstract String getLabel();
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
