package model.logs.interim;

import model.logs.kendo.KendoLogRow;

import java.util.ArrayList;
import java.util.List;

public class InterimLog {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private List<KendoLogRow> rows = new ArrayList<>();
    private String name;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    public String toString() {
        return name;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<KendoLogRow> getRows() {
        return rows;
    }

}
