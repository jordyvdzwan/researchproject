package model.logs.interim;

import model.logs.events.ProcessEvent;
import model.logs.model.*;

import java.util.Date;

public class InterimLogRow {

    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private int user;
    private NovSession novsessionid;
    private ASPSession aspsessionid;
    private WindowSession windowid;
    private String clientIP;
    private Date datetime;

    private ProcessEvent event;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public NovSession getNovsessionid() {
        return novsessionid;
    }

    public void setNovsessionid(NovSession novsessionid) {
        this.novsessionid = novsessionid;
    }

    public ASPSession getAspsessionid() {
        return aspsessionid;
    }

    public void setAspsessionid(ASPSession aspsessionid) {
        this.aspsessionid = aspsessionid;
    }

    public WindowSession getWindowid() {
        return windowid;
    }

    public void setWindowid(WindowSession windowid) {
        this.windowid = windowid;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public ProcessEvent getEvent() {
        return event;
    }

    public void setEvent(ProcessEvent event) {
        this.event = event;
    }
}
