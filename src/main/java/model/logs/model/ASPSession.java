package model.logs.model;

public class ASPSession {

    private String sessionId;

    public ASPSession(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

}
