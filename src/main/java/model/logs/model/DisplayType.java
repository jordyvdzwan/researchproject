package model.logs.model;

public enum DisplayType {
    ADD, VIEW, EDIT, NONE
}
