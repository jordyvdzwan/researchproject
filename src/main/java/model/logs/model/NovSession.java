package model.logs.model;

public class NovSession {

    private String sessionId;

    public NovSession(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }
}
