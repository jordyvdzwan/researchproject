package model.logs.model;

public class WindowSession {

    private String sessionId;

    public WindowSession(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

}
