package model.logs.model;

import java.util.Objects;

public class Action {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private String name;
    private int trigger;
    private int pageid;
    private int mainrecordid;
    private DisplayType displayType;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public Action(String name) {
        this.name = name;
    }

    public Action(String name, Integer pageid, DisplayType displayType) {
        this.name = name;
        this.pageid = pageid == null ? -1 : pageid;
        this.displayType = displayType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Action action = (Action) o;
        return pageid == action.pageid &&
                Objects.equals(name, action.name) &&
                displayType == action.displayType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, pageid, displayType);
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public String getName() {
        return name;
    }

    public int getTrigger() {
        return trigger;
    }

    public void setTrigger(int trigger) {
        this.trigger = trigger;
    }

    public int getPageid() {
        return pageid;
    }

    public void setPageid(int pageid) {
        this.pageid = pageid;
    }

    public int getMainrecordid() {
        return mainrecordid;
    }

    public void setMainrecordid(int mainrecordid) {
        this.mainrecordid = mainrecordid;
    }

    public DisplayType getDisplayType() {
        return displayType;
    }

    public void setDisplayType(DisplayType displayType) {
        this.displayType = displayType;
    }
}
