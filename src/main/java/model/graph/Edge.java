package model.graph;

import javafx.scene.paint.Color;

public interface Edge {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    

    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public Node getTo();
    public Node getFrom();
    public String getLabel();
    public Color getColor();
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
