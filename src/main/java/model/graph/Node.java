package model.graph;

import javafx.scene.paint.Color;
import model.logs.model.Action;

public interface Node {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    

    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public String getId();
    public Color getColor();
    public Action getAction();
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
