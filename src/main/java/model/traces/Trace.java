package model.traces;

import model.logs.kendo.KendoLogRow;

import java.util.ArrayList;
import java.util.List;

public class Trace {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private List<KendoLogRow> rows = new ArrayList<>();
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/


    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public List<KendoLogRow> getRows() {
        return rows;
    }
}
