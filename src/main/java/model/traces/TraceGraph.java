package model.traces;

import model.graph.Edge;
import model.graph.Graph;
import model.graph.Node;
import model.logs.kendo.KendoLogRow;
import model.logs.model.Action;

import java.util.*;

public class TraceGraph implements Graph {
    private Map<Action, TraceNode> traceNodes = new HashMap<>();
    private Map<TraceNode, Map<TraceNode, TraceEdge>> traceEdges = new HashMap<>();

    public void addTraceNode(Trace trace, KendoLogRow kendoLogRow) {
        Action action = kendoLogRow.toAction();
        if (!traceNodes.containsKey(action))
            traceNodes.put(action, new TraceNode(action.getName(), action));

        Map<Trace, Set<KendoLogRow>> traceSetMap = traceNodes.get(action).getTraceSetMap();
        if (!traceSetMap.containsKey(trace))
            traceSetMap.put(trace, new HashSet<>());

        traceSetMap.get(trace).add(kendoLogRow);
    }
    public void addTraceEdge(Trace trace, KendoLogRow from, KendoLogRow to) {
        if (!traceNodes.containsKey(to.toAction()) || !traceNodes.containsKey(from.toAction()))
            throw new IllegalArgumentException("Nodes must already be in the graph.");

        TraceNode fromNode = traceNodes.get(from.toAction());
        TraceNode toNode = traceNodes.get(to.toAction());

        if (!traceEdges.containsKey(fromNode))
            traceEdges.put(fromNode, new HashMap<>());

        TraceEdge edge = new TraceEdge(fromNode.getId() + "-" + toNode.getId(), fromNode, toNode);
        traceEdges.get(fromNode).put(toNode, edge);

        if (!edge.getFromTraceSetMap().containsKey(trace))
            edge.getFromTraceSetMap().put(trace, new HashSet<>());
        edge.getFromTraceSetMap().get(trace).add(from);

        if (!edge.getToTraceSetMap().containsKey(trace))
            edge.getToTraceSetMap().put(trace, new HashSet<>());
        edge.getToTraceSetMap().get(trace).add(to);

        fromNode.getOutEdges().add(edge);
        toNode.getInEdges().add(edge);
    }

    public Map<Action, TraceNode> getTraceNodes() {
        return traceNodes;
    }
    public Map<TraceNode, Map<TraceNode, TraceEdge>> getTraceEdges() {
        return traceEdges;
    }
    @Override
    public List<Node> getNodes() {
        return new ArrayList<>(traceNodes.values());
    }
    @Override
    public List<Edge> getEdges() {
        List<Edge> edges = new ArrayList<>();
        for (Map<TraceNode, TraceEdge> traceEdgesMap : traceEdges.values()) {
            edges.addAll(traceEdgesMap.values());
        }
        return edges;
    }
}
