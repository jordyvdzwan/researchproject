package model.traces;

import java.util.HashSet;
import java.util.Set;

public class TraceSet {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private Set<Trace> traces = new HashSet<>();
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/


    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public Set<Trace> getTraces() {
        return traces;
    }
}
