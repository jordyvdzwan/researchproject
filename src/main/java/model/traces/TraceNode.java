package model.traces;

import javafx.scene.paint.Color;
import model.graph.Node;
import model.logs.kendo.KendoLogRow;
import model.logs.model.Action;

import java.util.*;

public class TraceNode implements Node {
    private String name;
    private Action action;
    private Color color = Color.WHITE;
    private Map<Trace, Set<KendoLogRow>> traceSetMap = new HashMap<>();

    private List<TraceEdge> outEdges = new ArrayList<>();
    private List<TraceEdge> inEdges = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TraceNode node = (TraceNode) o;
        return Objects.equals(name, node.name) &&
                Objects.equals(action, node.action) &&
                Objects.equals(color, node.color);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, action, color, traceSetMap, outEdges, inEdges);
    }

    public List<TraceEdge> getOutEdges() {
        return outEdges;
    }

    public List<TraceEdge> getInEdges() {
        return inEdges;
    }

    public TraceNode(String name, Action action) {
        this.name = name;
        this.action = action;
    }

    public Map<Trace, Set<KendoLogRow>> getTraceSetMap() {
        return traceSetMap;
    }

    @Override
    public String getId() {
        return name;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public Action getAction() {
        return action;
    }
}
