package model.traces;

import model.processModel.ProcessStep;

import java.util.ArrayList;
import java.util.List;

public class BusinessProcessTrace {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private List<ProcessStep> steps = new ArrayList<>();
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public List<ProcessStep> getSteps() {
        return steps;
    }

}
