package model.traces;

import javafx.scene.paint.Color;
import model.graph.Edge;
import model.graph.Node;
import model.logs.kendo.KendoLogRow;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TraceEdge implements Edge {
    private String name;
    private Color color = Color.BLACK;

    private TraceNode to;
    private TraceNode from;
    private Map<Trace, Set<KendoLogRow>> fromTraceSetMap = new HashMap<>();
    private Map<Trace, Set<KendoLogRow>> toTraceSetMap = new HashMap<>();


    public TraceEdge(String name, TraceNode from, TraceNode to) {
        this.name = name;
        this.to = to;
        this.from = from;
    }

    public Map<Trace, Set<KendoLogRow>> getFromTraceSetMap() {
        return fromTraceSetMap;
    }
    public Map<Trace, Set<KendoLogRow>> getToTraceSetMap() {
        return toTraceSetMap;
    }

    public TraceNode getTraceTo() {
        return to;
    }

    public TraceNode getTraceFrom() {
        return from;
    }

    @Override
    public Node getTo() {
        return to;
    }

    @Override
    public Node getFrom() {
        return from;
    }

    @Override
    public String getLabel() {
        return name;
    }

    @Override
    public Color getColor() {
        return color;
    }
}
