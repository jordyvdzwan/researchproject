package model.processMapComparison;

import model.processModel.BusinessProcess;

public class ProcessComparison {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private BusinessProcess original;
    private BusinessProcess actual;
    private GraphComparison graphComparison;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public ProcessComparison(BusinessProcess original, BusinessProcess actual, GraphComparison graphComparison) {
        this.original = original;
        this.actual = actual;
        this.graphComparison = graphComparison;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public String getOriginalProcessName() {
        return original.getName();
    }
    public String getActualProcessName() {
        return actual.getName();
    }
    public double getScore() {
        return graphComparison.calculateScorePercentage();
    }
    public double getInvalidNodesPercentage() {
        return graphComparison.calculateInvalidNodePercentage();
    }
    public double getInvalidEdgesPercentage() {
        return graphComparison.calculateInvalidEdgePercentage();
    }
    public double getMissingNodesPercentage() {
        return graphComparison.calculateMissingNodePercentage();
    }
    public double getMissingEdgesPercentage() {
        return graphComparison.calculateMissingEdgePercentage();
    }
    public double getMissingPercentage() {
        return (getMissingNodesPercentage() + getMissingEdgesPercentage()) / 2;
    }
    public double getInvalidPercentage() {
        return (getInvalidNodesPercentage() + getInvalidEdgesPercentage()) / 2;
    }

    public GraphComparison getGraphComparison() {
        return graphComparison;
    }
    public BusinessProcess getOriginal() {
        return original;
    }
    public BusinessProcess getActual() {
        return actual;
    }
    public String getLabel() {
        return String.format("%.2f", getGraphComparison().calculateScorePercentage()) + "%";
    }
    public String getTooltipText() {
        return getOriginal().getName() + " vs " + getActual().getName();
    }
}
