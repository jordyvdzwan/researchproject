package model.processMapComparison;

import model.graph.Edge;
import model.graph.Node;
import model.transitionModel.TransitionEdge;

public class EdgePair {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private Edge original;
    private Edge actual;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public EdgePair(Edge original, Edge actual) {
        this.original = original;
        this.actual = actual;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public Edge getOriginal() {
        return original;
    }

    public Edge getActual() {
        return actual;
    }
}
