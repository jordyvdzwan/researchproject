package model.processMapComparison;

import javafx.scene.paint.Color;
import model.graph.Edge;
import model.graph.Graph;
import model.graph.Node;
import model.transitionModel.TransitionEdge;
import model.transitionModel.TransitionModel;
import model.transitionModel.TransitionNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GraphComparison {

    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private Graph originalGraph;
    private Graph actualGraph;

    private Set<Node> invalidNodes = new HashSet<>();
    private Set<NodePair> correctNodes = new HashSet<>();
    private Set<Node> missingNodes = new HashSet<>();

    private Set<Edge> invalidEdges = new HashSet<>();
    private Set<EdgePair> correctEdges = new HashSet<>();
    private Set<Edge> missingEdges = new HashSet<>();

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public GraphComparison(Graph originalGraph, Graph actualGraph) {
        this.originalGraph = originalGraph;
        this.actualGraph = actualGraph;
    }

    public double calculateScorePercentage() {
        return (((double)(correctEdges.size() + correctNodes.size())) / (double)(getTotalAmountOfEdges() + getTotalNrOfNodes())) * 100;
    }

    private int getTotalNrOfNodes() {
        return invalidNodes.size() + correctNodes.size() + missingNodes.size();
    }

    private int getTotalAmountOfEdges() {
        return invalidEdges.size() + correctEdges.size() + missingEdges.size();
    }

    public double calculateInvalidNodePercentage() {
        return ((double)invalidNodes.size() / (double)actualGraph.getNodes().size()) * 100;
    }
    public double calculateInvalidEdgePercentage() {
        return ((double)invalidEdges.size() / (double)actualGraph.getEdges().size()) * 100;
    }
    public double calculateMissingNodePercentage() {
        return ((double)missingNodes.size() / (double)originalGraph.getNodes().size()) * 100;
    }
    public double calculateMissingEdgePercentage() {
        return ((double)missingEdges.size() / (double)originalGraph.getEdges().size()) * 100;
    }
    public double calculateCorrectNodePercentage() {
        return ((double)correctNodes.size() / (double)getTotalNrOfNodes()) * 100;
    }
    public double calculateCorrectEdgePercentage() {
        return ((double)correctEdges.size() / (double)getTotalAmountOfEdges()) * 100;
    }

    public TransitionModel generateDiffTransitionModel() {
        Set<TransitionNode> transitionNodes = new HashSet<>();
        Set<TransitionEdge> transitionEdges = new HashSet<>();

        Map<String, TransitionNode> nodeMap = new HashMap<>();

        for (Node node : invalidNodes) {
            TransitionNode transitionNode = new TransitionNode(node.getId(), node.getAction());
            transitionNode.setColor(Color.RED);
            nodeMap.put(node.getId(), transitionNode);
            transitionNodes.add(transitionNode);
        }
        for (NodePair nodePair : correctNodes) {
            TransitionNode transitionNode = new TransitionNode(nodePair.getActual().getId(), nodePair.getActual().getAction());
            transitionNode.setColor(Color.GREEN);
            nodeMap.put(nodePair.getActual().getId(), transitionNode);
            transitionNodes.add(transitionNode);
        }
        for (Node node : missingNodes) {
            TransitionNode transitionNode = new TransitionNode(node.getId(), node.getAction());
            transitionNode.setColor(Color.GRAY);
            nodeMap.put(node.getId(), transitionNode);
            transitionNodes.add(transitionNode);
        }

        for (Edge edge : invalidEdges) {
            TransitionEdge transitionEdge = new TransitionEdge(
                    nodeMap.get(edge.getFrom().getId()),
                    nodeMap.get(edge.getTo().getId()),
                    edge.getLabel()
            );
            transitionEdge.setColor(Color.RED);
            transitionEdges.add(transitionEdge);
        }
        for (EdgePair edgePair : correctEdges) {
            TransitionEdge transitionEdge = new TransitionEdge(
                    nodeMap.get(edgePair.getActual().getFrom().getId()),
                    nodeMap.get(edgePair.getActual().getTo().getId()),
                    edgePair.getActual().getLabel()
            );
            transitionEdge.setColor(Color.GREEN);
            transitionEdges.add(transitionEdge);
        }
        for (Edge edge : missingEdges) {
            TransitionEdge transitionEdge = new TransitionEdge(
                    nodeMap.get(edge.getFrom().getId()),
                    nodeMap.get(edge.getTo().getId()),
                    edge.getLabel()
            );
            transitionEdge.setColor(Color.GRAY);
            transitionEdges.add(transitionEdge);
        }

        return new TransitionModel(transitionNodes, transitionEdges);
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public Set<Node> getInvalidNodes() {
        return invalidNodes;
    }
    public void setInvalidNodes(Set<Node> invalidNodes) {
        this.invalidNodes = invalidNodes;
    }
    public Set<NodePair> getCorrectNodes() {
        return correctNodes;
    }
    public void setCorrectNodes(Set<NodePair> correctNodes) {
        this.correctNodes = correctNodes;
    }
    public Set<Node> getMissingNodes() {
        return missingNodes;
    }
    public void setMissingNodes(Set<Node> missingNodes) {
        this.missingNodes = missingNodes;
    }
    public Set<Edge> getInvalidEdges() {
        return invalidEdges;
    }
    public void setInvalidEdges(Set<Edge> invalidEdges) {
        this.invalidEdges = invalidEdges;
    }
    public Set<EdgePair> getCorrectEdges() {
        return correctEdges;
    }
    public void setCorrectEdges(Set<EdgePair> correctEdges) {
        this.correctEdges = correctEdges;
    }
    public Set<Edge> getMissingEdges() {
        return missingEdges;
    }
    public void setMissingEdges(Set<Edge> missingEdges) {
        this.missingEdges = missingEdges;
    }
    public Graph getOriginalGraph() {
        return originalGraph;
    }
    public Graph getActualGraph() {
        return actualGraph;
    }
}
