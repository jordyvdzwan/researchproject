package model.processMapComparison;

public class ProcessComparisonResult {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private String name;
    private double maxScore;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public ProcessComparisonResult(String name, double maxScore) {
        this.name = name;
        this.maxScore = maxScore;
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(double maxScore) {
        this.maxScore = maxScore;
    }
}
