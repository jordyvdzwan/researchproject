package model.processMapComparison;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;
import model.processModel.ProcessStep;
import model.processModel.ProcessStepTransition;

import java.util.*;

public class ProcessMapComparison {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private ProcessMap original;
    private ProcessMap actual;

    private ObservableList<ProcessComparison> processComparisons = FXCollections.observableArrayList();
    private GraphComparison transitionModelComparison;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public ProcessMapComparison(ProcessMap original, ProcessMap actual, GraphComparison transitionModelComparison) {
        this.original = original;
        this.actual = actual;
        this.transitionModelComparison = transitionModelComparison;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public double calculateTotalProcessCompletionScore() {
        Map<BusinessProcess, Set<ProcessComparison>> comparisons = new HashMap<>();
        Set<BusinessProcess> originals = new HashSet<>();
        for (ProcessComparison comparison : processComparisons) {
            comparisons.putIfAbsent(comparison.getActual(), new HashSet<>());
            comparisons.get(comparison.getActual()).add(comparison);
            originals.add(comparison.getOriginal());
        }

        Map<BusinessProcess, Set<BusinessProcess>> topMap = new HashMap<>();
        for (Set<ProcessComparison> comparisonSet : comparisons.values()) {
            double min = 100;
            ProcessComparison minComparison = null;
            for (ProcessComparison comparison : comparisonSet) {
                if (comparison.getInvalidPercentage() < min) {
                    min = comparison.getInvalidPercentage();
                    minComparison = comparison;
                }
            }
            if (minComparison != null) {
                topMap.putIfAbsent(minComparison.getOriginal(), new HashSet<>());
                topMap.get(minComparison.getOriginal()).add(minComparison.getActual());
            }
        }

        List<Double> results = new ArrayList<>();
        for (BusinessProcess process : originals) {
            Set<String> originalElements = new HashSet<>();
            Set<String> actualElements = new HashSet<>();

            for (ProcessStep processStep : process.getProcessSteps()) {
                originalElements.add(processStep.getId());
            }
            for (ProcessStepTransition transition : process.getTransitions()) {
                originalElements.add(transition.getId());
            }

            if (topMap.get(process) != null) {
                for (BusinessProcess actualProcess : topMap.get(process)) {
                    for (ProcessStep processStep : actualProcess.getProcessSteps()) {
                        actualElements.add(processStep.getId());
                    }
                    for (ProcessStepTransition transition : actualProcess.getTransitions()) {
                        actualElements.add(transition.getId());
                    }
                }
            }

            Set<String> correctItems = new HashSet<>();
            for (String originalElement : originalElements) {
                if (actualElements.contains(originalElement)) correctItems.add(originalElement);
            }

            double total = ((originalElements.size() - correctItems.size()) + actualElements.size());
            if (total != 0)
                results.add(((double) correctItems.size() / total) * 100);
            else
                results.add(0d);
        }
        double count = 0;
        double total = 0d;
        for (Double result : results) {
            total += result;
            count++;
        }
        return count > 0 ? total / count : 0d;
    }


    public Double calculateAverageScore() {
        Double total = 0d;
        Double count = 0d;
        for (ProcessComparison processComparison : processComparisons) {
            count++;
            total += processComparison.getScore();
        }
        return (count > 0) ? total / count : 0;
    }
    public Map<BusinessProcess, Double> calculateOriginalMaxScores() {
        Map<BusinessProcess, Double> maxScores = new HashMap<>();
        for (ProcessComparison comparison : getProcessComparisons()) {
            if (!maxScores.containsKey(comparison.getOriginal())) {
                maxScores.put(comparison.getOriginal(), 0d);
            }
            if (maxScores.get(comparison.getOriginal()) < comparison.getGraphComparison().calculateScorePercentage())
                maxScores.put(comparison.getOriginal(), comparison.getGraphComparison().calculateScorePercentage());
        }
        return maxScores;
    }
    public Map<BusinessProcess, Double> calculateActualMaxScores() {
        Map<BusinessProcess, Double> maxScores = new HashMap<>();
        for (ProcessComparison comparison : getProcessComparisons()) {
            if (!maxScores.containsKey(comparison.getActual())) {
                maxScores.put(comparison.getActual(), 0d);
            }
            if (maxScores.get(comparison.getActual()) < comparison.getGraphComparison().calculateScorePercentage())
                maxScores.put(comparison.getActual(), comparison.getGraphComparison().calculateScorePercentage());
        }
        return maxScores;
    }
    public Map<BusinessProcess, Double> calculateOriginalMaxMissingScores() {
        Map<BusinessProcess, Double> maxScores = new HashMap<>();
        Map<BusinessProcess, Double> result = new HashMap<>();

        for (ProcessComparison comparison : getProcessComparisons()) {
            if (!maxScores.containsKey(comparison.getOriginal())) {
                maxScores.put(comparison.getOriginal(), 0d);
            }
            if (maxScores.get(comparison.getOriginal()) < comparison.getGraphComparison().calculateScorePercentage()) {
                maxScores.put(comparison.getOriginal(), comparison.getGraphComparison().calculateScorePercentage());
                result.put(comparison.getOriginal(), (comparison.getGraphComparison().calculateMissingEdgePercentage() + comparison.getGraphComparison().calculateMissingNodePercentage()) / 2);
            }
        }
        return result;
    }
    public Map<BusinessProcess, Double> calculateActualMaxMissingScores() {
        Map<BusinessProcess, Double> maxScores = new HashMap<>();
        Map<BusinessProcess, Double> result = new HashMap<>();
        for (ProcessComparison comparison : getProcessComparisons()) {
            if (!maxScores.containsKey(comparison.getActual())) {
                maxScores.put(comparison.getActual(), 0d);
            }
            if (maxScores.get(comparison.getActual()) < comparison.getGraphComparison().calculateScorePercentage()) {
                maxScores.put(comparison.getActual(), comparison.getGraphComparison().calculateScorePercentage());
            result.put(comparison.getActual(), (comparison.getGraphComparison().calculateMissingEdgePercentage() + comparison.getGraphComparison().calculateMissingNodePercentage()) / 2);
        }
        }
        return result;
    }
    public Map<BusinessProcess, Double> calculateOriginalMaxInvalidScores() {
        Map<BusinessProcess, Double> maxScores = new HashMap<>();
        Map<BusinessProcess, Double> result = new HashMap<>();
        for (ProcessComparison comparison : getProcessComparisons()) {
            if (!maxScores.containsKey(comparison.getOriginal())) {
                maxScores.put(comparison.getOriginal(), 0d);
            }
            if (maxScores.get(comparison.getOriginal()) < comparison.getGraphComparison().calculateScorePercentage()) {
                maxScores.put(comparison.getOriginal(), comparison.getGraphComparison().calculateScorePercentage());
            result.put(comparison.getOriginal(), (comparison.getGraphComparison().calculateInvalidEdgePercentage() + comparison.getGraphComparison().calculateInvalidNodePercentage()) / 2);
        }
        }
        return result;
    }
    public Map<BusinessProcess, Double> calculateActualMaxInvalidScores() {
        Map<BusinessProcess, Double> maxScores = new HashMap<>();
        Map<BusinessProcess, Double> result = new HashMap<>();
        for (ProcessComparison comparison : getProcessComparisons()) {
            if (!maxScores.containsKey(comparison.getActual())) {
                maxScores.put(comparison.getActual(), 0d);
            }
            if (maxScores.get(comparison.getActual()) < comparison.getGraphComparison().calculateScorePercentage()) {
                maxScores.put(comparison.getActual(), comparison.getGraphComparison().calculateScorePercentage());
        result.put(comparison.getActual(), (comparison.getGraphComparison().calculateInvalidEdgePercentage() + comparison.getGraphComparison().calculateInvalidNodePercentage()) / 2);
        }
        }
        return result;
    }


    public ProcessMap getOriginal() {
        return original;
    }
    public ProcessMap getActual() {
        return actual;
    }
    public GraphComparison getTransitionModelComparison() {
        return transitionModelComparison;
    }
    public ObservableList<ProcessComparison> getProcessComparisons() {
        return processComparisons;
    }
    public ProcessComparison getProgressComparison(BusinessProcess original, BusinessProcess actual) {
        for (ProcessComparison comparison : processComparisons) {
            if (comparison.getOriginal() == original && comparison.getActual() == actual) {
                return comparison;
            }
        }
        return null;
    }

    public double calculateAverageActualMaxScores() {
        Map<BusinessProcess, Double> data = calculateActualMaxScores();
        return data.values().size() > 0 ? data.values().stream().mapToDouble(f -> f).sum() / (double) data.values().size() : 0;
    }

    public double calculateAverageOriginalMaxScores() {
        Map<BusinessProcess, Double> data = calculateOriginalMaxScores();
        return data.values().size() > 0 ? data.values().stream().mapToDouble(f -> f).sum() / (double) data.values().size() : 0;
    }
    public double calculateAverageOriginalMaxMissingScores() {
        Map<BusinessProcess, Double> data = calculateOriginalMaxMissingScores();
        return data.values().size() > 0 ? data.values().stream().mapToDouble(f -> f).sum() / (double) data.values().size() : 0;
    }
    public double calculateAverageActualMaxMissingScores() {
        Map<BusinessProcess, Double> data = calculateActualMaxMissingScores();
        return data.values().size() > 0 ? data.values().stream().mapToDouble(f -> f).sum() / (double) data.values().size() : 0;
    }
    public double calculateAverageOriginalMaxInvalidScores() {
        Map<BusinessProcess, Double> data = calculateOriginalMaxInvalidScores();
        return data.values().size() > 0 ? data.values().stream().mapToDouble(f -> f).sum() / (double) data.values().size() : 0;
    }
    public double calculateAverageActualMaxInvalidScores() {
        Map<BusinessProcess, Double> data = calculateActualMaxInvalidScores();
        return data.values().size() > 0 ? data.values().stream().mapToDouble(f -> f).sum() / (double) data.values().size() : 0;
    }
}
