package model.processMapComparison;

import model.graph.Node;
import model.transitionModel.TransitionNode;

public class NodePair {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private Node original;
    private Node actual;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public NodePair(Node original, Node actual) {
        this.original = original;
        this.actual = actual;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public Node getOriginal() {
        return original;
    }

    public Node getActual() {
        return actual;
    }

}
