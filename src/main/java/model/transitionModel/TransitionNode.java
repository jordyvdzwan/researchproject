package model.transitionModel;

import javafx.scene.paint.Color;
import model.graph.Node;
import model.logs.model.Action;

public class TransitionNode implements Node {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private String name;
    private Color color = Color.WHITE;
    private Action action;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public TransitionNode(String name, Action action) {
        this.name = name;
        this.action = action;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    @Override
    public String getId() {
        return name;
    }

    @Override
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Action getAction() {
        return action;
    }
}
