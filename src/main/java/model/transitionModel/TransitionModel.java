package model.transitionModel;

import model.graph.Edge;
import model.graph.Graph;
import model.graph.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TransitionModel implements Graph {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private Set<TransitionNode> nodes;
    private Set<TransitionEdge> edges;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public TransitionModel(Set<TransitionNode> nodes, Set<TransitionEdge> edges) {
        this.nodes = nodes;
        this.edges = edges;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public List<Node> getNodes() {
        List<Node> result = new ArrayList();
        result.addAll(nodes);
        return result;
    }

    public List<Edge> getEdges() {
        List<Edge> result = new ArrayList();
        result.addAll(edges);
        return result;
    }

    public Set<TransitionNode> getTransitionNodes() {
        return nodes;
    }

    public Set<TransitionEdge> getTransitionsEdges() {
        return edges;
    }
}
