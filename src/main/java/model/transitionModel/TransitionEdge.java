package model.transitionModel;

import javafx.scene.paint.Color;
import model.graph.Edge;

public class TransitionEdge implements Edge {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private TransitionNode from;
    private TransitionNode to;
    private String label;
    private Color color = Color.BLACK;
    private int weight = 1;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public TransitionEdge(TransitionNode from, TransitionNode to, String label) {
        this.from = from;
        this.to = to;
        this.label = label;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    @Override
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public TransitionNode getFrom() {
        return from;
    }

    public TransitionNode getTo() {
        return to;
    }

    @Override
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void addToWeight(int nr) {
        weight += nr;
    }

    public int getWeight() {
        return weight;
    }
}
