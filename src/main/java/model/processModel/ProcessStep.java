package model.processModel;

import javafx.scene.paint.Color;
import model.graph.Node;
import model.logs.model.Action;

import java.util.List;

public class ProcessStep implements Node {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private String name;
    private Color color = Color.WHITE;
    private Action action;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public ProcessStep(String name, Action action) {
        this.name = name;
        this.action = action;
    }

    public ProcessStep(String name) {
        this.name = name;
    }

    public ProcessStep(Action action) {
        this.name = action.getName();
        this.action = action;
    }

    public boolean equalValue(ProcessStep step) {
        return step.action.equals(action) && step.name.equals(name) && step.color.equals(color);
    }

    public boolean containsEqualValue(List<ProcessStep> steps) {
        for (ProcessStep processStep : steps) {
            if (equalValue(processStep)) return true;
        }
        return false;
    }

    public ProcessStep getEqualValue(List<ProcessStep> steps) {
        for (ProcessStep processStep : steps) {
            if (equalValue(processStep)) return processStep;
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public Action getAction() {
        return action;
    }

    @Override
    public String getId() {
        return name;
    }

    @Override
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
