package model.processModel;

import javafx.scene.paint.Color;
import model.graph.Edge;
import model.graph.Node;

import java.util.List;
import java.util.Objects;

public class ProcessStepTransition implements Edge {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private ProcessStep to;
    private ProcessStep from;

    private String name;
    private Color color = Color.BLACK;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public ProcessStepTransition(ProcessStep from, ProcessStep to, String name) {
        this.to = to;
        this.from = from;
        this.name = name;
    }

    @Override
    public String toString() {
        return from.toString() + "-" + to.toString();
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public String getId() {
        return from.getId() + "-" + to.getId();
    }

    @Override
    public Node getTo() {
        return to;
    }
    @Override
    public Node getFrom() {
        return from;
    }
    @Override
    public String getLabel() {
        return name;
    }

    public ProcessStep getFromStep() { return from; }
    public ProcessStep getToStep() { return to; }

    @Override
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean containsEqualValue(List<ProcessStepTransition> transitions) {
        for (ProcessStepTransition transition : transitions) {
            if (equalValue(transition)) return true;
        }
        return false;
    }

    public boolean equalValue(ProcessStepTransition transition) {
        return from.equalValue(transition.getFromStep()) && to.equalValue(transition.getToStep()) && name.equals(transition.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessStepTransition that = (ProcessStepTransition) o;
        return Objects.equals(getTo(), that.getTo()) &&
                Objects.equals(getFrom(), that.getFrom());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getTo(), getFrom());
    }
}
