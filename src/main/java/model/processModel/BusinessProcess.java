package model.processModel;

import javafx.scene.paint.Color;
import model.graph.Edge;
import model.graph.Graph;
import model.graph.Node;
import model.processMapComparison.ProcessComparison;

import java.util.*;

public class BusinessProcess implements Graph {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    private Set<ProcessStep> startSteps = new HashSet<>();
    private Set<ProcessStep> endSteps = new HashSet<>();

    private List<ProcessStep> processSteps = new ArrayList<>();
    private List<ProcessStepTransition> transitions = new ArrayList<>();
    private String name;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public BusinessProcess(String name) {
        this.name = name;
    }

    public BusinessProcess(String name, BusinessProcess process) {
        this.name = name;
        Map<ProcessStep, ProcessStep> processStepProcessStepMap = new HashMap<>();
        for (ProcessStep step : process.getProcessSteps()) {
            ProcessStep step1 = new ProcessStep(step.getAction());
            processStepProcessStepMap.put(step, step1);
            processSteps.add(step1);
            if (process.getStartSteps().contains(step))
                startSteps.add(step1);
            if (process.getEndSteps().contains(step))
                endSteps.add(step1);
        }
        for (ProcessStepTransition transition : process.getTransitions()){
            transitions.add(new ProcessStepTransition(
                    processStepProcessStepMap.get(transition.getFrom()),
                    processStepProcessStepMap.get(transition.getTo()),
                    transition.getLabel()
            ));
        }
    }

    public void detectStartEndNodes() {
        Set<String> incoming = new HashSet<>();
        Set<String> outgoing = new HashSet<>();

        for (ProcessStepTransition transition : transitions) {
            incoming.add(transition.getToStep().getId());
            outgoing.add(transition.getFromStep().getId());
        }

        for (ProcessStep step : processSteps) {
            step.setColor(Color.WHITE);
        }

        startSteps.clear();
        endSteps.clear();
        for (ProcessStep step : processSteps) {
            if (!incoming.contains(step.getId())) {
                startSteps.add(step);
                step.setColor(Color.AQUA);
            }
            if (!outgoing.contains(step.getId())) {
                endSteps.add(step);
                step.setColor(Color.ORANGE);
            }
        }
    }

    public boolean equalValue(BusinessProcess process) {
        if (process.getProcessSteps().size() != getProcessSteps().size() ||
                process.getTransitions().size() != getTransitions().size() ||
                process.getStartSteps().size() != getStartSteps().size() ||
                process.getEndSteps().size() != getEndSteps().size()) return false;

        for (ProcessStep step : processSteps) {
            if (!step.containsEqualValue(process.getProcessSteps())) return false;
        }
        for (ProcessStepTransition transition : transitions) {
            if (!transition.containsEqualValue(process.getTransitions())) return false;
        }
        return name.equals(process.name);
    }

    public boolean containsEqualValue(Collection<BusinessProcess> businessProcesses) {
        for (BusinessProcess process : businessProcesses) {
            if (equalValue(process)) return true;
        }
        return false;
    }

    @Override
    public int hashCode() {

        return Objects.hash(getStartSteps(), getEndSteps(), getProcessSteps(), getTransitions(), getName());
    }

    @Override
    public List<Node> getNodes() {
        List<Node> result = new ArrayList<>();
        result.addAll(processSteps);
        return result;
    }

    @Override
    public List<Edge> getEdges() {
        List<Edge> result = new ArrayList<>();
        result.addAll(transitions);
        return result;
    }

    @Override
    public String toString() {
        return name;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public List<ProcessStep> getProcessSteps() {
        return processSteps;
    }
    public List<ProcessStepTransition> getTransitions() {
        return transitions;
    }
    public Set<ProcessStep> getStartSteps() {
        return startSteps;
    }
    public Set<ProcessStep> getEndSteps() {
        return endSteps;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public Integer getNrOfSteps() {
        return processSteps.size();
    }
    public Integer getNrOfTransitions() {
        return transitions.size();
    }

    public double getMergingMissingPercentage(BusinessProcess finalProcess) {
        int totalCount = 0;
        int missingCount = 0;

//        for (ProcessStep processStep : processSteps) {
//            if (!processStep.containsEqualValue(finalProcess.getProcessSteps())) missingCount++;
//            totalCount++;
//        }
        for (ProcessStepTransition transition : transitions) {
            if (!transition.containsEqualValue(finalProcess.getTransitions())) missingCount++;
            totalCount++;
        }

        return totalCount > 0 ? ((double) missingCount / (double) totalCount) * 100 : 0;
    }

    public void mergeProcess(BusinessProcess process) {
        for (ProcessStep processStep : process.getProcessSteps()) {
            if (!processStep.containsEqualValue(processSteps))
                processSteps.add(processStep);
        }
        for (ProcessStepTransition processStepTransition : process.getTransitions()) {
            if (!processStepTransition.containsEqualValue(transitions)) {
                ProcessStep from = processStepTransition.getFromStep().getEqualValue(processSteps);
                ProcessStep to = processStepTransition.getToStep().getEqualValue(processSteps);

                if (from == null) {
                    processSteps.add(processStepTransition.getFromStep());
                    from = processStepTransition.getFromStep();
                }
                if (to == null) {
                    processSteps.add(processStepTransition.getToStep());
                    to = processStepTransition.getToStep();
                }

                ProcessStepTransition processStepTransition1 = new ProcessStepTransition(from, to, processStepTransition.getLabel());
                processStepTransition1.setColor(processStepTransition.getColor());
                transitions.add(processStepTransition1);
            }
        }
    }
}
