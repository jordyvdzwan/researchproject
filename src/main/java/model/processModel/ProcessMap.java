package model.processModel;

import model.transitionModel.TransitionModel;

import java.util.ArrayList;
import java.util.List;

public class ProcessMap {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    private List<BusinessProcess> businessProcesses = new ArrayList<>();
    private String name;
    private TransitionModel transitionModel;
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    public ProcessMap(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public List<BusinessProcess> getBusinessProcesses() {
        return businessProcesses;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getNrOfProcesses() {
        return businessProcesses.size();
    }
    public TransitionModel getTransitionModel() {
        return transitionModel;
    }
    public void setTransitionModel(TransitionModel transitionModel) {
        this.transitionModel = transitionModel;
    }
}
