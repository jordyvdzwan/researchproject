package view;

import com.mxgraph.io.mxCodec;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;
import org.w3c.dom.Document;
import view.MainController;

import java.awt.*;

public class CustomGraphComponent extends mxGraphComponent {
    public CustomGraphComponent(mxGraph graph)
    {
        super(graph);

        // Sets switches typically used in an editor
//            setPageVisible(true);
        setGridVisible(true);
        setToolTips(true);
        getConnectionHandler().setCreateTarget(true);

        // Loads the defalt stylesheet from an external file
        mxCodec codec = new mxCodec();
        Document doc = mxUtils.loadDocument(MainController.class.getResource(
                "/default-style.xml")
                .toString());
        codec.decode(doc.getDocumentElement(), graph.getStylesheet());

        // Sets the background to white
        getViewport().setOpaque(true);
        getViewport().setBackground(Color.WHITE);
    }

}