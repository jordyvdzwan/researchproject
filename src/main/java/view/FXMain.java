package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FXMain extends Application {
    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/
    
    
    
    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/main.fxml"));
        primaryStage.setTitle("BusinessProcess mining Testing Framework v0.1");
        primaryStage.setScene(new Scene(root, 1280, 720));
        primaryStage.setMaximized(true);
//        primaryStage.getIcons().add(new Image(FXMain.class.getResourceAsStream("/view/icon.png")));

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
    
    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/


}
