package view;


import com.mxgraph.layout.*;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import com.sun.javafx.charts.Legend;
import com.sun.javaws.exceptions.InvalidArgumentException;
import controller.LoggableTask;
import controller.TaskParameters;
import controller.logGeneratorTasks.ErrorInsertingLogGeneratorTask;
import controller.logImportTasks.KendoLogImportParameters;
import controller.parametereizedProcessMapComparators.processMapComparators.DefaultParameterizedParameterizedProcessMapComparatorTask;
import controller.parametereizedProcessMapComparators.processMapComparators.ParameterizedProcessMapComparatorTask;
import controller.parametereizedProcessMapComparators.processMapComparators.ParameterizedProcessMapComparatorTaskParameters;
import controller.parametereizedProcessMapComparators.processMapComparators.Score;
import controller.processComparators.DefaultProcessComparatorTask;
import controller.processComparators.ProcessComparatorTaskParameters;
import controller.processMapComparators.DefaultProcessMapComparatorTask;
import controller.processMapComparators.ProcessMapComparatorTask;
import controller.processMapComparators.ProcessMapComparatorTaskParameters;
import controller.processMapGenerationTasks.*;
import controller.processMapMiningTasks.*;
import controller.tracers.*;
import controller.transitionMapBuilderTasks.*;
import controller.logImportTasks.KendoLogXLSXReadTask;
import controller.logGeneratorTasks.KendoLogGenerationParameters;
import controller.logGeneratorTasks.LogGeneratorTask;
import controller.logGeneratorTasks.SimpleLogGeneratorTask;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;
import model.graph.Edge;
import model.graph.Graph;
import model.graph.Node;
import model.logs.kendo.KendoLog;
import model.processMapComparison.GraphComparison;
import model.processMapComparison.ProcessComparison;
import model.processMapComparison.ProcessComparisonResult;
import model.processMapComparison.ProcessMapComparison;
import model.processModel.BusinessProcess;
import model.processModel.ProcessMap;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.logging.Logger;

@SuppressWarnings({"unused", "WeakerAccess"})
public class MainController {

    /*-----------------------------------------------------------
                           Attributes
     -----------------------------------------------------------*/

    public SwingNode graphContainer;

    public SwingNode originalGraphContainer;
    public SwingNode actualGraphContainer;

    //List views
    public ListView<File> xlsxImportFilesList;
    public ListView<KendoLog> executionLogList;

    //Labels
    public TextField xlsxImportNameField;
    public Button xlsxImportAddFilesButton;
    public Button xlsxImportStartImportButton;


    public Button showKendoLogAsGraphButton;

    public TextField processMapNameTextField;
    public TableView<ProcessMap> processMapTableView;

    public TableColumn processMapNameTableColumn;
    public TableColumn processMapNrOfProcessesTableColumn;

    public ComboBox<ProcessMapDataGenerationTask> processMapGenerationType;
    public Button processMapGenerateButton;

    public ComboBox<LogGeneratorTask> kendologGeneratorTypeComboBox;
    public Button generateKendoLogDataButton;

    public ComboBox<TransitionModelBuilderTask> transitionModelBuilderType;
    public ComboBox<ProcessMap> kendoLogGeneratorProcessModelComboBox;

    public TextField kendoLogGeneratorSeed;
    public TextField kendoLogGeneratorNrOfRepeats;
    public TextField kendoLogGeneratorErrorInsertionChance;


    public AnchorPane processComparisonTableContainer;
    public ComboBox<ProcessMap> compareProcessMapOriginalComboBox;
    public ComboBox<ProcessMap> compareProcessMapActualComboBox;
    public ComboBox<ProcessMapComparatorTask> determineProcessMapComparatorComboBox;
    public Button determineProcessMapComparisonButton;
    public CheckBox compareProcessMapCalculateProcesses;

    public TextField processComparisonInvalidNodes;
    public TextField processComparisonInvalidEdges;
    public TextField processComparisonMissingNodes;
    public TextField processComparisonMissingEdges;
    public TextField processComparisonOriginalProcessName;
    public TextField processComparisonActualProcessName;

    public TextField processMapGenerationNrOfProcesses;
    public TextField processMapGenerationNrOfSteps;
    public TextField processMapGenerationStepDeviation;
    public TextField processMapGenerationNrOfTransitions;
    public TextField processMapGenerationTransitionsDeviation;
    public TextField processMapGenerationNrOfStartSteps;
    public TextField processMapGenerationStartStepDeviation;
    public TextField processMapGenerationNrOfEndSteps;
    public TextField processMapGenerationEndStepDeviation;
    public TextField processMapGenerationRandomSeed;

    public ComboBox<KendoLog> mineProcessMapKendoLogComboBox;
    public Button mineProcessMapButton;
    public ComboBox<ProcessMapMiningTask> mineProcessMapMinerComboBox;
    public TextField mineProcessMapLimit;

    public Button dictFileSelectButton;
    public Label dictFilePathLabel;
    public File dictFile;
    public ComboBox<TraceBuilder> processMinerTracerComboBox;

    public TableColumn processNrOfStepsTableColumn;
    public TableColumn processNameTableColumn;
    public TableColumn processNrOfTransitionsTableColumn;
    public TableView<BusinessProcess> processTableView;

    public TableView<ProcessComparison> processComparisonsTableView;
    public TableColumn originalProcessNameTableColumn;
    public TableColumn actualProcessNameTableColumn;
    public TableColumn processComparisonScoreTableColumn;
    public TableColumn processComparisonInvalidNodesTableColumn;
    public TableColumn processComparisonInvalidEdgesTableColumn;
    public TableColumn processComparisonMissingNodesTableColumn;
    public TableColumn processComparisonMissingEdgesTableColumn;

    public TextField mergingThresholdTextField;
    public TextField errorCooldownTextField;
    public TextField errorThresholdTextField;
    public TextField nodesThresholdTextField;

    public TableView<ProcessComparisonResult> processComparisonOriginalHighestScores;
    public TableColumn processComparisonOriginalHighestScoresProcessNameColumn;
    public TableColumn processComparisonOriginalHighestScoresHSColumn;
    public TableView<ProcessComparisonResult> processComparisonActualHighestScores;
    public TableColumn processComparisonActualHighestScoresProcessNameColumn;
    public TableColumn processComparisonActualHighestScoresHSColumn;

    public BarChart originalPercentilesBarChart;
    public LineChart<Number, Number> parameterizedComparisonResultLineChart;

    public TextField parameterizedMapComparisonStartIndex;
    public TextField parameterizedMapComparisonIncrementer;
    public TextField parameterizedMapComparisonEndIndex;
    public ComboBox<String> parameterizedMapComparisonParameter;
//    public ComboBox<String> parameterizedMapComparisonScore;
    public ComboBox<ProcessMap> parameterizedMapComparisonOriginal;
    public TextArea tracerBlacklistTextArea;

    public TextField pmCompInvalidNodes;
    public TextField pmCompInvalidEdges;
    public TextField pmCompMissingNodes;
    public TextField pmCompMissingEdges;
    public TextField pmCompTotalSimilarity;
    public TextField pmCompCorrectNodes;
    public TextField pmCompCorrectEdges;
    public TextField pmCompTotalProcSimilarity1;
    public TextField pmCompAvgMaxOriginalSimilarity;


    private mxGraphComponent graphComponent;
    private mxGraphComponent originalGraphComponent;
    private mxGraphComponent actualGraphComponent;
    private ProcessMapComparison processMapComparison;

    /*-----------------------------------------------------------
                            Methods
     -----------------------------------------------------------*/

    //INIT
    @FXML
    public void initialize() {
        graphComponent = loadMXGraph(graphContainer);
        originalGraphComponent = loadMXGraph(originalGraphContainer);
        actualGraphComponent = loadMXGraph(actualGraphContainer);

        determineProcessMapComparatorComboBox.getItems().add(new DefaultProcessMapComparatorTask());
        determineProcessMapComparatorComboBox.getSelectionModel().select(0);

        processMinerTracerComboBox.getItems().add(new NovSessionExtendedLoadPageTraceBuilder());
        processMinerTracerComboBox.getItems().add(new NovSessionLoadPageTraceBuilder());
        processMinerTracerComboBox.getItems().add(new NovSessionTraceBuilder());
        processMinerTracerComboBox.getItems().add(new UserTraceBuilder());
        processMinerTracerComboBox.getItems().add(new UELPTraceBuilder());
        processMinerTracerComboBox.getItems().add(new MainRecordIdTraceBuilder());
        processMinerTracerComboBox.getItems().add(new MRIELPTraceBuilder());
        processMinerTracerComboBox.getItems().add(new ExtendedNovSessionTraceBuilder());
        processMinerTracerComboBox.getSelectionModel().select(2);

        processMapGenerationType.getItems().add(new StaticPMDGenerationTask());
        processMapGenerationType.getItems().add(new DynamicPMDGenerationTask());
        processMapGenerationType.getItems().add(new KendoPMDGenerationTask());
        processMapGenerationType.getItems().add(new LineBasedPMDGenerationTask());
        processMapGenerationType.getItems().add(new ForwardLineBasedPMDGenerationTask());
        processMapGenerationType.getSelectionModel().select(4);


        mineProcessMapMinerComboBox.getItems().add(new LimitProcessMapMiningTask());
        mineProcessMapMinerComboBox.getItems().add(new TraceFrequencyProcessMapMiningTask());
        mineProcessMapMinerComboBox.getItems().add(new MTFProcessMapMiningTask());
        mineProcessMapMinerComboBox.getItems().add(new ERMTFProcessMapMiningTask());
        mineProcessMapMinerComboBox.getSelectionModel().select(2);


        kendologGeneratorTypeComboBox.getItems().add(new SimpleLogGeneratorTask());
        kendologGeneratorTypeComboBox.getItems().add(new ErrorInsertingLogGeneratorTask());
        kendologGeneratorTypeComboBox.getSelectionModel().select(1);


        transitionModelBuilderType.getItems().add(new LoadPageQuantityTransitionModelBuilder());
        transitionModelBuilderType.getItems().add(new ExtendedLoadPageQuantityTransitionModelBuilder());
        transitionModelBuilderType.getItems().add(new SimpleTransitionModelBuilder());
        transitionModelBuilderType.getSelectionModel().select(2);


        kendoLogGeneratorProcessModelComboBox.setItems(processMapTableView.getItems());
        compareProcessMapActualComboBox.setItems(processMapTableView.getItems());
        compareProcessMapOriginalComboBox.setItems(processMapTableView.getItems());
        parameterizedMapComparisonOriginal.setItems(processMapTableView.getItems());

        mineProcessMapKendoLogComboBox.setItems(executionLogList.getItems());

        parameterizedMapComparisonParameter.getItems().add("Traffic Threshold");
        parameterizedMapComparisonParameter.getItems().add("Merge Threshold");
        parameterizedMapComparisonParameter.getItems().add("Error Threshold");
        parameterizedMapComparisonParameter.getItems().add("Nodes Threshold");
        parameterizedMapComparisonParameter.getItems().add("Error Cooldown");
        parameterizedMapComparisonParameter.getSelectionModel().select(0);
//        parameterizedMapComparisonScore.getItems().add("Average Score");
//        parameterizedMapComparisonScore.getSelectionModel().select(0);


        kendoLogGeneratorSeed.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        kendoLogGeneratorSeed.setText("1");
        kendoLogGeneratorNrOfRepeats.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        kendoLogGeneratorNrOfRepeats.setText("20");
        kendoLogGeneratorErrorInsertionChance.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
        kendoLogGeneratorErrorInsertionChance.setText("20");


        parameterizedMapComparisonStartIndex.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        parameterizedMapComparisonStartIndex.setText("2");
        parameterizedMapComparisonIncrementer.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        parameterizedMapComparisonIncrementer.setText("2");
        parameterizedMapComparisonEndIndex.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        parameterizedMapComparisonEndIndex.setText("100");


        processMapGenerationNrOfProcesses.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationNrOfProcesses.setText("10");
        processMapGenerationNrOfSteps.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationNrOfSteps.setText("25");
        processMapGenerationStepDeviation.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationStepDeviation.setText("4");
        processMapGenerationNrOfTransitions.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationNrOfTransitions.setText("28");
        processMapGenerationTransitionsDeviation.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationTransitionsDeviation.setText("8");
        processMapGenerationNrOfStartSteps.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationNrOfStartSteps.setText("3");
        processMapGenerationStartStepDeviation.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationStartStepDeviation.setText("2");
        processMapGenerationNrOfEndSteps.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationNrOfEndSteps.setText("3");
        processMapGenerationEndStepDeviation.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationEndStepDeviation.setText("2");
        processMapGenerationRandomSeed.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        processMapGenerationRandomSeed.setText("0");

        mineProcessMapLimit.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        mineProcessMapLimit.setText("1");
        mergingThresholdTextField.setTextFormatter(new TextFormatter<>(new DoubleStringConverter()));
        mergingThresholdTextField.setText("1");
        errorCooldownTextField.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        errorCooldownTextField.setText("1");
        errorThresholdTextField.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        errorThresholdTextField.setText("1");
        nodesThresholdTextField.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        nodesThresholdTextField.setText("1");

        processComparisonOriginalHighestScoresProcessNameColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparisonResult,String>("name")
        );
        processComparisonOriginalHighestScoresHSColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparisonResult,Double>("maxScore")
        );
        processComparisonActualHighestScoresProcessNameColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparisonResult,String>("name")
        );
        processComparisonActualHighestScoresHSColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparisonResult,Double>("maxScore")
        );


        originalProcessNameTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparison,String>("originalProcessName")
        );
        actualProcessNameTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparison,String>("actualProcessName")
        );
        processComparisonScoreTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparison,String>("score")
        );
        processComparisonInvalidNodesTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparison,String>("invalidNodesPercentage")
        );
        processComparisonInvalidEdgesTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparison,String>("invalidEdgesPercentage")
        );
        processComparisonMissingNodesTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparison,String>("missingNodesPercentage")
        );
        processComparisonMissingEdgesTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessComparison,String>("missingEdgesPercentage")
        );


        processNrOfStepsTableColumn.setCellValueFactory(
                new PropertyValueFactory<BusinessProcess,String>("nrOfSteps")
        );
        processNameTableColumn.setCellValueFactory(
                new PropertyValueFactory<BusinessProcess,String>("name")
        );
        processNrOfTransitionsTableColumn.setCellValueFactory(
                new PropertyValueFactory<BusinessProcess,String>("nrOfTransitions")
        );

        processMapNameTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessMap,String>("name")
        );
        processMapNrOfProcessesTableColumn.setCellValueFactory(
                new PropertyValueFactory<ProcessMap,Integer>("nrOfProcesses")
        );
    }
    private mxGraphComponent loadMXGraph(SwingNode swingNode) {
        mxGraph graph = new mxGraph();
        Object parent = graph.getDefaultParent();
        graph.getModel().beginUpdate();
        try
        {
//            Object v1 = graph.insertVertex(parent, null, "Hello", 20, 20, 80,
//                    30);
//            Object v2 = graph.insertVertex(parent, null, "World!", 240, 150,
//                    80, 30);
//            Object v3 = graph.insertVertex(parent, null, "World!", 240, 150,
//                    80, 30);
//            Object v4 = graph.insertVertex(parent, null, "World!", 240, 150,
//                    80, 30);
//            Object v5 = graph.insertVertex(parent, null, "World!", 240, 150,
//                    80, 30);
//            graph.insertEdge(parent, null, "", v1, v2);
//            graph.insertEdge(parent, null, "", v2, v3);
//            graph.insertEdge(parent, null, "", v4, v3);
//            graph.insertEdge(parent, null, "", v3, v5);
        } finally {
            graph.getModel().endUpdate();
        }
        mxGraphLayout layout = new mxHierarchicalLayout(graph);
        layout.execute(parent);
        mxGraphComponent graphComponent = new CustomGraphComponent(graph) {
            @Override public boolean isPanningEvent(MouseEvent event) {
                return true;
            }
        };

        swingNode.setOnScroll(event -> {
            if (event.getDeltaY() > 0) {
                graphComponent.zoomIn();
            } else {
                graphComponent.zoomOut();
            }
        });
        swingNode.setContent(graphComponent);
        return graphComponent;
    }

    //Input handling
    public void handleProcessSelection() {
        BusinessProcess businessProcess = processTableView.getSelectionModel().getSelectedItem();
        if (businessProcess != null) {
            displayGraph(businessProcess, graphComponent);
            hierarchicalLayout();
        }
    }
    public void handleShowProcessMap() {
        ProcessMap processMap = processMapTableView.getSelectionModel().getSelectedItem();
        if (processMap != null) {
            displayGraph(processMap.getTransitionModel(), graphComponent);
        } else showWarning("There was no data there... :(");
    }
    public void handleProcessMapTableClick() {
        ProcessMap processMap = processMapTableView.getSelectionModel().getSelectedItem();
        displayProcessMap(processMap);
    }
    public void xlsxImportAddFiles(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        List<File> files = fileChooser.showOpenMultipleDialog(((javafx.scene.Node)actionEvent.getSource()).getScene().getWindow());
        xlsxImportFilesList.getItems().addAll(files);
    }


    //Displaying objects
    private void displayGraph(Graph graphData, mxGraphComponent component) {
        mxGraph graph = new mxGraph();
        graph.getModel().beginUpdate();

        Map<Node, Object> objectMap = new HashMap<>();

        for (Node node : graphData.getNodes()) {
            objectMap.put(node, graph.insertVertex(graph.getDefaultParent(), null, node.getId(), 240, 150, node.getId().length() * 6, 30));
            graph.setCellStyle("fillColor=" +
                    String.format( "#%02X%02X%02X",
                            (int)( node.getColor().getRed() * 255 ),
                            (int)( node.getColor().getGreen() * 255 ),
                            (int)( node.getColor().getBlue() * 255 ) ), new Object[]{objectMap.get(node)});
        }

        for (Edge edge : graphData.getEdges()) {
            Object object = graph.insertEdge(
                    graph.getDefaultParent(),
                    null,
                    "" + edge.getLabel(),
                    objectMap.get(edge.getFrom()),
                    objectMap.get(edge.getTo())
            );
            graph.setCellStyle("strokeColor=" +
                    String.format( "#%02X%02X%02X",
                    (int)( edge.getColor().getRed() * 255 ),
                    (int)( edge.getColor().getGreen() * 255 ),
                    (int)( edge.getColor().getBlue() * 255 ) ), new Object[]{object});
        }

        graph.getModel().endUpdate();
        graph.refresh();
        graph.repaint();
        component.setGraph(graph);
        component.refresh();

        mxFastOrganicLayout layout = new mxFastOrganicLayout(graph);
        layout.setMinDistanceLimit(0.001);
        layout.execute(component.getGraph().getDefaultParent());
        component.zoomAndCenter();
    }
    private void displayProcessMap(ProcessMap processMap) {
        processTableView.getItems().clear();

        if (processMap != null) {
            processMapNameTextField.setText(processMap.getName());
            processTableView.getItems().addAll(processMap.getBusinessProcesses());
        }
    }

    private void displayProcessMapComparison(ProcessMapComparison comparison) {
        processComparisonInvalidNodes.setText("");
        processComparisonInvalidEdges.setText("");
        processComparisonMissingNodes.setText("");
        processComparisonMissingEdges.setText("");
        processComparisonOriginalProcessName.setText("");
        processComparisonActualProcessName.setText("");


        pmCompInvalidNodes.setText("" + comparison.getTransitionModelComparison().calculateInvalidNodePercentage());
        pmCompInvalidEdges.setText("" + comparison.getTransitionModelComparison().calculateInvalidEdgePercentage());
        pmCompMissingNodes.setText("" + comparison.getTransitionModelComparison().calculateMissingNodePercentage());
        pmCompMissingEdges.setText("" + comparison.getTransitionModelComparison().calculateMissingEdgePercentage());
        pmCompTotalSimilarity.setText("" + comparison.getTransitionModelComparison().calculateScorePercentage());
        pmCompCorrectNodes.setText("" + comparison.getTransitionModelComparison().calculateCorrectNodePercentage());
        pmCompCorrectEdges.setText("" + comparison.getTransitionModelComparison().calculateCorrectEdgePercentage());
        pmCompTotalProcSimilarity1.setText("" + comparison.calculateTotalProcessCompletionScore());
        pmCompAvgMaxOriginalSimilarity.setText("" + comparison.calculateAverageOriginalMaxScores());

        processComparisonsTableView.setItems(processMapComparison.getProcessComparisons());
        processComparisonsTableView.setOnMouseClicked(event -> {
            ProcessComparison comparison1 = processComparisonsTableView.getSelectionModel().getSelectedItem();
            if (comparison1 != null) {
                displayProcessComparison(comparison1);
            }
        });

        clearComparisonMatrix();
        refreshProcessMapComparison(null);
    }

    public void displayProcessComparisonOriginalMaxScores(ProcessMapComparison processMapComparison) {
        Map<BusinessProcess, Double> maxScores = processMapComparison.calculateOriginalMaxScores();

        processComparisonOriginalHighestScores.getItems().clear();
        for (BusinessProcess businessProcess : maxScores.keySet()) {
            processComparisonOriginalHighestScores.getItems().add(new ProcessComparisonResult(businessProcess.getName(), maxScores.get(businessProcess)));
        }
    }
    public void displayProcessComparisonActualMaxScores(ProcessMapComparison processMapComparison) {
        Map<BusinessProcess, Double> maxScores = processMapComparison.calculateActualMaxScores();

        processComparisonActualHighestScores.getItems().clear();
        for (BusinessProcess businessProcess : maxScores.keySet()) {
            processComparisonActualHighestScores.getItems().add(new ProcessComparisonResult(businessProcess.getName(), maxScores.get(businessProcess)));
        }
    }
    @SuppressWarnings("unchecked")
    public void displayProcessComparisonOriginalPercentiles(ProcessMapComparison processMapComparison) {
        originalPercentilesBarChart.getData().clear();

        Map<BusinessProcess, Double> originalMaxScores = new HashMap<>();
        for (ProcessComparison comparison : processMapComparison.getProcessComparisons()) {
            if (!originalMaxScores.containsKey(comparison.getOriginal())) {
                originalMaxScores.put(comparison.getOriginal(), 0d);
            }
            if (originalMaxScores.get(comparison.getOriginal()) < comparison.getGraphComparison().calculateScorePercentage())
                originalMaxScores.put(comparison.getOriginal(), comparison.getGraphComparison().calculateScorePercentage());
        }

        Map<Integer, Integer> originalScores = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            originalScores.put(i, 0);
        }
        for (Double score : originalMaxScores.values()) {
            int percentile = (int) Math.floor(score / 10d);
            if (percentile == 10) percentile = 9;
            originalScores.put(percentile, originalScores.get(percentile) + 1);
        }

        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName("Original");
        dataSeries1.getData().add(new XYChart.Data("0", originalScores.get(0)));
        dataSeries1.getData().add(new XYChart.Data("10"  , originalScores.get(1)));
        dataSeries1.getData().add(new XYChart.Data("20"  , originalScores.get(2)));
        dataSeries1.getData().add(new XYChart.Data("30"  , originalScores.get(3)));
        dataSeries1.getData().add(new XYChart.Data("40"  , originalScores.get(4)));
        dataSeries1.getData().add(new XYChart.Data("50"  , originalScores.get(5)));
        dataSeries1.getData().add(new XYChart.Data("60"  , originalScores.get(6)));
        dataSeries1.getData().add(new XYChart.Data("70"  , originalScores.get(7)));
        dataSeries1.getData().add(new XYChart.Data("80"  , originalScores.get(8)));
        dataSeries1.getData().add(new XYChart.Data("90"  , originalScores.get(9)));


        Map<BusinessProcess, Double> actualMaxScores = new HashMap<>();
        for (ProcessComparison comparison : processMapComparison.getProcessComparisons()) {
            if (!actualMaxScores.containsKey(comparison.getActual())) {
                actualMaxScores.put(comparison.getActual(), 0d);
            }
            if (actualMaxScores.get(comparison.getActual()) < comparison.getGraphComparison().calculateScorePercentage())
                actualMaxScores.put(comparison.getActual(), comparison.getGraphComparison().calculateScorePercentage());
        }

        Map<Integer, Integer> actualScores = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            actualScores.put(i, 0);
        }
        for (Double score : actualMaxScores.values()) {
            int percentile = (int) Math.floor(score / 10d);
            if (percentile == 10) percentile = 9;
            actualScores.put(percentile, actualScores.get(percentile) + 1);
        }

        XYChart.Series dataSeries2 = new XYChart.Series();
        dataSeries2.setName("Actual");
        dataSeries2.getData().add(new XYChart.Data("0", actualScores.get(0)));
        dataSeries2.getData().add(new XYChart.Data("10"  , actualScores.get(1)));
        dataSeries2.getData().add(new XYChart.Data("20"  , actualScores.get(2)));
        dataSeries2.getData().add(new XYChart.Data("30"  , actualScores.get(3)));
        dataSeries2.getData().add(new XYChart.Data("40"  , actualScores.get(4)));
        dataSeries2.getData().add(new XYChart.Data("50"  , actualScores.get(5)));
        dataSeries2.getData().add(new XYChart.Data("60"  , actualScores.get(6)));
        dataSeries2.getData().add(new XYChart.Data("70"  , actualScores.get(7)));
        dataSeries2.getData().add(new XYChart.Data("80"  , actualScores.get(8)));
        dataSeries2.getData().add(new XYChart.Data("90"  , actualScores.get(9)));

        originalPercentilesBarChart.getData().add(dataSeries1);
        originalPercentilesBarChart.getData().add(dataSeries2);
    }
    public void refreshProcessMapComparison(ActionEvent actionEvent) {
        if (processMapComparison != null) {
            displayProcessComparisonOriginalMaxScores(processMapComparison);
            displayProcessComparisonActualMaxScores(processMapComparison);
            displayProcessComparisonOriginalPercentiles(processMapComparison);
        } else showWarning("You must first compare two process maps");
    }
    private void displayTransitionModelComparison(GraphComparison comparison) {
        displayGraph(comparison.generateDiffTransitionModel(), graphComponent);
        displayGraph(comparison.getOriginalGraph(), originalGraphComponent);
        displayGraph(comparison.getActualGraph(), actualGraphComponent);
        actualHierachicalLayout();
        originalHierachicalLayout();
        hierarchicalLayout();
    }
    private void displayProcessComparison(ProcessComparison comparison) {
        processComparisonInvalidNodes.setText(comparison.getGraphComparison().calculateInvalidNodePercentage() + "%");
        processComparisonInvalidEdges.setText(comparison.getGraphComparison().calculateInvalidEdgePercentage() + "%");
        processComparisonMissingNodes.setText(comparison.getGraphComparison().calculateMissingNodePercentage() + "%");
        processComparisonMissingEdges.setText(comparison.getGraphComparison().calculateMissingEdgePercentage() + "%");
        processComparisonOriginalProcessName.setText(comparison.getOriginal().getName());
        processComparisonActualProcessName.setText(comparison.getActual().getName());

        displayGraph(comparison.getGraphComparison().generateDiffTransitionModel(), graphComponent);
//        comparison.getOriginal().detectStartEndNodes();
//        comparison.getActual().detectStartEndNodes();
        displayGraph(comparison.getOriginal(), originalGraphComponent);
        displayGraph(comparison.getActual(), actualGraphComponent);
        actualHierachicalLayout();
        originalHierachicalLayout();
        hierarchicalLayout();
    }
    private void displayProcessComparisonTable(ProcessMapComparison processMapComparison, Function<ProcessComparison, Double> textRetriever, Function<ProcessComparison, Double> colorRetriever) {
        GridPane gridPane = new GridPane();
        List<BusinessProcess> originalProcesses = processMapComparison.getOriginal().getBusinessProcesses();
        List<BusinessProcess> actualProcesses = processMapComparison.getActual().getBusinessProcesses();

        gridPane.getRowConstraints().add(new RowConstraints(30));
        gridPane.getColumnConstraints().add(new ColumnConstraints(180));

        int index = 0;
        for (BusinessProcess businessProcess : originalProcesses) {
            gridPane.getRowConstraints().add(new RowConstraints(30));
            gridPane.add(createProcessLabel(businessProcess), 0, ++index, 1, 1);
        }

        index = 0;
        for (BusinessProcess businessProcess : actualProcesses) {
            gridPane.getColumnConstraints().add(new ColumnConstraints(60));
            gridPane.add(createProcessLabel(businessProcess), ++index, 0, 1, 1);
        }

        int originalIndex = 0;
        int actualIndex = 0;
        for (BusinessProcess original : originalProcesses) {
            originalIndex++;
            actualIndex = 0;
            for (BusinessProcess actual : actualProcesses) {
                actualIndex++;
                ProcessComparison comparison = processMapComparison.getProgressComparison(original, actual);
                if (comparison == null) {
                    gridPane.add(
                            createClickableCalculateProcessComparisonLabel(original, actual, textRetriever, colorRetriever),
                            actualIndex,
                            originalIndex,
                            1,
                            1
                    );
                } else {
                    AnchorPane anchorPane = new AnchorPane();
                    anchorPane.setStyle("-fx-border-color: grey; -fx-border-width: 2px;");
                    anchorPane.getChildren().add(createClickableProcessComparisonLabel(comparison, textRetriever));
                    int score = (int) colorRetriever.apply(comparison).doubleValue();
                    Color color = getColor(score);
                    anchorPane.setStyle("-fx-background-color: " + String.format( "#%02X%02X%02X",
                            (int) (color.getRed() * 255),
                            (int) (color.getGreen() * 255),
                            (int) (color.getBlue() * 255)
                    ));
                    gridPane.add(
                            anchorPane,
                            actualIndex,
                            originalIndex,
                            1,
                            1
                    );
                }
            }
        }

        processComparisonTableContainer.getChildren().clear();
        processComparisonTableContainer.getChildren().add(gridPane);
    }
    private void clearComparisonMatrix() {
        processComparisonTableContainer.getChildren().clear();
    }
    public void showProcessComparisonMatrix(Function<ProcessComparison, Double> textRetriever, Function<ProcessComparison, Double> colorRetriever) {
        if (processMapComparison != null) displayProcessComparisonTable(processMapComparison, textRetriever, colorRetriever);
        else showWarning("No process comparisons executed yet.");
    }
    public void showProcessComparisonOverallMatrix(ActionEvent actionEvent) {
        showProcessComparisonMatrix(
                (comparison) -> comparison.getGraphComparison().calculateScorePercentage(),
                (comparison) -> comparison.getGraphComparison().calculateScorePercentage()
        );
    }
    public void showProcessComparisonMissingMatrix(ActionEvent actionEvent) {
        showProcessComparisonMatrix((comparison) -> comparison.getMissingPercentage(),
                (comparison) -> 100 - comparison.getMissingPercentage()
        );
    }
    public void showProcessComparisonInvalidMatrix(ActionEvent actionEvent) {
        showProcessComparisonMatrix((comparison) -> comparison.getInvalidPercentage(),
                (comparison) -> 100 - comparison.getInvalidPercentage()
        );
    }

    private Color getColor(int score) {
        return new Color(
                ( score > 80 ? (double)(100 - score) / (double)20 : 1 ),
                ( score < 80 ? (((double)score/(double)80) ) : 1 ),
                0,
                1);
    }
    private Label createProcessLabel(BusinessProcess businessProcess) {
        Label label = new Label(businessProcess.getName());
        label.setTooltip(new Tooltip(businessProcess.getName()));
        return label;
    }
    private Label createClickableProcessComparisonLabel(ProcessComparison comparison, Function<ProcessComparison, Double> textRetriever) {
        Label label = new Label(String.format("%.2f", textRetriever.apply(comparison)) + "%");
        label.setOnMouseClicked(event -> {
            displayProcessComparison(comparison);
        });
        label.setMaxWidth(58);
        label.setTooltip(new Tooltip(comparison.getTooltipText()));
        return label;
    }
    private AnchorPane createClickableCalculateProcessComparisonLabel(BusinessProcess original, BusinessProcess actual, Function<ProcessComparison, Double> textRetriever, Function<ProcessComparison, Double> colorRetriever) {
        Label label = new Label("-");
        label.setMaxWidth(58);
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setStyle("-fx-border-color: grey; -fx-border-width: 2px;");
        anchorPane.getChildren().add(label);
        label.setOnMouseClicked(event -> {
            ProcessComparatorTaskParameters parameters = new ProcessComparatorTaskParameters();
            parameters.setOriginal(original);
            parameters.setActual(actual);

            buildAndExecuteTask(
                    new DefaultProcessComparatorTask(), parameters,
                    (task) -> {
                        anchorPane.getChildren().clear();
                        anchorPane.getChildren().add(
                                createClickableProcessComparisonLabel(((DefaultProcessComparatorTask) task).getValue(), textRetriever)
                        );
                        int score = (int) colorRetriever.apply(((DefaultProcessComparatorTask) task).getValue()).doubleValue();
                        Color color = getColor(score);
                        anchorPane.setStyle("-fx-background-color: " + String.format( "#%02X%02X%02X",
                                (int) (color.getRed() * 255),
                                (int) (color.getGreen() * 255),
                                (int) (color.getBlue() * 255)
                        ));
                        processMapComparison.getProcessComparisons().add(((DefaultProcessComparatorTask) task).getValue());
                        return new TaskExecutionResult();
                    }, true,
                    label
            );
        });
        label.setTooltip(new Tooltip("Calculate " + original.getName() + " vs " + actual.getName()));
        return anchorPane;
    }

    //Task executions
    public void importXLSXData(ActionEvent actionEvent) {
        KendoLogImportParameters parameters = new KendoLogImportParameters();
        parameters.setFiles(xlsxImportFilesList.getItems());
        parameters.setName(xlsxImportNameField.getText());

        buildAndExecuteTask(
                new KendoLogXLSXReadTask(), parameters,
                (task) -> {
                    executionLogList.getItems().add(((KendoLogXLSXReadTask) task).getValue());
                    return new TaskExecutionResult();
                }, true,
                xlsxImportFilesList, xlsxImportStartImportButton, xlsxImportAddFilesButton, xlsxImportNameField
        );
    }
    public void showKendoLogAsGraph(ActionEvent actionEvent) {
        TransitionModelBuilderParameters parameters = new TransitionModelBuilderParameters();
        parameters.setKendoLog(executionLogList.getSelectionModel().getSelectedItem());

        buildAndExecuteTask(
                transitionModelBuilderType.getSelectionModel().getSelectedItem(), parameters,
                (task) -> {
                    displayGraph(((TransitionModelBuilderTask) task).getValue(), graphComponent);
                    return new TaskExecutionResult();
                }, true,
                showKendoLogAsGraphButton
        );
    }
    public void processMapGenerateButton() {
        ProcessMapGenerationParameters parameters = new ProcessMapGenerationParameters();
        try {
            parameters.setNrOfProcesses(Integer.parseInt(processMapGenerationNrOfProcesses.getText()));
            parameters.setNrOfStepsPerProcess(Integer.parseInt(processMapGenerationNrOfSteps.getText()));
            parameters.setStepCountDeviation(Integer.parseInt(processMapGenerationStepDeviation.getText()));
            parameters.setNrOfTransitionsPerProcess(Integer.parseInt(processMapGenerationNrOfTransitions.getText()));
            parameters.setTransitionCountDeviation(Integer.parseInt(processMapGenerationTransitionsDeviation.getText()));
            parameters.setNrOfStartStepsPerProcess(Integer.parseInt(processMapGenerationNrOfStartSteps.getText()));
            parameters.setStartStepsCountDeviation(Integer.parseInt(processMapGenerationStartStepDeviation.getText()));
            parameters.setNrOfEndStepsPerProcess(Integer.parseInt(processMapGenerationNrOfEndSteps.getText()));
            parameters.setEndStepCountDeviation(Integer.parseInt(processMapGenerationEndStepDeviation.getText()));
            parameters.setSeed(Integer.parseInt(processMapGenerationRandomSeed.getText()));

            buildAndExecuteTask(
                    processMapGenerationType.getSelectionModel().getSelectedItem(), parameters,
                    (task) -> {
                        processMapTableView.getItems().add(((ProcessMapDataGenerationTask) task).getValue());
                        return new TaskExecutionResult();
                    }, true,
                    processMapGenerationType, processMapGenerateButton
            );
        } catch (NumberFormatException e) {
            showError("Please enter numbers in all fields");
        }
    }
    public void generateKendoLogData() {
        KendoLogGenerationParameters parameters = new KendoLogGenerationParameters();
        parameters.setProcessMap(kendoLogGeneratorProcessModelComboBox.getValue());
        try { parameters.setNrOfRepeats(Integer.parseInt(kendoLogGeneratorNrOfRepeats.getText())); }
        catch (NumberFormatException e) { showError("Nr. of repeats must be an Integer"); }
        try { parameters.setSeed(Integer.parseInt(kendoLogGeneratorSeed.getText())); }
        catch (NumberFormatException e) { showError("The seed must be an Integer"); }
        try { parameters.setErrorInsertionChance(Double.parseDouble(kendoLogGeneratorErrorInsertionChance.getText())); }
        catch (NumberFormatException e) { showError("The error insertion must be an Double"); }

        buildAndExecuteTask(
                kendologGeneratorTypeComboBox.getSelectionModel().getSelectedItem(), parameters,
                (task) -> {
                    executionLogList.getItems().add(((LogGeneratorTask) task).getValue());
                    return new TaskExecutionResult();
                }, true,
                kendologGeneratorTypeComboBox, generateKendoLogDataButton
        );
    }
    public void determineProcessMapComparison(ActionEvent actionEvent) {
        ProcessMapComparatorTaskParameters parameters = new ProcessMapComparatorTaskParameters();
        parameters.setOriginal(compareProcessMapOriginalComboBox.getValue());
        parameters.setActual(compareProcessMapActualComboBox.getValue());
        parameters.setCalculateProcesses(compareProcessMapCalculateProcesses.isSelected());

        buildAndExecuteTask(
                determineProcessMapComparatorComboBox.getSelectionModel().getSelectedItem(), parameters,
                (task) -> {
                    processMapComparison = ((ProcessMapComparatorTask) task).getValue();
                    displayProcessMapComparison(processMapComparison);
                    return new TaskExecutionResult();
                }, true,
                determineProcessMapComparisonButton, compareProcessMapOriginalComboBox, compareProcessMapActualComboBox, determineProcessMapComparatorComboBox, compareProcessMapCalculateProcesses
        );
    }
    public void mineProcessMap(ActionEvent actionEvent) {
        ProcessMapMiningParameters parameters = new ProcessMapMiningParameters();
        parameters.setKendoLog(mineProcessMapKendoLogComboBox.getValue());


        String[] res = tracerBlacklistTextArea.getText().split(";");
        if (res.length > 1 || (res.length == 1 && !res[0].equals("")))
            parameters.setTracerBlacklist(res);

        try {
            parameters.setTrafficThreshold(Integer.parseInt(mineProcessMapLimit.getText()));
            parameters.setMergeThreshold(Double.parseDouble(mergingThresholdTextField.getText()));
            parameters.setErrorThreshold(Integer.parseInt(errorThresholdTextField.getText()));
            parameters.setErrorCooldown(Integer.parseInt(errorCooldownTextField.getText()));
            parameters.setNodesThresHold(Integer.parseInt(nodesThresholdTextField.getText()));
        } catch (NumberFormatException e) { showError("Limit must be an Integer"); }
        parameters.setDictionaryFile(dictFile);
        if (processMinerTracerComboBox.getValue() != null) {
            try {
                parameters.setTraceBuilder(processMinerTracerComboBox.getValue().getClass().newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                showException(e.getMessage(), e);
            }
        } else showError("You must select a tracer!");

        buildAndExecuteTask(
                mineProcessMapMinerComboBox.getSelectionModel().getSelectedItem(), parameters,
                (task) -> {
                    processMapTableView.getItems().add(((ProcessMapMiningTask) task).getValue());
                    return new TaskExecutionResult();
                }, true,
                mineProcessMapButton, mineProcessMapKendoLogComboBox, mineProcessMapLimit, dictFileSelectButton, processMinerTracerComboBox, mergingThresholdTextField
        );
    }
    @FXML
    private void calculateAndDisplayProcessMiningParameterized() {
        ParameterizedProcessMapComparatorTaskParameters parameters = new ParameterizedProcessMapComparatorTaskParameters();
        String yAxis = "score (%)";
        String xAxis;

        if (parameterizedMapComparisonOriginal.getValue() == null) {
            showError("Original map must be selected");
            return;
        }
        parameters.setOriginal(parameterizedMapComparisonOriginal.getValue());
//        parameterizedMapComparisonScore.getValue() == null ||
        if (parameterizedMapComparisonParameter.getValue() == null) {
            showError("Score and Parameter must be selected");
            return;
        }
//        switch (parameterizedMapComparisonScore.getValue()) {
//            case "Average Score":
//                parameters.setScoreRetriever(ProcessMapComparison::calculateAverageScore);
//                yAxis = "Average Score";
//                break;
//            default:
//                showError("Invalid Score value");
//                return;
//        }
        switch (parameterizedMapComparisonParameter.getValue()) {
            case "Traffic Threshold":
                parameters.setAddFunction(((processMapMiningParameters, aDouble) -> {
                    processMapMiningParameters.setTrafficThreshold((int) Math.floor(aDouble));
                    return processMapMiningParameters;
                }));
                xAxis = "Traffic Threshold";
                break;
            case "Error Cooldown":
                parameters.setAddFunction(((processMapMiningParameters, aDouble) -> {
                    processMapMiningParameters.setErrorCooldown((int) Math.floor(aDouble));
                    return processMapMiningParameters;
                }));
                xAxis = "Error Cooldown";
                break;
            case "Error Threshold":
                parameters.setAddFunction(((processMapMiningParameters, aDouble) -> {
                    processMapMiningParameters.setErrorThreshold((int) Math.floor(aDouble));
                    return processMapMiningParameters;
                }));
                xAxis = "Error Threshold";
                break;
            case "Nodes Threshold":
                parameters.setAddFunction(((processMapMiningParameters, aDouble) -> {
                    processMapMiningParameters.setNodesThresHold((int) Math.floor(aDouble));
                    return processMapMiningParameters;
                }));
                xAxis = "Nodes Threshold\"";
                break;
            case "Merge Threshold":
                parameters.setAddFunction(((processMapMiningParameters, aDouble) -> {
                    processMapMiningParameters.setMergeThreshold(aDouble);
                    return processMapMiningParameters;
                }));
                xAxis = "Merge Threshold";
                break;
            default:
                showError("Invalid Parameter value");
                return;
        }

        try {
            parameters.setStartIndex(Double.parseDouble(parameterizedMapComparisonStartIndex.getText()));
            parameters.setIncrementer(Double.parseDouble(parameterizedMapComparisonIncrementer.getText()));
            parameters.setEndIndex(Double.parseDouble(parameterizedMapComparisonEndIndex.getText()));
        } catch (NumberFormatException e) { showError("Limit must be an Integer"); }

        ProcessMapMiningParameters miningParameters = new ProcessMapMiningParameters();
        miningParameters.setKendoLog(mineProcessMapKendoLogComboBox.getValue());
        try {
            miningParameters.setTrafficThreshold(Integer.parseInt(mineProcessMapLimit.getText()));
            miningParameters.setMergeThreshold(Double.parseDouble(mergingThresholdTextField.getText()));
            miningParameters.setErrorThreshold(Integer.parseInt(errorThresholdTextField.getText()));
            miningParameters.setErrorCooldown(Integer.parseInt(errorCooldownTextField.getText()));
            miningParameters.setNodesThresHold(Integer.parseInt(nodesThresholdTextField.getText()));
        } catch (NumberFormatException e) { showError("Limit must be an Integer"); }
        miningParameters.setDictionaryFile(dictFile);
        if (processMinerTracerComboBox.getValue() != null) {
            try {
                miningParameters.setTraceBuilder(processMinerTracerComboBox.getValue().getClass().newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                showException(e.getMessage(), e);
            }
        } else showError("You must select a tracer!");
        parameters.setProcessMapMiningParameters(miningParameters);
        parameters.setMiningTask(mineProcessMapMinerComboBox.getSelectionModel().getSelectedItem());

        parameterizedComparisonResultLineChart.getData().clear();

        buildAndExecuteTask(
                new DefaultParameterizedParameterizedProcessMapComparatorTask(), parameters,
                (task) -> {
                    Map<Double, Score> values = ((ParameterizedProcessMapComparatorTask) task).getValue();

                    parameterizedComparisonResultLineChart.getXAxis().setLabel(xAxis);
                    parameterizedComparisonResultLineChart.getYAxis().setLabel(yAxis);
                    ((NumberAxis) parameterizedComparisonResultLineChart.getXAxis()).setForceZeroInRange(false);

                    parameterizedComparisonResultLineChart.setTitle(yAxis + " over " + xAxis);
                    //defining a series
                    XYChart.Series totalScore = new XYChart.Series();
                    totalScore.setName("Total Score");
                    XYChart.Series originalScore = new XYChart.Series();
                    originalScore.setName("Average Max Original Score");
                    XYChart.Series actualScore = new XYChart.Series();
                    actualScore.setName("Average Max Actual Score");
                    XYChart.Series actualMissingScore = new XYChart.Series();
                    actualMissingScore.setName("Average Max Actual Missing Score");
                    XYChart.Series actualInvalidScore = new XYChart.Series();
                    actualInvalidScore.setName("Average Max Actual Invalid Score");
                    XYChart.Series originalMissingScore = new XYChart.Series();
                    originalMissingScore.setName("Average Max Original Missing Score");
                    XYChart.Series originalInvalidScore = new XYChart.Series();
                    originalInvalidScore.setName("Average Max Original Invalid Score");

                    XYChart.Series transTotal = new XYChart.Series();
                    transTotal.setName("Trans Total Score");
                    XYChart.Series transInvalidN = new XYChart.Series();
                    transInvalidN.setName("Trans Invalid Nodes");
                    XYChart.Series transInvalidE = new XYChart.Series();
                    transInvalidE.setName("Trans Invalid Edges");
                    XYChart.Series transMissingN = new XYChart.Series();
                    transMissingN.setName("Trans Missing Nodes");
                    XYChart.Series transMissingE = new XYChart.Series();
                    transMissingE.setName("Trans Missing Edges");
                    XYChart.Series transCorrectN = new XYChart.Series();
                    transCorrectN.setName("Trans Correct Nodes");
                    XYChart.Series transCorrectE = new XYChart.Series();
                    transCorrectE.setName("Trans Correct Edges");
                    XYChart.Series totalProcessCompletion = new XYChart.Series();
                    totalProcessCompletion.setName("Total Process Completion");

                    for (Double key : values.keySet()) {
                        totalScore.getData().add(new XYChart.Data(key,              Double.isNaN(values.get(key).getTotalScore()) ? -1 : values.get(key).getTotalScore()));
                        originalScore.getData().add(new XYChart.Data(key,           Double.isNaN(values.get(key).getMaxOriginalScore()) ? -1 : values.get(key).getMaxOriginalScore()));
                        actualScore.getData().add(new XYChart.Data(key,             Double.isNaN(values.get(key).getMaxActualScore()) ? -1 : values.get(key).getMaxActualScore()));
                        actualMissingScore.getData().add(new XYChart.Data(key,      Double.isNaN(values.get(key).getMaxActualMissingPercentage()) ? -1 : values.get(key).getMaxActualMissingPercentage()));
                        actualInvalidScore.getData().add(new XYChart.Data(key,      Double.isNaN(values.get(key).getMaxActualInvalidPercentage()) ? -1 : values.get(key).getMaxActualInvalidPercentage()));
                        originalMissingScore.getData().add(new XYChart.Data(key,    Double.isNaN(values.get(key).getMaxOriginalMissingPercentage()) ? -1 : values.get(key).getMaxOriginalMissingPercentage()));
                        originalInvalidScore.getData().add(new XYChart.Data(key,    Double.isNaN(values.get(key).getMaxOriginalInvalidPercentage()) ? -1 : values.get(key).getMaxOriginalInvalidPercentage()));
                        transTotal.getData().add(new XYChart.Data(key,              Double.isNaN(values.get(key).getTransTotal()) ? -1 : values.get(key).getTransTotal()));
                        transInvalidN.getData().add(new XYChart.Data(key,           Double.isNaN(values.get(key).getTransInvalidN()) ? -1 : values.get(key).getTransInvalidN()));
                        transInvalidE.getData().add(new XYChart.Data(key,           Double.isNaN(values.get(key).getTransInvalidE()) ? -1 : values.get(key).getTransInvalidE()));
                        transMissingN.getData().add(new XYChart.Data(key,           Double.isNaN(values.get(key).getTransMissingN()) ? -1 : values.get(key).getTransMissingN()));
                        transMissingE.getData().add(new XYChart.Data(key,           Double.isNaN(values.get(key).getTransMissingE()) ? -1 : values.get(key).getTransMissingE()));
                        transCorrectN.getData().add(new XYChart.Data(key,           Double.isNaN(values.get(key).getTransCorrectN()) ? -1 : values.get(key).getTransCorrectN()));
                        transCorrectE.getData().add(new XYChart.Data(key,           Double.isNaN(values.get(key).getTransCorrectE()) ? -1 : values.get(key).getTransCorrectE()));

                        totalProcessCompletion.getData().add(new XYChart.Data(key,  Double.isNaN(values.get(key).getTotalProcessCompletion()) ? -1 : values.get(key).getTotalProcessCompletion()));
                    }
                    parameterizedComparisonResultLineChart.getData().add(totalScore);
                    parameterizedComparisonResultLineChart.getData().add(originalScore);
                    parameterizedComparisonResultLineChart.getData().add(actualScore);
                    parameterizedComparisonResultLineChart.getData().add(actualMissingScore);
                    parameterizedComparisonResultLineChart.getData().add(actualInvalidScore);
                    parameterizedComparisonResultLineChart.getData().add(originalMissingScore);
                    parameterizedComparisonResultLineChart.getData().add(originalInvalidScore);

                    parameterizedComparisonResultLineChart.getData().add(transTotal);
                    parameterizedComparisonResultLineChart.getData().add(transInvalidN);
                    parameterizedComparisonResultLineChart.getData().add(transInvalidE);
                    parameterizedComparisonResultLineChart.getData().add(transMissingN);
                    parameterizedComparisonResultLineChart.getData().add(transMissingE);
                    parameterizedComparisonResultLineChart.getData().add(transCorrectN);
                    parameterizedComparisonResultLineChart.getData().add(transCorrectE);

                    parameterizedComparisonResultLineChart.getData().add(totalProcessCompletion);


                    //https://stackoverflow.com/questions/44956955/javafx-use-chart-legend-to-toggle-show-hide-series-possible
                    for (javafx.scene.Node n : parameterizedComparisonResultLineChart.getChildrenUnmodifiable()) {
                        if (n instanceof Legend) {
                            Legend l = (Legend) n;
                            for (Legend.LegendItem li : l.getItems()) {
                                for (XYChart.Series<Number, Number> s : parameterizedComparisonResultLineChart.getData()) {
                                    if (s.getName().equals(li.getText())) {
                                        li.getSymbol().setCursor(Cursor.HAND); // Hint user that legend symbol is clickable
                                        li.getSymbol().setOnMouseClicked(me -> {
                                            if (me.getButton() == MouseButton.PRIMARY) {
                                                toggleVisibility(s);
                                            }
                                        });
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    // END https://stackoverflow.com/questions/44956955/javafx-use-chart-legend-to-toggle-show-hide-series-possible

                    toggleVisibility(totalScore);
                    toggleVisibility(actualScore);
                    toggleVisibility(actualMissingScore);
                    toggleVisibility(actualInvalidScore);
                    toggleVisibility(originalMissingScore);
                    toggleVisibility(originalInvalidScore);
                    toggleVisibility(transInvalidN);
                    toggleVisibility(transInvalidE);
                    toggleVisibility(transMissingN);
                    toggleVisibility(transMissingE);
                    toggleVisibility(transCorrectN);
                    toggleVisibility(transCorrectE);


                    return new TaskExecutionResult();
                }, true,
                mineProcessMapButton, mineProcessMapKendoLogComboBox, mineProcessMapLimit,
                dictFileSelectButton, processMinerTracerComboBox, mergingThresholdTextField,
                parameterizedMapComparisonStartIndex, parameterizedMapComparisonIncrementer,
                parameterizedMapComparisonEndIndex, parameterizedMapComparisonParameter,
                parameterizedMapComparisonOriginal
        );
    }
    @FXML
    private void optimizeParameters() {
        ParameterizedProcessMapComparatorTaskParameters parameters = new ParameterizedProcessMapComparatorTaskParameters();
        if (parameterizedMapComparisonOriginal.getValue() == null) {
            showError("Original map must be selected");
            return;
        }
        parameters.setOriginal(parameterizedMapComparisonOriginal.getValue());
        if (parameterizedMapComparisonParameter.getValue() == null) {
            showError("Score and Parameter must be selected");
            return;
        }

        ProcessMapMiningParameters miningParameters = new ProcessMapMiningParameters();
        try {
            miningParameters.setTrafficThreshold(Integer.parseInt(mineProcessMapLimit.getText()));
            miningParameters.setMergeThreshold(Double.parseDouble(mergingThresholdTextField.getText()));
            miningParameters.setErrorThreshold(Integer.parseInt(errorThresholdTextField.getText()));
            miningParameters.setErrorCooldown(Integer.parseInt(errorCooldownTextField.getText()));
            miningParameters.setNodesThresHold(Integer.parseInt(nodesThresholdTextField.getText()));
            miningParameters.setKendoLog(mineProcessMapKendoLogComboBox.getValue());
            miningParameters.setDictionaryFile(dictFile);
            if (processMinerTracerComboBox.getValue() != null) {
                miningParameters.setTraceBuilder(processMinerTracerComboBox.getValue().getClass().newInstance());
            } else showError("You must select a tracer!");

            parameters.setStartIndex(Double.parseDouble(parameterizedMapComparisonStartIndex.getText()));
            parameters.setIncrementer(Double.parseDouble(parameterizedMapComparisonIncrementer.getText()));
            parameters.setEndIndex(Double.parseDouble(parameterizedMapComparisonEndIndex.getText()));
            parameters.setProcessMapMiningParameters(miningParameters);
            parameters.setMiningTask(mineProcessMapMinerComboBox.getSelectionModel().getSelectedItem());
        } catch (NumberFormatException e) {
            showError("Limit must be an Integer");
        } catch (InstantiationException | IllegalAccessException e) {
            showException(e.getMessage(), e);
        }
        parameterOptimizationStep(
                parameters,
                new ArrayList<BiFunction<ProcessMapMiningParameters, Double, ProcessMapMiningParameters>>(){{
                    add((processMapMiningParameters, aDouble) -> {
                        processMapMiningParameters.setTrafficThreshold((int) Math.floor(aDouble));
                        return processMapMiningParameters;
                    });
                    add((processMapMiningParameters, aDouble) -> {
                        processMapMiningParameters.setMergeThreshold(aDouble);
                        return processMapMiningParameters;
                    });
                    add((processMapMiningParameters, aDouble) -> {
                        processMapMiningParameters.setErrorThreshold((int) Math.floor(aDouble));
                        return processMapMiningParameters;
                    });
                    add((processMapMiningParameters, aDouble) -> {
                        processMapMiningParameters.setNodesThresHold((int) Math.floor(aDouble));
                        return processMapMiningParameters;
                    });
                    add((processMapMiningParameters, aDouble) -> {
                        processMapMiningParameters.setErrorCooldown((int) Math.floor(aDouble));
                        return processMapMiningParameters;
                    });
                }},
                new ArrayList<TextField>(){{
                    add(mineProcessMapLimit);
                    add(mergingThresholdTextField);
                    add(errorThresholdTextField);
                    add(nodesThresholdTextField);
                    add(errorCooldownTextField);
                }}
        );
    }

    private double extractScoreDouble(Score score) {
        return (score.getTransTotal() * 0.80) +
                (score.getTotalProcessCompletion() * 0.13) +
                (score.getMaxOriginalScore() * 0.07);
    }

    private void parameterOptimizationStep(ParameterizedProcessMapComparatorTaskParameters parameters,
                                           List<BiFunction<ProcessMapMiningParameters, Double, ProcessMapMiningParameters>> addFunction,
                                           List<TextField> setField) {
        parameters.setAddFunction(addFunction.get(0));
        buildAndExecuteTask(
                new DefaultParameterizedParameterizedProcessMapComparatorTask(), parameters,
                (task) -> {
                    double parameterValue = -1;
                    double maxScoreValue = 0;
                    Map<Double, Score> values = ((ParameterizedProcessMapComparatorTask) task).getValue();

                    for (Double pValue : values.keySet()) {
                        Score score = values.get(pValue);

                        if (extractScoreDouble(score) > maxScoreValue) {
                            parameterValue = pValue;
                            maxScoreValue = extractScoreDouble(score);
                        }
                    }

                    setField.get(0).setText(((int)parameterValue) + "");
                    addFunction.get(0).apply(parameters.getProcessMapMiningParameters(), parameterValue);
                    addFunction.remove(0);
                    setField.remove(0);
                    if (addFunction.size() > 0 && setField.size() > 0)
                        parameterOptimizationStep(parameters, addFunction, setField);
                    return new TaskExecutionResult();
                }, true,
                mineProcessMapButton, mineProcessMapKendoLogComboBox, mineProcessMapLimit,
                dictFileSelectButton, processMinerTracerComboBox, mergingThresholdTextField,
                parameterizedMapComparisonStartIndex, parameterizedMapComparisonIncrementer,
                parameterizedMapComparisonEndIndex, parameterizedMapComparisonParameter,
                parameterizedMapComparisonOriginal
        );
    }

    private void toggleVisibility(XYChart.Series<Number, Number> s) {
        s.getNode().setVisible(!s.getNode().isVisible()); // Toggle visibility of line
        for (XYChart.Data<Number, Number> d : s.getData()) {
            if (d.getNode() != null) {
                d.getNode().setVisible(s.getNode().isVisible()); // Toggle visibility of every node in the series
            }
        }
    }

    //Message displaying
    private void handleError(String error, Throwable exception) {
        showException(error, exception);
        Logger.getAnonymousLogger().warning("Error: '" + error + "' Exception: '" + exception + "'");
    }
    private void showException(String title, Throwable exception) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setContentText(exception.getMessage());

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);
//        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(FXMain.class.getResourceAsStream("icon.png")));
        alert.showAndWait();
    }
    private void showError(String error) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("An error has occured");
        alert.setContentText(error);
//        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(FXMain.class.getResourceAsStream("icon.png")));
        alert.showAndWait();
    }
    private void showWarning(String warning) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning");
        alert.setContentText(warning);
//        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(FXMain.class.getResourceAsStream("icon.png")));
        alert.showAndWait();
    }
    private void showInfo(String info) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Ahum...");
        alert.setContentText(info);
        ((Stage) alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(FXMain.class.getResourceAsStream("icon.png")));
        alert.showAndWait();
    }

    //Task execution utils
    private Stage showProgressDialog(Task task) {
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);

        GridPane gridPane = new GridPane();
        gridPane.getRowConstraints().add(new RowConstraints(30));
        gridPane.getRowConstraints().add(new RowConstraints(30));
        gridPane.getColumnConstraints().add(new ColumnConstraints(100));
        gridPane.getColumnConstraints().add(new ColumnConstraints(400));
        gridPane.getColumnConstraints().add(new ColumnConstraints(100));
        gridPane.setHgap(2);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(10));

        Label progressLabel = new Label("Progress: ");
        gridPane.add(progressLabel, 0, 1, 1, 1);

        Label progressMessageLabel = new Label();
        progressMessageLabel.textProperty().bind(task.messageProperty());
        gridPane.add(progressMessageLabel, 1, 1, 1, 1);

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(event -> {
            task.cancel(true);
        });
        gridPane.add(cancelButton, 2, 1, 1, 1);

        ProgressBar progressBar = new ProgressBar();
        progressBar.setPrefHeight(30);
        progressBar.setPrefWidth(600);
        progressBar.progressProperty().bind(task.progressProperty());
        gridPane.add(progressBar, 0, 0, 3, 1);

        Scene dialogScene = new Scene(gridPane, 600, 85);
        dialog.setScene(dialogScene);
//        dialog.getIcons().add(new Image(FXMain.class.getResourceAsStream("icon.png")));
        dialog.setResizable(false);
        dialog.show();
        return dialog;
    }
    private void buildAndExecuteTask(LoggableTask taskTemplate, TaskParameters taskParameters, Function<Task, TaskExecutionResult> successCallback, boolean closePopupOnFinish, Control... controls) {
        try {
            if (taskTemplate != null) {
                LoggableTask task = taskTemplate.getClass().newInstance();

                task.setTaskParameters(taskParameters);

                lockControls(controls);
                Stage dialog = showProgressDialog(task);
                task.setOnSucceeded(event -> {
                    successCallback.apply(task);
                    resetControls(controls);
                    if (closePopupOnFinish) dialog.close();
                });
                task.setOnFailed(event -> {
                    releaseControls(controls);
                    showException("Exception thrown during execution of task: " +  task.toString(), task.getException());
                    if (closePopupOnFinish) dialog.close();
                });
                task.setOnCancelled(event -> {
                    releaseControls(controls);
                    showWarning("Task was cancelled");
                    if (closePopupOnFinish) dialog.close();
                });

                Thread thread = new Thread(task);
                thread.start();
            } else showWarning("You have to select a generator/builder first.");
        } catch (InvalidArgumentException e) {
            showException("Invalid parameters detected", e);
        } catch (IllegalAccessException e) {
            showException("Illegal access", e);
        } catch (InstantiationException e) {
            showException("Unable to create a new Task from template", e);
        }
    }
    private void lockControls(Control... controls) {
        for (Control control : controls) {
            control.setDisable(true);
        }
    }
    private void releaseControls(Control... controls) {
        for (Control control : controls) {
            control.setDisable(false);
        }
    }
    private void resetControls(Control... controls) {
        for (Control control : controls) {
//            if (control instanceof TextField) {
//                ((TextField) control).setText("");
//            } else if (control instanceof ListView) {
//                ((ListView) control).getItems().clear();
//            }
        }
        releaseControls(controls);
    }

    /*-----------------------------------------------------------
                       Getters and Setters
     -----------------------------------------------------------*/

    public void selectDictFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Xml", ".xml"));
        dictFile = fileChooser.showOpenDialog(((javafx.scene.Node)actionEvent.getSource()).getScene().getWindow());
        dictFilePathLabel.setText(dictFile.getName());
    }

    //Standard Listeners
    public void listViewDeletionListener(KeyEvent keyEvent) {
        if (!(keyEvent.getSource() instanceof ListView))
            return;

        ListView listView = (ListView) keyEvent.getSource();

        if (keyEvent.getCode().equals( KeyCode.DELETE ))
            listView.getItems().remove(listView.getSelectionModel().getSelectedIndex());
    }

    //Simple button listeners
    public void circleLayout() {
        new mxCircleLayout(graphComponent.getGraph()).execute(graphComponent.getGraph().getDefaultParent());
    }
    public void compactTreeLayout() {
        mxCompactTreeLayout layout = new mxCompactTreeLayout(graphComponent.getGraph());
        layout.setNodeDistance(150);
        layout.execute(graphComponent.getGraph().getDefaultParent());
    }
    public void fastOrganicLayout() {
        mxFastOrganicLayout layout = new mxFastOrganicLayout(graphComponent.getGraph());
        layout.setMinDistanceLimit(1000);
        layout.execute(graphComponent.getGraph().getDefaultParent());
    }
    public void organicLayout() {
        new mxOrganicLayout(graphComponent.getGraph()).execute(graphComponent.getGraph().getDefaultParent());
    }
    public void parallelEdgeLayout() {
        new mxParallelEdgeLayout(graphComponent.getGraph()).execute(graphComponent.getGraph().getDefaultParent());
    }
    public void partitionLayout() {
        new mxPartitionLayout(graphComponent.getGraph()).execute(graphComponent.getGraph().getDefaultParent());
    }
    public void stackLayout() {
        new mxStackLayout(graphComponent.getGraph()).execute(graphComponent.getGraph().getDefaultParent());
    }
    public void hierarchicalLayout() {
        new mxHierarchicalLayout(graphComponent.getGraph()).execute(graphComponent.getGraph().getDefaultParent());
    }

    public void originalFastOrganicLayout() {
        mxFastOrganicLayout layout = new mxFastOrganicLayout(originalGraphComponent.getGraph());
        layout.setMinDistanceLimit(1000);
        layout.execute(originalGraphComponent.getGraph().getDefaultParent());
    }
    public void originalCompactTreeLayout() {
        new mxCompactTreeLayout(originalGraphComponent.getGraph()).execute(originalGraphComponent.getGraph().getDefaultParent());
    }
    public void originalHierachicalLayout() {
        new mxHierarchicalLayout(originalGraphComponent.getGraph()).execute(originalGraphComponent.getGraph().getDefaultParent());
    }
    public void originalOrganicLayout() {
        new mxOrganicLayout(originalGraphComponent.getGraph()).execute(originalGraphComponent.getGraph().getDefaultParent());
    }
    public void originalCircleLayout() {
        new mxCircleLayout(originalGraphComponent.getGraph()).execute(originalGraphComponent.getGraph().getDefaultParent());
    }
    public void originalParallelEdgeLayout() {
        new mxParallelEdgeLayout(originalGraphComponent.getGraph()).execute(originalGraphComponent.getGraph().getDefaultParent());
    }
    public void originalStackLayout() {
        new mxStackLayout(originalGraphComponent.getGraph()).execute(originalGraphComponent.getGraph().getDefaultParent());
    }
    public void originalPartitionLayout() {
        new mxPartitionLayout(originalGraphComponent.getGraph()).execute(originalGraphComponent.getGraph().getDefaultParent());
    }

    public void actualFastOrganicLayout() {
        mxFastOrganicLayout layout = new mxFastOrganicLayout(actualGraphComponent.getGraph());
        layout.setMinDistanceLimit(1000);
        layout.execute(actualGraphComponent.getGraph().getDefaultParent());
    }
    public void actualCompactTreeLayout() {
        new mxCompactTreeLayout(actualGraphComponent.getGraph()).execute(actualGraphComponent.getGraph().getDefaultParent());
    }
    public void actualHierachicalLayout() {
        new mxHierarchicalLayout(actualGraphComponent.getGraph()).execute(actualGraphComponent.getGraph().getDefaultParent());
    }
    public void actualOrganicLayout() {
        new mxOrganicLayout(actualGraphComponent.getGraph()).execute(actualGraphComponent.getGraph().getDefaultParent());
    }
    public void actualCircleLayout() {
        new mxCircleLayout(actualGraphComponent.getGraph()).execute(actualGraphComponent.getGraph().getDefaultParent());
    }
    public void actualParallelEdgeLayout() {
        new mxParallelEdgeLayout(actualGraphComponent.getGraph()).execute(actualGraphComponent.getGraph().getDefaultParent());
    }
    public void actualStackLayout() {
        new mxStackLayout(actualGraphComponent.getGraph()).execute(actualGraphComponent.getGraph().getDefaultParent());
    }
    public void actualPartitionLayout() {
        new mxPartitionLayout(actualGraphComponent.getGraph()).execute(actualGraphComponent.getGraph().getDefaultParent());
    }


    public void showProcessMapComparisonTransitionModel(ActionEvent actionEvent) {
        if (processMapComparison != null)
            displayTransitionModelComparison(processMapComparison.getTransitionModelComparison());
    }
}
